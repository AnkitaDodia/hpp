package com.codefidea.hpp.customviews;

/**
 * Created by MyInnos on 01-02-2017.
 */

import java.util.ArrayList;
import java.util.List;

public class DataHelper {

    public static List<String> getAlphabetData() {
        return new ArrayList<String>() {{
            add("Christian\tSutherland");
            add("Bella\tJames");
            add("Sophie\tRandall");
            add("Bernadette\tOliver");
            add("Lauren\tSmith");

            add("Jack\tMcLean");
            add("Angela\tReid");
            add("Kimberly\tSlater");
            add("Leah\tPayne");
            add("Alexander\tJames");

            add("Lillian\tHardacre");
            add("Harry\tDuncan");
            add("Sebastian\tPullman");
            add("Abigail\tRandall");
            add("Ava\tFisher");

            add("Anna\tWalsh");
            add("Carol\tNewman");
            add("Joanne\tPaterson");
            add("Neil\tVaughan");
            add("Wendy\tFerguson");

            add("Anna\tRampling");
            add("Abigail\tCornish");
            add("Sean\tUnderwood");
            add("Jan\tNash");
            add("Cameron\tRutherford");

            add("Sonia\tWelch");
            add("Gavin\tParr");
            add("Lily\tHenderson");
            add("Steven\tSkinner");
            add("Olivia\tMiller");

        }};
    }
}
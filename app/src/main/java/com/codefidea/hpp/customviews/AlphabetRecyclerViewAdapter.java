package com.codefidea.hpp.customviews;

/**
 * Created by MyInnos on 01-02-2017.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.StaffListActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.Staff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class AlphabetRecyclerViewAdapter extends RecyclerView.Adapter<AlphabetRecyclerViewAdapter.ViewHolder>
        implements SectionIndexer {

    //    private List<String> mDataArray;
    private ArrayList<Staff> mStaffsList;
    private ArrayList<Integer> mSectionPositions;
    StaffListActivity mContext;
    ImageLoader imageLoader;

    String [] Alphabes = new String [] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    public AlphabetRecyclerViewAdapter(StaffListActivity mContext, ArrayList<Staff> mStaffsList) {
        this.mStaffsList = mStaffsList;
        this.mContext = mContext;

        imageLoader = AppController.getInstance().getImageLoader();
    }


    @Override
    public int getItemCount() {
        if (mStaffsList == null)
            return 0;
        return mStaffsList.size();
    }

    @Override
    public AlphabetRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_alphabets, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mtxt_staffname.setText(mStaffsList.get(position).getFirstName());
        holder.mtxt_staffname.setTypeface(mContext.getTypeFace());

        imageLoader.get(mStaffsList.get(position).getProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.img_staffperson.setImageBitmap(response.getBitmap());
//                    holder.news_progress.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        List<String> strAlphabets = new ArrayList<String>(Arrays.asList(Alphabes));
        for (int i = 0, size = strAlphabets.size(); i < size; i++) {
//            String section = String.valueOf(mStaffsList.get(i).getFirstName().charAt(0)).toUpperCase();
            String section = String.valueOf(strAlphabets.get(i));
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView mtxt_staffname;
//        ImageView img_staffperson;
        CircleImageView img_staffperson;

        ViewHolder(View itemView) {
            super(itemView);

            mtxt_staffname = (TextView) itemView.findViewById(R.id.txt_staffname);
            img_staffperson = (CircleImageView) itemView.findViewById(R.id.img_staffperson);
        }
    }
}
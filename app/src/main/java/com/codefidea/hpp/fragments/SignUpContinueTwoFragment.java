package com.codefidea.hpp.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.adapters.MorphoTypeAdapter;
import com.codefidea.hpp.adapters.WeeklyDatesAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.MorphesType;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class SignUpContinueTwoFragment extends Fragment
{
    SignUpPagerActivity mContext;

    RecyclerView rcv_morpho_type;

    RelativeLayout layout_signUp_two_main;

    boolean isInternet;

    private ArrayList<MorphesType> mMorpho = new ArrayList<>();
    List<Integer> mMorphoSelectionsList = new ArrayList<>();

    MorphoTypeAdapter adapter;

    Button btn_signup_two;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = (SignUpPagerActivity) getActivity();
        return inflater.inflate(R.layout.fragment_signup_two, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isInternet = new InternetStatus().isInternetOn(mContext);

        getView(view);
        mContext.overrideFonts(mContext,layout_signUp_two_main);
        setClickListener();

        if(BaseActivity.mMorpho.size() > 0)
        {
            mMorpho = BaseActivity.mMorpho;
            initMorphoRecycler();
        }
        else
        {
            if (isInternet) {
                sendMorphoRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getView(View view)
    {
        layout_signUp_two_main = (RelativeLayout) view.findViewById(R.id.layout_signUp_two_main);

        rcv_morpho_type = (RecyclerView) view.findViewById(R.id.rcv_morpho_type);

        btn_signup_two = (Button) view.findViewById(R.id.btn_signup_two);
    }

    private void setClickListener()
    {
        btn_signup_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current =  mContext.getItem(+1);

                if (current <=  mContext.no_of_sample_tutorial -1) {
                    mContext.view_pager_signup_pager.setCurrentItem(current);
                }
            }
        });

        rcv_morpho_type.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_morpho_type, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.morpheTypeId = mMorpho.get(position).getId();
                Log.e("morpheTypeId",""+BaseActivity.morpheTypeId);

                PrepareMorphoSelection(position);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void PrepareMorphoSelection(int position) {
        mMorphoSelectionsList.clear();

        Log.e("SelectionList", "SelectionList size" + mMorpho.size());
        for (int i = 0; i <= mMorpho.size(); i++) {
            Log.e("SelectionList", "SelectionList" + i);
            if (i == position) {
                mMorphoSelectionsList.add(1);
            } else {
                mMorphoSelectionsList.add(0);
            }
        }
    }

    private void sendMorphoRequest()
    {
        mContext.showWaitIndicator(true);

        String morphoURL = Constant.API_BASE_URL+"/morphes";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,morphoURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("MORPHO_API ", response.toString());
                getMorphoData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getMorphoData(String s)
    {
        try {
            JSONArray morphoList = new JSONArray();

            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res){
                    morphoList = jObject.getJSONArray("lifestyles");

                    for (int i = 0; i < morphoList.length(); i++)
                    {
                        MorphesType data = new MorphesType();

                        data.setId(morphoList.getJSONObject(i).getString(Constant.id));
                        data.setDescription(morphoList.getJSONObject(i).getString(Constant.description));
                        data.setImage(morphoList.getJSONObject(i).getString(Constant.image));
                        data.setName(morphoList.getJSONObject(i).getString(Constant.name));

                        mMorpho.add(data);
                    }

                    BaseActivity.mMorpho = mMorpho;
                    initMorphoRecycler();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initMorphoRecycler()
    {
        PrepareMorphoSelection(0);

        rcv_morpho_type.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_morpho_type.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        adapter = new MorphoTypeAdapter(mContext, mMorpho,mMorphoSelectionsList);
        rcv_morpho_type.setAdapter(adapter);
        rcv_morpho_type.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
    }
}

package com.codefidea.hpp.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.adapters.CityAdapter;
import com.codefidea.hpp.adapters.GenderSpinnerAdapter;
import com.codefidea.hpp.adapters.UnitSpinnerAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.Cities;
import com.codefidea.hpp.model.Countries;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class SignUpContinueOneFragment extends Fragment {
    SignUpPagerActivity mContext;

    EditText edit_name, edit_surname, edit_dob, edit_phonenumber, edit_cod, edit_aboutme;

    LinearLayout layout_signup_one_main;

    Spinner spinner_country, spinner_city, spinner_gender;

    TextView text_spinner_title;

    ArrayList<Countries> mCountryList = new ArrayList<>();
    ArrayList<Cities> mCityList = new ArrayList<>();

    boolean isInternet;

    CountryAdapter mCountryAdapter;
    CityAdapter cityAdapter;

    String errorMsg;

    Calendar myCalendar;

    Button btn_signup_one;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = (SignUpPagerActivity) getActivity();
        return inflater.inflate(R.layout.fragment_signup_one, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView(view);
        mContext.overrideFonts(mContext, layout_signup_one_main);
        setClickListener();

        isInternet = new InternetStatus().isInternetOn(mContext);

        if (BaseActivity.countryList.size() > 0) {
            mCountryList = BaseActivity.countryList;
            setSpinnerData();
        } else {
            if (isInternet) {
                sendCountryRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }

        myCalendar = Calendar.getInstance();
    }

    private void getView(View view) {
        layout_signup_one_main = (LinearLayout) view.findViewById(R.id.layout_signup_one_main);

        edit_name = (EditText) view.findViewById(R.id.edit_name);
        edit_surname = (EditText) view.findViewById(R.id.edit_surname);
        edit_dob = (EditText) view.findViewById(R.id.edit_dob);
        edit_phonenumber = (EditText) view.findViewById(R.id.edit_phonenumber);
        edit_cod = (EditText) view.findViewById(R.id.edit_cod);
        edit_aboutme = (EditText) view.findViewById(R.id.edit_aboutme);

        spinner_country = (Spinner) view.findViewById(R.id.spinner_country);
        spinner_city = (Spinner) view.findViewById(R.id.spinner_city);
        spinner_gender = (Spinner) view.findViewById(R.id.spinner_gender);

        text_spinner_title = (TextView) view.findViewById(R.id.text_spinner_title);

        btn_signup_one = (Button) view.findViewById(R.id.btn_signup_one);

        GenderSpinnerAdapter mGenderSpinnerAdapter = new GenderSpinnerAdapter(
                mContext,
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.gender_array))
        );

        spinner_gender.setAdapter(mGenderSpinnerAdapter);

    }

    private void setClickListener() {

        edit_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(mContext, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        spinner_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("Spinner item : ", "" + mCountryList.get(position).getName());

                BaseActivity.countryID = mCountryList.get(position).getId();
                sendCityRequest();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.e("Spinner item : ", "" + mCityList.get(position).getName());

                BaseActivity.cityID = mCityList.get(position).getId();
//                spinner_city.getSelectedItem().toString();
                Log.e("selected item","selected item"+spinner_city.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                BaseActivity.sex = spinner_gender.getSelectedItem().toString();
//                spinner_city.getSelectedItem().toString();
                Log.e("selected item","selected item  : "+spinner_gender.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_signup_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(text_spinner_title.getVisibility() == View.VISIBLE)
                {
                    Toast.makeText(mContext,"Please select appropriate country",Toast.LENGTH_LONG).show();
                }
                else
                {
                    int current =  mContext.getItem(+1);

                    BaseActivity.userName = edit_name.getText().toString();
                    BaseActivity.surName = edit_surname.getText().toString();
                    BaseActivity.phone = edit_phonenumber.getText().toString();
                    BaseActivity.birthDate = edit_dob.getText().toString();
                    BaseActivity.fiscalCode = edit_cod.getText().toString();
                    BaseActivity.aboutMe = edit_aboutme.getText().toString();

                   /* if(edit_gender.getText().toString().equalsIgnoreCase("Male") ||
                            edit_gender.getText().toString().equalsIgnoreCase("male") ||
                            edit_gender.getText().toString().equalsIgnoreCase("MALE") ||
                            edit_gender.getText().toString().equalsIgnoreCase("m") ||
                            edit_gender.getText().toString().equalsIgnoreCase("M"))
                    {
                        BaseActivity.sex = "M";
                    }
                    else if(edit_gender.getText().toString().equalsIgnoreCase("Female") ||
                            edit_gender.getText().toString().equalsIgnoreCase("female") ||
                            edit_gender.getText().toString().equalsIgnoreCase("FEMALE") ||
                            edit_gender.getText().toString().equalsIgnoreCase("f") ||
                            edit_gender.getText().toString().equalsIgnoreCase("F"))
                    {
                        BaseActivity.sex = "F";
                    }
                    else
                    {
                        BaseActivity.sex = "O";
                    }*/

                    Log.e("UserInfo",""+BaseActivity.userName);
                    Log.e("UserInfo",""+BaseActivity.surName);
                    Log.e("UserInfo ",""+BaseActivity.phone );
                    Log.e("UserInfo",""+BaseActivity.birthDate);
                    Log.e("UserInfo",""+BaseActivity.sex);
                    Log.e("UserInfo",""+BaseActivity.fiscalCode);
                    Log.e("UserInfo",""+BaseActivity.aboutMe);

                    if (current <=  mContext.no_of_sample_tutorial -1) {
                        mContext.view_pager_signup_pager.setCurrentItem(current);
                    }
                }
            }
        });
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
//        String myFormat = DateFormat.getDateInstance(DateFormat.SHORT).format(date);
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        edit_dob.setText(sdf.format(myCalendar.getTime()));
    }

    /***************************************** Country request **************************************/
    private void sendCountryRequest() {
//        mContext.showWaitIndicator(true);

        String countryURL = Constant.API_BASE_URL + "/countries";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST, countryURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("COUNTRY_API ", response.toString());
                getCountryData(response.toString());
//                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getCountryData(String s) {
        try {
            JSONArray countryList = new JSONArray();

            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    countryList = jObject.getJSONArray("countries");

                    for (int i = 0; i < countryList.length(); i++) {
                        Countries mCountries = new Countries();

                        mCountries.setId(countryList.getJSONObject(i).getString(Constant.id));
                        mCountries.setName(countryList.getJSONObject(i).getString(Constant.name));

                        mCountryList.add(mCountries);
                    }

                    BaseActivity.countryList = mCountryList;
                    setSpinnerData();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinnerData() {
        mCountryAdapter = new CountryAdapter(mContext, mCountryList);
        spinner_country.setAdapter(mCountryAdapter);
        for(int i = 0; i < mCountryList.size(); i++)
        {
            if(mCountryList.get(i).getId().equalsIgnoreCase("111"))
            {
                spinner_country.setSelection(i);
                break;
            }
        }
    }

    public class CountryAdapter extends BaseAdapter {
        SignUpPagerActivity mContext;

        ArrayList<Countries> CountriesList = new ArrayList<>();

        LayoutInflater inflter;

        public CountryAdapter(SignUpPagerActivity applicationContext, ArrayList<Countries> countryList) {
            this.mContext = applicationContext;
            this.CountriesList = countryList;

            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return CountriesList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            view = inflter.inflate(R.layout.row_countries_city_layout, null);

            TextView names = (TextView) view.findViewById(R.id.txt_country_name);

            LinearLayout ll_parent_row_country = (LinearLayout) view.findViewById(R.id.ll_parent_row_country);

            names.setTypeface(mContext.getTypeFace());

            Countries data = CountriesList.get(position);

            names.setText(data.getName());
            return view;
        }
    }
    /***************************************** Country request **************************************/

    /***************************************** City request **************************************/
    private void sendCityRequest() {
//        mContext.showWaitIndicator(true);

        String countryURL = Constant.API_BASE_URL + "/cities";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST, countryURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CITY_API ", response.toString());
                getCityData(response.toString());
//                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("countryId", BaseActivity.countryID);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getCityData(String s) {
        try {
            JSONArray cityListArray;

            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    cityListArray = jObject.getJSONArray("cities");

                    for (int i = 0; i < cityListArray.length(); i++) {
                        Cities data = new Cities();

                        data.setId(cityListArray.getJSONObject(i).getString(Constant.id));
                        data.setName(cityListArray.getJSONObject(i).getString(Constant.name));

                        mCityList.add(data);
                    }


                } else {
                    errorMsg = jObject.getString("message");
                    text_spinner_title.setText(errorMsg);
                    mCityList.clear();
                }

                setCitySpinnerData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCitySpinnerData() {

        Log.e("size", ""+mCityList.size());
        if (mCityList.size() > 0) {
            text_spinner_title.setVisibility(View.GONE);
            spinner_city.setVisibility(View.VISIBLE);

            cityAdapter = new CityAdapter(mContext, mCityList);
            spinner_city.setAdapter(cityAdapter);


        } else {
            text_spinner_title.setVisibility(View.VISIBLE);
            spinner_city.setVisibility(View.GONE);


        }
    }
    /***************************************** City request **************************************/
}

package com.codefidea.hpp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.PaymentActivity;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.activities.SingUpActivity;
import com.codefidea.hpp.adapters.LifeStyleAdapter;
import com.codefidea.hpp.adapters.PlanAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.LifeStyle;
import com.codefidea.hpp.model.Plans;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class SignUpContinueFourFragment extends Fragment
{
    SignUpPagerActivity mContext;

    Button btn_make_payment_singUp_four;

    private ArrayList<Plans> mPlans = new ArrayList<>();
    List<Integer> mPlanSelectionsList = new ArrayList<>();

    RecyclerView rcv_plan;

    boolean isInternet;

    PlanAdapter adapter;

    RelativeLayout layout_signUp_four_main;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = (SignUpPagerActivity) getActivity();
        return inflater.inflate(R.layout.fragment_signup_four, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView(view);
        mContext.overrideFonts(mContext,layout_signUp_four_main);
        setClickListener();

        isInternet = new InternetStatus().isInternetOn(mContext);

        if(BaseActivity.mPlans.size() > 0)
        {
            mPlans = BaseActivity.mPlans;
            initPlansRecycler();
        }
        else
        {
            if (isInternet) {
                sendPlansRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getView(View view)
    {
        rcv_plan = (RecyclerView) view.findViewById(R.id.rcv_plan);

        layout_signUp_four_main = (RelativeLayout) view.findViewById(R.id.layout_signUp_four_main);

        btn_make_payment_singUp_four = (Button) view.findViewById(R.id.btn_make_payment_singUp_four);
    }

    private void setClickListener()
    {
        btn_make_payment_singUp_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyPlanRequest();
            }
        });

        rcv_plan.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_plan, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.planID = mPlans.get(position).getId();
                BaseActivity.planPrice = mPlans.get(position).getCost();
                Log.e("planID",""+BaseActivity.planID);

                PreparePlanSelection(position);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void PreparePlanSelection(int position)
    {
        mPlanSelectionsList.clear();

        Log.e("SelectionList", "SelectionList size" + mPlanSelectionsList.size());
        for (int i = 0; i <= mPlans.size(); i++) {
            Log.e("SelectionList", "SelectionList" + i);
            if (i == position) {
                mPlanSelectionsList.add(1);
                BaseActivity.planID = mPlans.get(position).getId();
                BaseActivity.planPrice = mPlans.get(position).getCost();
            } else {
                mPlanSelectionsList.add(0);
            }
        }
    }

    private void sendPlansRequest()
    {
        String planURL = Constant.API_BASE_URL+"/plans";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.GET,planURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("PLANS_API ", response.toString());
                getPlanData(response.toString());
//                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getPlanData(String s)
    {
        try {
            JSONArray plan = new JSONArray();

            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    plan = jObject.getJSONArray("responseData");

                    for (int i = 0; i < plan.length(); i++) {
                        Plans data = new Plans();

                        data.setActive(plan.getJSONObject(i).getString(Constant.active));
                        data.setCost(plan.getJSONObject(i).getString(Constant.cost));
                        data.setCreatedAt(plan.getJSONObject(i).getString(Constant.createdAt));
                        data.setDescription(plan.getJSONObject(i).getString(Constant.description));
                        data.setDuration(plan.getJSONObject(i).getString(Constant.duration));
                        data.setDurationType(plan.getJSONObject(i).getString(Constant.durationType));
                        data.setId(plan.getJSONObject(i).getString(Constant.id));
                        data.setName(plan.getJSONObject(i).getString(Constant.name));
                        data.setUpdatedAt(plan.getJSONObject(i).getString(Constant.updatedAt));

                        mPlans.add(data);
                    }

                    BaseActivity.mPlans = mPlans;
                    initPlansRecycler();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initPlansRecycler()
    {
        PreparePlanSelection(0);

        rcv_plan.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_plan.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        adapter = new PlanAdapter(mContext, mPlans,mPlanSelectionsList);
        rcv_plan.setAdapter(adapter);
        rcv_plan.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
    }

    private void applyPlanRequest()
    {
        mContext.showWaitIndicator(true);

        String applyPlanURL = Constant.API_BASE_URL+"/user/"+BaseActivity.userID+"/applyplan";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST,applyPlanURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("APPLY_PLANS_API ", response.toString());
                getApplyPlanData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId",BaseActivity.userID);
                params.put("planId",BaseActivity.planID);
                params.put("lifestyleTypeId",BaseActivity.lifestyleTypeId);
                params.put("morphoTypeId",BaseActivity.morpheTypeId);

                Log.e("PLANS_PARAMS",""+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getApplyPlanData(String s)
    {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res){
                    String msg = jObject.getString(Constant.message);

                    Toast.makeText(mContext,""+msg,Toast.LENGTH_LONG).show();

                    Intent it = new Intent(mContext, PaymentActivity.class);
                    startActivity(it);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

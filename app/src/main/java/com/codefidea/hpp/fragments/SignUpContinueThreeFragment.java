package com.codefidea.hpp.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.adapters.LifeStyleAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.LifeStyle;
import com.codefidea.hpp.util.InternetStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class SignUpContinueThreeFragment extends Fragment
{
    SignUpPagerActivity mContext;

    RelativeLayout layout_signUp_three_main;

    private ArrayList<LifeStyle> mLifeStyle = new ArrayList<>();
    List<Integer> mLifeStyleSelectionsList = new ArrayList<>();

    RecyclerView rcv_life_style;

    boolean isInternet;

    LifeStyleAdapter adapter;

    int current;

    Button btn_signup_three;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = (SignUpPagerActivity) getActivity();
        return inflater.inflate(R.layout.fragment_signup_three, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getView(view);
        mContext.overrideFonts(mContext,layout_signUp_three_main);
        setClickListener();

        isInternet = new InternetStatus().isInternetOn(mContext);

        if(BaseActivity.mLifeStyle.size() > 0)
        {
            mLifeStyle = BaseActivity.mLifeStyle;
            initLifeStyleRecycler();
        }
        else
        {
            if (isInternet) {
                sendLifeStyleRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getView(View view)
    {
        layout_signUp_three_main = (RelativeLayout) view.findViewById(R.id.layout_signUp_three_main);

        rcv_life_style = (RecyclerView) view.findViewById(R.id.rcv_life_style);

        btn_signup_three = (Button) view.findViewById(R.id.btn_signup_three);
    }

    private void setClickListener()
    {
        btn_signup_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                current =  mContext.getItem(+1);

                sendCreateUserRequest();
            }
        });

        rcv_life_style.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_life_style, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.lifestyleTypeId = mLifeStyle.get(position).getId();
                Log.e("lifestyleTypeId",""+BaseActivity.lifestyleTypeId);

                PrepareLifeStyleSelection(position);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void PrepareLifeStyleSelection(int position) {
        mLifeStyleSelectionsList.clear();

        Log.e("SelectionList", "SelectionList size" + mLifeStyle.size());
        for (int i = 0; i <= mLifeStyle.size(); i++) {
            Log.e("SelectionList", "SelectionList" + i);
            if (i == position) {
                mLifeStyleSelectionsList.add(1);
            } else {
                mLifeStyleSelectionsList.add(0);
            }
        }
    }

    private void sendLifeStyleRequest() 
    {
        String lifeStylesURL = Constant.API_BASE_URL+"/lifestyles";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,lifeStylesURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("LIFE_STYLE_API ", response.toString());
                getLifeStyleData(response.toString());
//                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getLifeStyleData(String s)
    {
        try {
            JSONArray lifestyles = new JSONArray();

            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res){
                    lifestyles = jObject.getJSONArray("lifestyles");

                    for (int i = 0; i < lifestyles.length(); i++)
                    {
                        LifeStyle data = new LifeStyle();

                        data.setId(lifestyles.getJSONObject(i).getString(Constant.id));
                        data.setDescription(lifestyles.getJSONObject(i).getString(Constant.description));
                        data.setImage(lifestyles.getJSONObject(i).getString(Constant.image));
                        data.setName(lifestyles.getJSONObject(i).getString(Constant.name));

                        mLifeStyle.add(data);
                    }

                    BaseActivity.mLifeStyle = mLifeStyle;
                    initLifeStyleRecycler();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initLifeStyleRecycler()
    {
        PrepareLifeStyleSelection(0);

        rcv_life_style.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_life_style.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        adapter = new LifeStyleAdapter(mContext, mLifeStyle,mLifeStyleSelectionsList);
        rcv_life_style.setAdapter(adapter);
        rcv_life_style.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
    }

    private void sendCreateUserRequest()
    {
        mContext.showWaitIndicator(true);

        String createUserURL = Constant.API_BASE_URL+"/user/update";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,createUserURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CREATE_USER_API ", response.toString());
                getCreateData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId",BaseActivity.userID);
                params.put("name",BaseActivity.userName);
                params.put("surname",BaseActivity.surName);
                params.put("phone",BaseActivity.phone);
                params.put("email",BaseActivity.email);
                params.put("birthDate",BaseActivity.birthDate);
                params.put("sex",BaseActivity.sex);
                params.put("countryId",BaseActivity.countryID);
                params.put("cityId",BaseActivity.cityID);
                params.put("morpheTypeId",BaseActivity.morpheTypeId);
                params.put("lifestyleTypeId",BaseActivity.lifestyleTypeId);
                params.put("practiceSportsFlag",BaseActivity.practiceSportsFlag);
                params.put("sportsId",BaseActivity.sportsId);
                params.put("weekTimes",BaseActivity.weekTimes);
                params.put("isActive",BaseActivity.isActive);
                params.put("fiscalCode",BaseActivity.fiscalCode);
                params.put("aboutMe",BaseActivity.aboutMe);

                Log.e("PARAMS",""+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getCreateData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res){
                    BaseActivity.userID = jObject.getString("userid");
                    String msg = jObject.getString(Constant.message);

                    Toast.makeText(mContext,""+msg,Toast.LENGTH_LONG).show();

                    if (current <=  mContext.no_of_sample_tutorial -1) {
                        mContext.view_pager_signup_pager.setCurrentItem(current);
                    }
                }
                else
                {
                    Toast.makeText(mContext,""+jObject.getString(Constant.message),Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

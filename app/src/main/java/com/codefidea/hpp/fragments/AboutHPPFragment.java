package com.codefidea.hpp.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class AboutHPPFragment extends Fragment
{
    MainActivity mContext;

    boolean isInternet;

    WebView webview_about;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_abouthpp, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();

        setHasOptionsMenu(true);

        isInternet = new InternetStatus().isInternetOn(mContext);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        setView(view);

        if (isInternet) {
            String newsURL = Constant.API_BASE_URL+"/abouthpp";

            webview_about.loadUrl(newsURL);
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView(View view)
    {
        webview_about = view.findViewById(R.id.webview_about);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }
}

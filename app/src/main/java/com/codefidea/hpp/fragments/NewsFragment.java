package com.codefidea.hpp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.NewsDetailActivity;
import com.codefidea.hpp.activities.NewsSearchActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.NewsData;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class NewsFragment extends Fragment
{
    MainActivity mContext;

    RecyclerView rcv_news;

    private ArrayList<NewsData> mNewsList = new ArrayList<>();

    NewsAdapter itemsAdapter;

    boolean isInternet;

    public static boolean shouldRefreshOnResume = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        inItView(view);

//        Toast.makeText(mContext, "onCreate Call"+BaseActivity.mNewsList.size(), Toast.LENGTH_SHORT).show();

        if(BaseActivity.mNewsList.size() > 0)
        {
            mNewsList = BaseActivity.mNewsList;
            InitNewsRecyler();
        }
        else
        {
            if (isInternet) {
                sendNewsRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void inItView(View view) {
        rcv_news = (RecyclerView) view.findViewById(R.id.rcv_news);
    }

    private void InitNewsRecyler() {
        rcv_news.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_news.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        itemsAdapter = new NewsAdapter(mContext, mNewsList);
        rcv_news.setAdapter(itemsAdapter);
        rcv_news.setItemAnimator(new DefaultItemAnimator());
        itemsAdapter.notifyDataSetChanged();
    }

    private void sendNewsRequest()
    {
        if(mContext.isProgressVisible())
        {
            mContext.showWaitIndicator(false);
        }
        mContext.showWaitIndicator(true);

        String newsURL = Constant.API_BASE_URL+"/news/"+BaseActivity.userID+"/search";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("News_API ", response.toString());
                getNewsData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getNewsData(String response)
    {
        JSONArray newsList = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if(res)
                {
                    newsList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < newsList.length(); i++)
                    {
                        NewsData data = new NewsData();

                        data.setId(newsList.getJSONObject(i).getString(Constant.id));
                        data.setTitle(newsList.getJSONObject(i).getString(Constant.title));
                        data.setImage(newsList.getJSONObject(i).getString(Constant.image));
                        data.setBody(newsList.getJSONObject(i).getString(Constant.body));
                        data.setUserId(newsList.getJSONObject(i).getString(Constant.userId));
                        data.setCreatedAt(newsList.getJSONObject(i).getString(Constant.createdAt));
                        data.setUpdatedAt(newsList.getJSONObject(i).getString(Constant.updatedAt));
                        data.setUserFirstName(newsList.getJSONObject(i).getString(Constant.userFirstName));
                        data.setUserLastName(newsList.getJSONObject(i).getString(Constant.userLastName));
                        data.setUserProfilePic(newsList.getJSONObject(i).getString(Constant.userProfilePic));
                        data.setTotalLikes(newsList.getJSONObject(i).getString(Constant.totalLikes));
                        data.setIsLiked(newsList.getJSONObject(i).getString(Constant.isLiked));

                        mNewsList.add(data);
                    }

                    BaseActivity.mNewsList = mNewsList;

                    InitNewsRecyler();
                }
                else
                {

                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder>
    {
        private ArrayList<NewsData> mNewsList;

        MainActivity mContext;

        ImageLoader imageLoader;

        public class NewsHolder extends RecyclerView.ViewHolder
        {
            TextView txt_news_title,txt_new_sub_title;

            ImageView img_news, img_news_fav;

            CircleImageView image_news_thumb;

            RelativeLayout lay_list_news;

            ProgressBar news_progress;

            public NewsHolder(View view) {
                super(view);

                lay_list_news = (RelativeLayout) view.findViewById(R.id.lay_list_news);

                txt_news_title = (TextView) view.findViewById(R.id.txt_news_title);
                txt_new_sub_title = (TextView) view.findViewById(R.id.txt_new_sub_title);

                img_news = (ImageView) view.findViewById(R.id.img_news);
                image_news_thumb = (CircleImageView) view.findViewById(R.id.image_news_thumb);
                img_news_fav = (ImageView) view.findViewById(R.id.img_news_fav);

                news_progress = (ProgressBar) view.findViewById(R.id.news_progress);
            }
        }

        public NewsAdapter(MainActivity mContext, ArrayList<NewsData> mNewsList) {
            this.mNewsList = mNewsList;
            this.mContext = mContext;

            imageLoader = AppController.getInstance().getImageLoader();
        }

        @Override
        public NewsAdapter.NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View itemView = inflater.inflate(R.layout.news_row_layout_list, parent, false);
            NewsHolder newsVh = new NewsHolder(itemView);
            return newsVh;
        }

        @Override
        public void onBindViewHolder(final NewsHolder holder,final int position)
        {
            final NewsData mNews = mNewsList.get(position);

            holder.txt_news_title.setText(mNews.getTitle());
            holder.txt_new_sub_title.setText(mNews.getCreatedAt());

            mContext.overrideFonts(mContext,holder.lay_list_news);

            imageLoader.get(mNews.getImage(), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Image Load Error: " + error.getMessage());
                    holder.news_progress.setVisibility(View.GONE);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        holder.img_news.setImageBitmap(response.getBitmap());
                        holder.news_progress.setVisibility(View.GONE);
                    }
                }
            });

            imageLoader.get(mNews.getUserProfilePic(), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Image Load Error: " + error.getMessage());
//                    holder.news_progress.setVisibility(View.GONE);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        holder.image_news_thumb.setImageBitmap(response.getBitmap());
//                        holder.news_progress.setVisibility(View.GONE);
                    }
                }
            });

            if(mNews.getIsLiked().equalsIgnoreCase("1"))
            {
                holder.img_news_fav.setImageResource(R.drawable.ic_fav_selected);
            }
            else
            {
                holder.img_news_fav.setImageResource(R.drawable.ic_fav_unselect);
            }

            holder.lay_list_news.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.newsID = mNewsList.get(position).getId();
                    BaseActivity.comments.clear();

                    Intent it = new Intent(mContext, NewsDetailActivity.class);
                    mContext.startActivity(it);
                }
            });

            holder.img_news_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.newsID = mNewsList.get(position).getId();
                    BaseActivity.userID = mNewsList.get(position).getUserId();

//                    Toast.makeText(mContext,"POS : "+position+" FAVE : "+mNewsList.get(position).getIsLiked(),Toast.LENGTH_LONG).show();

                    if(mNewsList.get(position).getIsLiked().equalsIgnoreCase("1"))
                    {
                        //send unfavorite request
                        setUnFavoriteNews();
                    }
                    else
                    {
                        //send favorite request
                        sendFavoriteNews();
                    }
                }
            });
        }

        private void sendFavoriteNews() {
            if(mContext.isProgressVisible())
            {
                mContext.showWaitIndicator(false);
            }
//            mContext.showWaitIndicator(false);
            mContext.showWaitIndicator(true);

            String newsFavURL = Constant.API_BASE_URL+"/news/like";

            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("News_Favorite_API ", response.toString());
                    getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("newsId", BaseActivity.newsID);
                    params.put("userId", BaseActivity.userID);

                    return params;
                }
            };
            queue.add(sr);

            BaseActivity.mNewsList.clear();
            sendNewsRequest();
        }

        private void getFavoriteData(String s) {
            try {
                JSONObject jObject = new JSONObject(s.toString());

                if (jObject.toString() != null) {
                    boolean res = jObject.getBoolean("response");

                    if (res) {
                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private void setUnFavoriteNews()
        {
            if(mContext.isProgressVisible())
            {
                mContext.showWaitIndicator(false);
            }
//            mContext.showWaitIndicator(false);
            mContext.showWaitIndicator(true);

            String newsFavURL = Constant.API_BASE_URL+"/news/unlike";

            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("News_Favorite_API ", response.toString());
                    getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("newsId", BaseActivity.newsID);
                    params.put("userId", BaseActivity.userID);

                    return params;
                }
            };
            queue.add(sr);

            BaseActivity.mNewsList.clear();
            sendNewsRequest();
        }


        @Override
        public int getItemCount() {
            return mNewsList.size();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(true);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(false);

        MenuItem item = menu.findItem(R.id.action_search);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent it = new Intent(mContext, NewsSearchActivity.class);
                mContext.startActivity(it);
                return false;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }

     /*@Override
     public void onResume() {
            // TODO Auto-generated method stub
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                    if (doubleBackToExitPressedOnce) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        System.exit(1);
                    }

                    doubleBackToExitPressedOnce = true;
                    Toast.makeText(mContext , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                    return true;
                }
                return false;
            }
        });
    }*/

    @Override
    public void onResume() {
        super.onResume();
        // Check should we need to refresh the fragment
        if(shouldRefreshOnResume){
            // refresh fragment

//            Toast.makeText(mContext, "onResume", Toast.LENGTH_SHORT).show();
            BaseActivity.mNewsList.clear();
                if (isInternet) {
                    sendNewsRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }


        }
    }
}

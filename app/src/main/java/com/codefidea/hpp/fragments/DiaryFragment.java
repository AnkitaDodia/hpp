package com.codefidea.hpp.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.adapters.DairyMealsAdapter;
import com.codefidea.hpp.model.Meals;
import com.codefidea.hpp.model.MealsItem;
import com.codefidea.hpp.adapters.WeeklyDatesAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.WeeklyDates;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Raavan on 08-Mar-18.
 */

public class DiaryFragment extends Fragment {

    private String TAG = "DiaryFragment";
    MainActivity mContext;
    RecyclerView rv_meal, rv_weeklydates;

    LinearLayout ll_diary_parent, ll_meals_buttons, lay_breakfast, lay_lunch, lay_dinner;
    Button btn_diary_confirm;
    TextView txt_meals_title, empty_view;

    private ArrayList<WeeklyDates> mWeeklyDatesList = new ArrayList<>();
    ArrayList<Integer> mWeeklyDatesSelecList = new ArrayList<>();
    private ArrayList<Meals> mMealOption = new ArrayList<>();
    private ArrayList<MealsItem> mealsItem = new ArrayList<>();
    public WeeklyDatesAdapter mWeeklyDatesAdapter;
    private boolean isInternet;
    private String MealType = "";
    String msg;

    LinearLayout lay_allday;

    DairyMealsAdapter recyclerDataAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diary, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();

        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        inItView(view);

        isInternet = new InternetStatus().isInternetOn(mContext);
        mContext.overrideFonts(mContext, ll_diary_parent);

        if (isInternet) {
            getWeeklyDatesRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void inItView(View view) {

        rv_meal = (RecyclerView) view.findViewById(R.id.rv_meal);
        rv_weeklydates = (RecyclerView) view.findViewById(R.id.rv_weeklydates);

        ll_diary_parent = (LinearLayout) view.findViewById(R.id.ll_diary_parent);
        btn_diary_confirm = (Button) view.findViewById(R.id.btn_diary_confirm);
        ll_meals_buttons = (LinearLayout) view.findViewById(R.id.ll_meals_buttons);
        txt_meals_title = (TextView) view.findViewById(R.id.txt_meals_title);

        lay_breakfast = (LinearLayout) view.findViewById(R.id.lay_breakfast);
        lay_lunch = (LinearLayout) view.findViewById(R.id.lay_lunch);
        lay_dinner = (LinearLayout) view.findViewById(R.id.lay_dinner);

        empty_view = (TextView) view.findViewById(R.id.empty_view);

        lay_allday = (LinearLayout) view.findViewById(R.id.lay_allday);

        lay_allday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GetAllDayMeal();
            }
        });

        btn_diary_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternet) {
                    addEntryInDiary();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lay_breakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_meals_buttons.setVisibility(View.GONE);
                rv_meal.setVisibility(View.VISIBLE);
                txt_meals_title.setVisibility(View.VISIBLE);
                btn_diary_confirm.setVisibility(View.VISIBLE);

                MealType = "breakfast";

                mMealOption.clear();

                if (isInternet) {
                    sendMealOptionRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lay_lunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_meals_buttons.setVisibility(View.GONE);
                rv_meal.setVisibility(View.VISIBLE);
                txt_meals_title.setVisibility(View.VISIBLE);
                btn_diary_confirm.setVisibility(View.VISIBLE);

                MealType = "lunch";

                mMealOption.clear();

                if (isInternet) {
                    sendMealOptionRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });

        lay_dinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_meals_buttons.setVisibility(View.GONE);
                rv_meal.setVisibility(View.VISIBLE);
                txt_meals_title.setVisibility(View.VISIBLE);
                btn_diary_confirm.setVisibility(View.VISIBLE);

                MealType = "dinner";

                mMealOption.clear();

                if (isInternet) {
                    sendMealOptionRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }

            }
        });

        rv_weeklydates.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_weeklydates, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BaseActivity.SelectedDate = mWeeklyDatesList.get(position).getDate();
                Log.e(TAG, "" + BaseActivity.SelectedDate);

                PrepareWeeklyDatesSelection(position);
                mWeeklyDatesAdapter.notifyDataSetChanged();

                ll_meals_buttons.setVisibility(View.VISIBLE);
                rv_meal.setVisibility(View.GONE);
                txt_meals_title.setVisibility(View.GONE);
                btn_diary_confirm.setVisibility(View.GONE);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        rv_meal.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rv_meal, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Toast.makeText(mContext, "" + mMealOption.get(position).getOptionType(), Toast.LENGTH_SHORT).show();

                if (mMealOption.get(position).getOptionType().equalsIgnoreCase("Food")) {
                    //if item is food then feel the food array
                    BaseActivity.mFoodSelectedList.add(mMealOption.get(position).getOptionId());
                } else if (mMealOption.get(position).getOptionType().equalsIgnoreCase("Recipe")) {
                    //if item is recipe then feel the food array
                    BaseActivity.mRecipeSelectedList.add(mMealOption.get(position).getOptionId());
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        ll_meals_buttons.setVisibility(View.VISIBLE);
        rv_meal.setVisibility(View.GONE);
        txt_meals_title.setVisibility(View.GONE);
        btn_diary_confirm.setVisibility(View.GONE);

    }

    private void InitMealsRecyler() {
        if (mMealOption.size() > 0) {
            empty_view.setVisibility(View.GONE);
            rv_meal.setVisibility(View.VISIBLE);

            Log.e(TAG, "size : " + mMealOption.size());

            recyclerDataAdapter = new DairyMealsAdapter(mContext, mMealOption);
            rv_meal.setLayoutManager(new LinearLayoutManager(mContext));
            rv_meal.setAdapter(recyclerDataAdapter);
            rv_meal.setHasFixedSize(true);
        } else {
            empty_view.setVisibility(View.VISIBLE);
            rv_meal.setVisibility(View.GONE);
        }
    }

    private void getWeeklyDatesRequest() {
        try {
            mContext.showWaitIndicator(true);
            String WeeklyDatesURL = Constant.API_BASE_URL + "/diary/" + BaseActivity.userID + "/currentweek/status";

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.GET, WeeklyDatesURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, response.toString());
                    getWeeklyDatesData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            queue.add(sr);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getWeeklyDatesData(String response) {
        JSONArray WeeklyDatesArray = new JSONArray();

        mWeeklyDatesList.clear();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    WeeklyDatesArray = jObject.getJSONArray("responseData");

                    for (int i = 0; i < WeeklyDatesArray.length(); i++) {
                        WeeklyDates mWeeklyDates = new WeeklyDates();

                        mWeeklyDates.setDate(WeeklyDatesArray.getJSONObject(i).getString(Constant.date));
                        mWeeklyDates.setDay(WeeklyDatesArray.getJSONObject(i).getString(Constant.day));
                        mWeeklyDates.setDaystatus(WeeklyDatesArray.getJSONObject(i).getBoolean(Constant.flag));

                        mWeeklyDatesList.add(mWeeklyDates);
                    }
                    InitWeeklyDatesRecyler();
                    setCurrentDate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitWeeklyDatesRecyler() {
        mWeeklyDatesAdapter = new WeeklyDatesAdapter(mContext, mWeeklyDatesList, mWeeklyDatesSelecList);
        PrepareWeeklyDatesSelection(mWeeklyDatesAdapter.getPos());
        int width = getResources().getDisplayMetrics().widthPixels;
        mWeeklyDatesAdapter.setParentListView(width);
        rv_weeklydates.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        rv_weeklydates.setItemAnimator(new DefaultItemAnimator());
        rv_weeklydates.setAdapter(mWeeklyDatesAdapter);
    }

    private void setCurrentDate() {
        try {
            Date c = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            String formattedDate = df.format(c);
            Date stringCurrentDate = df.parse(formattedDate);

            for (int i = 0; i < mWeeklyDatesList.size(); i++) {
                Date getDate = df.parse(mWeeklyDatesList.get(i).getDate());
                if (stringCurrentDate.compareTo(getDate) == 0) {
                    PrepareWeeklyDatesSelection(i);
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrepareWeeklyDatesSelection(int position) {
        mWeeklyDatesSelecList.clear();

        for (int i = 0; i <= mWeeklyDatesList.size(); i++) {
            if (i == position) {
                mWeeklyDatesSelecList.add(1);
            } else {
                mWeeklyDatesSelecList.add(0);
            }
        }
        Log.e("SelectionList", "SelectionList" + mWeeklyDatesSelecList.toString());
    }

    private void sendMealOptionRequest() {
        try {
            if (mContext.isProgressVisible()) {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            } else {
                mContext.showWaitIndicator(true);
            }

            String mealOptionURL = Constant.API_BASE_URL + "/diary/mealoptions/" + BaseActivity.SelectedDate + "/" + MealType;
            Log.e("MEAL_OPTION_URL", "" + mealOptionURL);

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.GET, mealOptionURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("MEAL_OPTION_API ", response.toString());
                    getMealOptionData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }
            };
            queue.add(sr);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMealOptionData(String response) {
        JSONArray mealOptionArray = new JSONArray();
        JSONArray mIngredient = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");
                msg = jObject.getString("message");
                if (res) {
                    mMealOption.clear();

                    mealOptionArray = jObject.getJSONArray("responseData");

                    for (int i = 0; i < mealOptionArray.length(); i++) {
                        Meals mData = new Meals();

                        mData.setFoodName(mealOptionArray.getJSONObject(i).getString("foodName"));
                        mData.setOptionId(mealOptionArray.getJSONObject(i).getString("optionId"));
                        mData.setOptionType(mealOptionArray.getJSONObject(i).getString("optionType"));
                        mData.setQuantity(mealOptionArray.getJSONObject(i).getString("quantity"));
                        mData.setQuantityType(mealOptionArray.getJSONObject(i).getString("quantityType"));
                        mData.setRecipeId(mealOptionArray.getJSONObject(i).getString("recipeId"));
                        mData.setRecipeName(mealOptionArray.getJSONObject(i).getString("recipeName"));

                        mIngredient = mealOptionArray.getJSONObject(i).getJSONArray("ingredients");

                        if (mIngredient.length() > 0) {
                            mealsItem.clear();

                            for (int j = 0; j < mIngredient.length(); j++) {
                                MealsItem mealsItemData = new MealsItem();
                                mealsItemData.setId(mIngredient.getJSONObject(j).getString(Constant.id));
                                mealsItemData.setIngredient(mIngredient.getJSONObject(j).getString(Constant.ingredient));
                                mealsItem.add(mealsItemData);
                            }

                            mData.setIngredients(mealsItem);
                        }

                        mMealOption.add(mData);
                    }
                }

                InitMealsRecyler();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEntryInDiary() {
        try {
            if (mContext.isProgressVisible()) {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            } else {
                mContext.showWaitIndicator(true);
            }

            String addMealInDiary = Constant.API_BASE_URL + "/diary/addentry";
            Log.e("ADD_MEAL_OPTION", "" + addMealInDiary);

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.POST, addMealInDiary, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("MEAL_ADD_API ", response.toString());
                    addMealInDiaryData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("userId", BaseActivity.userID);
                    params.put("date", BaseActivity.SelectedDate);
                    params.put("mealType", MealType);
                    params.put("recipes[]", BaseActivity.mRecipeSelectedList.toString());
                    params.put("foods[]", BaseActivity.mFoodSelectedList.toString());

                    Log.e("put_recipe", "foods : " + BaseActivity.mRecipeSelectedList.size());
                    Log.e("put_food", "foods : " + BaseActivity.mFoodSelectedList.size());

//                    for(String object: BaseActivity.mRecipeSelectedList){
//                        params.put("recipes[]", object);
//                        Log.e("put_recipes","recipes[] : "+object);
//                        // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
//                    }
//
//                    for(String object: BaseActivity.mFoodSelectedList){
//                        params.put("foods[]", object);
//                        Log.e("put_food","foods : "+object);
//                        // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
//                    }

                    Log.e("PARAMS", "" + params);
                    return params;
                }
            };
            queue.add(sr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addMealInDiaryData(String s) {
        BaseActivity.mFoodSelectedList.clear();
        BaseActivity.mRecipeSelectedList.clear();

        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    Toast.makeText(mContext, "" + jObject.getString("message"), Toast.LENGTH_LONG).show();

                    ll_meals_buttons.setVisibility(View.VISIBLE);
                    rv_meal.setVisibility(View.GONE);
                    txt_meals_title.setVisibility(View.GONE);
                    btn_diary_confirm.setVisibility(View.GONE);
                    empty_view.setVisibility(View.GONE);
                } else {
                    Toast.makeText(mContext, "" + jObject.getString("message"), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetAllDayMeal() {

        try {
            if (mContext.isProgressVisible()) {
                mContext.showWaitIndicator(false);
                mContext.showWaitIndicator(true);
            } else {
                mContext.showWaitIndicator(true);
            }
            //http://api.foodporntheory.com/diary/mealoptions/1/2018-04-30
            String GetMealOptionDiary = Constant.API_BASE_URL + "/diary/mealoptions/"+BaseActivity.userID+"/2018-04-30";
            Log.e("GetMealOptionDiary_API", "" + GetMealOptionDiary);

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.POST, GetMealOptionDiary, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("GetMealOptionDiary_API ", response.toString());
//                    GetMealOptionDiary(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();



                    Log.e("PARAMS", "" + params);
                    return params;
                }
            };
            queue.add(sr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void GetMealOptionDiary(String response) {
        JSONArray mealOptionArray = new JSONArray();
        JSONArray mIngredient = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");
                msg = jObject.getString("message");
                if (res) {
                    mMealOption.clear();

                    mealOptionArray = jObject.getJSONArray("responseData");

                    for (int i = 0; i < mealOptionArray.length(); i++) {
                        Meals mData = new Meals();

                        mData.setFoodName(mealOptionArray.getJSONObject(i).getString("foodName"));
                        mData.setOptionId(mealOptionArray.getJSONObject(i).getString("optionId"));
                        mData.setOptionType(mealOptionArray.getJSONObject(i).getString("optionType"));
                        mData.setQuantity(mealOptionArray.getJSONObject(i).getString("quantity"));
                        mData.setQuantityType(mealOptionArray.getJSONObject(i).getString("quantityType"));
                        mData.setRecipeId(mealOptionArray.getJSONObject(i).getString("recipeId"));
                        mData.setRecipeName(mealOptionArray.getJSONObject(i).getString("recipeName"));

                        mIngredient = mealOptionArray.getJSONObject(i).getJSONArray("ingredients");

                        if (mIngredient.length() > 0) {
                            mealsItem.clear();

                            for (int j = 0; j < mIngredient.length(); j++) {
                                MealsItem mealsItemData = new MealsItem();
                                mealsItemData.setId(mIngredient.getJSONObject(j).getString(Constant.id));
                                mealsItemData.setIngredient(mIngredient.getJSONObject(j).getString(Constant.ingredient));
                                mealsItem.add(mealsItemData);
                            }

                            mData.setIngredients(mealsItem);
                        }

                        mMealOption.add(mData);
                    }
                }

                InitMealsRecyler();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }

}

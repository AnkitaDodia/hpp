package com.codefidea.hpp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.ChatActivity;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.StaffListActivity;
import com.codefidea.hpp.adapters.ChatAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.RecetChats;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raavan on 08-Mar-18.
 */

public class ChatFragment extends Fragment {

    MainActivity mContext;
    private boolean isInternet;

    RecyclerView rcv_chat;
    ChatAdapter mChatAdapter;
    private ArrayList<RecetChats> mRecetChatsList = new ArrayList<>();
    LinearLayout ll_chat_parent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();
        isInternet = new InternetStatus().isInternetOn(mContext);

        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        inItView(view);

        mContext.overrideFonts(mContext,ll_chat_parent);

        if (isInternet) {
            getRecentChatsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void inItView(View view) {

        rcv_chat = (RecyclerView) view.findViewById(R.id.rcv_chat);
        ll_chat_parent  = (LinearLayout) view.findViewById(R.id.ll_chat_parent);

        rcv_chat.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_chat, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Log.e("ChatMessagesActivity", "pos :  "+position+" fromuserid : "+mRecetChatsList.get(position).getFromUserId());

                Intent it = new Intent(mContext, ChatActivity.class);
                it.putExtra("fromid", mRecetChatsList.get(position).getFromUserId());
                it.putExtra("fromname", mRecetChatsList.get(position).getFirstName());
                mContext.startActivity(it);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void InitRecetChatsRecyler() {
        rcv_chat.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_chat.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        mChatAdapter = new ChatAdapter(mContext, mRecetChatsList);
        rcv_chat.setAdapter(mChatAdapter);
        rcv_chat.setItemAnimator(new DefaultItemAnimator());
        mChatAdapter.notifyDataSetChanged();
    }


    private void getRecentChatsRequest()
    {
        try {
            mContext.showWaitIndicator(true);
//            http://api.foodporntheory.com/messages/recent/1
            String RecentChatsURL = Constant.API_BASE_URL+"/messages/recent/"+BaseActivity.userID;

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.GET, RecentChatsURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("RECIPE_API ", response.toString());
                    getRecentChatsData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

//                    params.put("time", BaseActivity.time);
//                    params.put("difficulty", BaseActivity.difficulty);
//                    params.put("cuisineId", BaseActivity.cuisineId);
//                    params.put("ingredients[]", BaseActivity.ingredients.toString());

                    return params;
                }
            };
            queue.add(sr);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void getRecentChatsData(String response)
    {
        JSONArray recentchatlist = new JSONArray();
        mRecetChatsList.clear();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if(res)
                {
                    recentchatlist  = jObject.getJSONArray("responseData");

                    for (int i = 0; i < recentchatlist.length(); i++) {
                        RecetChats data = new RecetChats();

                        data.setId(recentchatlist.getJSONObject(i).getString(Constant.id));
                        data.setFromUserId(recentchatlist.getJSONObject(i).getString(Constant.fromUserId));
                        data.setToUserId(recentchatlist.getJSONObject(i).getString(Constant.toUserId));
                        data.setFirstName(recentchatlist.getJSONObject(i).getString(Constant.firstName));
                        data.setLastname(recentchatlist.getJSONObject(i).getString(Constant.lastname));
                        data.setMessage(recentchatlist.getJSONObject(i).getString(Constant.message));
                        data.setCreatedAt(recentchatlist.getJSONObject(i).getString(Constant.createdAt));
                        data.setProfilePic(recentchatlist.getJSONObject(i).getString(Constant.profilePic));

                        mRecetChatsList.add(data);
                    }

                    setListData();
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        if(mRecetChatsList.size() > 0)
        {
            InitRecetChatsRecyler();
        }
        else
        {
//            error_layout_topic.setVisibility(View.VISIBLE);
//            list_topic.setVisibility(View.GONE);
//            error_msg.setText(msg);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(true);

        menu.findItem(R.id.action_empty).setVisible(false);

        MenuItem item = menu.findItem(R.id.action_profile);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent it = new Intent(mContext, StaffListActivity.class);
                mContext.startActivity(it);
                return false;
            }
        });

        super.onPrepareOptionsMenu(menu);
    }
}

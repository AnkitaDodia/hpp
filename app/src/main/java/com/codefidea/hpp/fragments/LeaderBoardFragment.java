package com.codefidea.hpp.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.adapters.LeaderBoardAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.LeaderBoard;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class LeaderBoardFragment extends Fragment
{
    MainActivity mContext;

    LinearLayout leader_board_main;

    RecyclerView rcv_leader;

    LeaderBoardAdapter itemsAdapter;

    private ArrayList<LeaderBoard> mLeaderBoardList = new ArrayList<>();

    private boolean isInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_leaderboard, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();

        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        setView(view);
        mContext.overrideFonts(mContext,leader_board_main);

        isInternet = new InternetStatus().isInternetOn(mContext);


        if (BaseActivity.mLeaderBoardList.size() > 0) {
            mLeaderBoardList = BaseActivity.mLeaderBoardList;
            InitLeaderBoardRecyler();
        } else {
            if (isInternet) {
                GetLeaderboardRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void setView(View view)
    {
        leader_board_main = (LinearLayout) view.findViewById(R.id.leader_board_main);

        rcv_leader = (RecyclerView) view.findViewById(R.id.rcv_leader);
    }

    private void InitLeaderBoardRecyler() {
        rcv_leader.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_leader.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        itemsAdapter = new LeaderBoardAdapter(mContext, mLeaderBoardList);
        rcv_leader.setAdapter(itemsAdapter);
        rcv_leader.setItemAnimator(new DefaultItemAnimator());
        itemsAdapter.notifyDataSetChanged();

    }

    private void GetLeaderboardRequest() {
        mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/leaderboard
        String newsURL = Constant.API_BASE_URL + "/leaderboard";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("leaderboard_API ", response.toString());
                getLeaderboardData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getLeaderboardData(String response) {
        JSONArray LeaderboardList = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    LeaderboardList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < LeaderboardList.length(); i++) {
                        LeaderBoard mLeaderBoard = new LeaderBoard();

                        mLeaderBoard.setId(LeaderboardList.getJSONObject(i).getString(Constant.id));
                        mLeaderBoard.setFirstName(LeaderboardList.getJSONObject(i).getString(Constant.firstName));
                        mLeaderBoard.setLastName(LeaderboardList.getJSONObject(i).getString(Constant.lastName));
                        mLeaderBoard.setPoints(LeaderboardList.getJSONObject(i).getString(Constant.points));
                        mLeaderBoard.setProfilePic(LeaderboardList.getJSONObject(i).getString(Constant.profilePic));

                        mLeaderBoardList.add(mLeaderBoard);
                    }

                    BaseActivity.mLeaderBoardList = mLeaderBoardList;
                    InitLeaderBoardRecyler();
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }
}

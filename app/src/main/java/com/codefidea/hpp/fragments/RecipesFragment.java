package com.codefidea.hpp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.NewsSearchActivity;
import com.codefidea.hpp.activities.RecipeDetailsActivity;
import com.codefidea.hpp.activities.RecipesFilterActivity;
import com.codefidea.hpp.adapters.RecipesAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.RecipesData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;;import static android.app.Activity.RESULT_OK;

/**
 * Created by Raavan on 07-Mar-18.
 */

public class RecipesFragment extends Fragment {
    private String TAG = "RecipesFragment";
    MainActivity mContext;
    private boolean isInternet;

    RecyclerView rcv_recipe;
    LinearLayout lay_recipes_filter;

    private ArrayList<RecipesData> mRecipesList = new ArrayList<>();
    RecipesAdapter mRecipesAdapter;

    Boolean isViewGrid = true;
    public static final int FILTER_REQUEST_CODE = 1;
    TextView empty_view;
    String msg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recipes, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();
        isInternet = new InternetStatus().isInternetOn(mContext);

        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.GONE);
        mContext.ll_app_bar_button.setVisibility(View.VISIBLE);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isViewGrid = bundle.getBoolean("isViewGrid", true);
        }

        inItView(view);

//        if(BaseActivity.RecipesList.size() > 0)
//        {
//            mRecipesList = BaseActivity.RecipesList;
//            setListData();
//        }
//        else
//        {
            if (isInternet) {
                getRecipeRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
//        }

//        InitRecipeRecyler();
    }

    private void inItView(View view) {
        rcv_recipe = (RecyclerView) view.findViewById(R.id.rcv_recipe);
        lay_recipes_filter = (LinearLayout) view.findViewById(R.id.lay_recipes_filter);

        empty_view = (TextView) view.findViewById(R.id.empty_view);
        empty_view.setTypeface(mContext.getTypeFace());

        lay_recipes_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                startActivity(new Intent(mContext, RecipesFilterActivity.class));

                Intent intent_cuisines = new Intent(mContext, RecipesFilterActivity.class);
                startActivityForResult(intent_cuisines, FILTER_REQUEST_CODE);

            }
        });

        mContext.btn_recipe_grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isViewGrid = false;
                InitRecipeRecyler();

                mContext.btn_recipe_grid.setBackgroundResource(R.drawable.ic_grid_selected);
                mContext.btn_recipe_list.setBackgroundResource(R.drawable.ic_list_normal);

            }
        });

        mContext.btn_recipe_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isViewGrid = true;
                InitRecipeRecyler();

                mContext.btn_recipe_grid.setBackgroundResource(R.drawable.ic_grid_normal);
                mContext.btn_recipe_list.setBackgroundResource(R.drawable.ic_list_selected);
            }
        });

        rcv_recipe.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, rcv_recipe, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

//                mRecipesList.get(position).getId();
                BaseActivity.RecipesID = mRecipesList.get(position).getId();
                Intent it = new Intent(mContext, RecipeDetailsActivity.class);
                it.putExtra("id",mRecipesList.get(position).getId());
                mContext.startActivity(it);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void InitRecipeRecyler() {
        mRecipesAdapter = new RecipesAdapter(mContext, mRecipesList, isViewGrid);
        rcv_recipe.setLayoutManager(isViewGrid ? new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false) : new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        rcv_recipe.setItemAnimator(new DefaultItemAnimator());
        rcv_recipe.setAdapter(mRecipesAdapter);
    }

    private void PostRecipeRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            String recipeURL = Constant.API_BASE_URL+"/recipes/"+BaseActivity.userID+"/search";

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,recipeURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "PostRecipeRequest : "+response.toString());
                    getRecipeData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    Log.e(TAG, "time :"+BaseActivity.Time);
                    Log.e(TAG,"difficulty :"+BaseActivity.Difficulty);
                    Log.e(TAG,"CuisineId :"+BaseActivity.CuisineID);

                    params.put("time", BaseActivity.Time);
                    params.put("difficulty", BaseActivity.Difficulty);
                    params.put("cuisineId", BaseActivity.CuisineID);

                    for(String object: BaseActivity.mIngredientsSelectedList){

                        Log.e(TAG,"ingredients : "+object);
                        params.put("ingredients[]", object);
                        // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
                    }
                    return params;
                }
            };
            queue.add(sr);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void getRecipeRequest()
    {
        try {
            mContext.showWaitIndicator(true);

            String recipeURL = Constant.API_BASE_URL+"/recipes/"+BaseActivity.userID+"/search";

            RequestQueue queue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.GET,recipeURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "RECIPE_API : "+response.toString());
                    getRecipeData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

//                    params.put("time", BaseActivity.time);
//                    params.put("difficulty", BaseActivity.difficulty);
//                    params.put("cuisineId", BaseActivity.cuisineId);
//                    params.put("ingredients[]", BaseActivity.ingredients.toString());

                    return params;
                }
            };
            queue.add(sr);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void getRecipeData(String response)
    {
        JSONArray recipeList = new JSONArray();
        JSONArray recipeImageList = new JSONArray();

        mRecipesList.clear();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");
                msg = jObject.getString("message");
                if(res)
                {
                    recipeList  = jObject.getJSONArray("responseData");

                    for (int i = 0; i < recipeList.length(); i++) {
                        RecipesData data = new RecipesData();

                        data.setId(recipeList.getJSONObject(i).getString(Constant.id));
                        data.setName(recipeList.getJSONObject(i).getString(Constant.name));
                        data.setCuisineId(recipeList.getJSONObject(i).getString(Constant.cuisineId));
                        data.setPreparationTime(recipeList.getJSONObject(i).getString(Constant.preparationTime));
                        data.setDifficulty(recipeList.getJSONObject(i).getString(Constant.difficulty));
                        data.setRating(recipeList.getJSONObject(i).getString(Constant.rating));
                        data.setDescription(recipeList.getJSONObject(i).getString(Constant.description));
                        data.setUserId(recipeList.getJSONObject(i).getString(Constant.userId));
                        data.setCreatedAt(recipeList.getJSONObject(i).getString(Constant.createdAt));
                        data.setUpdatedAt(recipeList.getJSONObject(i).getString(Constant.updatedAt));
                        data.setCuisineName(recipeList.getJSONObject(i).getString(Constant.cuisineName));
                        data.setUserFirstName(recipeList.getJSONObject(i).getString(Constant.userFirstName));
                        data.setUserLastName(recipeList.getJSONObject(i).getString(Constant.userLastName));
                        data.setUserProfilePic(recipeList.getJSONObject(i).getString(Constant.userProfilePic));
                        data.setTotalLikes(recipeList.getJSONObject(i).getString(Constant.totalLikes));
                        data.setIsLiked(recipeList.getJSONObject(i).getString(Constant.isLiked));

                        recipeImageList = recipeList.getJSONObject(i).getJSONArray("images");

                        if(recipeImageList.length() > 0)
                        {
                            data.setImage(recipeImageList.getJSONObject(0).getString(Constant.image));
                        }

                        mRecipesList.add(data);
                    }

//                    BaseActivity.RecipesList = mRecipesList;

                }
                else {

                }

                setListData();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setListData()
    {
        if(mRecipesList.size() > 0)
        {

            empty_view.setVisibility(View.GONE);
            rcv_recipe.setVisibility(View.VISIBLE);

            Log.e(TAG,"size : "+mRecipesList.size());
            InitRecipeRecyler();
        }
        else
        {
            empty_view.setVisibility(View.VISIBLE);
            rcv_recipe.setVisibility(View.GONE);
            empty_view.setText(msg);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == FILTER_REQUEST_CODE && resultCode == RESULT_OK) {

                String FilterStatus = data.getStringExtra("FilterStatus");

                if(FilterStatus.equals("Apply")){


                    if (isInternet) {
                        PostRecipeRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }


//                    Toast.makeText(mContext, "Result :   "+FilterStatus, Toast.LENGTH_SHORT).show();

                }else if(FilterStatus.equals("Cancle")){

                    if (isInternet) {
                        getRecipeRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(mContext, "Result :   "+FilterStatus, Toast.LENGTH_SHORT).show();

                }else if(FilterStatus.equals("Reset")){


//                    Toast.makeText(mContext, "Result :   "+FilterStatus, Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception ex) {
            Log.e("Recipefilter", "" + ex.toString());
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(true);


//        MenuItem item = menu.findItem(R.id.action_search);

//        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                Intent it = new Intent(mContext, RecipesSearchActivity.class);
//                mContext.startActivity(it);
//                return false;
//            }
//        });
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }
}

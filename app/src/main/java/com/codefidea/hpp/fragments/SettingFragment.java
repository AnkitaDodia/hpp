package com.codefidea.hpp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.LoginActivity;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.AppSettings;
import com.codefidea.hpp.util.SettingsPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class SettingFragment extends Fragment
{
    MainActivity mContext;
    boolean isInternet;

    LinearLayout linear_settings_main, ll_delete_account;
    Switch switch_relevant, switch_optimize, switch_notification, switch_allow;
    AppSettings mAppSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();
        setHasOptionsMenu(true);

        isInternet = new InternetStatus().isInternetOn(mContext);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        setView(view);
        mContext.overrideFonts(mContext,linear_settings_main);

        if (isInternet) {
            GetSettingsRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }

    }

    private void setView(View view)
    {
        linear_settings_main = (LinearLayout) view.findViewById(R.id.linear_settings_main);
        ll_delete_account = (LinearLayout) view.findViewById(R.id.ll_delete_account);

        switch_relevant = (Switch) view.findViewById(R.id.switch_relevant);
        switch_optimize = (Switch) view.findViewById(R.id.switch_optimize);
        switch_notification = (Switch) view.findViewById(R.id.switch_notification);
        switch_allow = (Switch) view.findViewById(R.id.switch_allow);

        switch_relevant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked) {
                    //do stuff when Switch is ON
                    mAppSettings.setShowRelevantFirst("1");
                } else {
                    //do stuff when Switch if OFF
                    mAppSettings.setShowRelevantFirst("0");
                }

                PostSettingsRequest();

            }
        });

        switch_optimize.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked) {
                    //do stuff when Switch is ON
                    mAppSettings.setOptimizeImages("1");
                } else {
                    //do stuff when Switch if OFF
                    mAppSettings.setOptimizeImages("0");
                }

                PostSettingsRequest();
            }
        });

        switch_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked) {
                    //do stuff when Switch is ON
                    mAppSettings.setNotifications("1");
                } else {
                    //do stuff when Switch if OFF
                    mAppSettings.setNotifications("0");
                }

                PostSettingsRequest();
            }
        });

        switch_allow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked) {
                    //do stuff when Switch is ON
                    mAppSettings.setAllowSaving("1");
                } else {
                    //do stuff when Switch if OFF
                    mAppSettings.setAllowSaving("0");
                }

                PostSettingsRequest();
            }
        });

        ll_delete_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DeleteAccountRequest();
            }
        });

    }

    private void GetSettingsRequest()
    {
        mContext.showWaitIndicator(true);

//        http://api.foodporntheory.com/user/1/settings
        String newsURL = Constant.API_BASE_URL+"/user/"+ BaseActivity.userID+"/settings";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.GET,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("GET_Settings_API ", response.toString());
                getSettingsData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void PostSettingsRequest()
    {
        mContext.showWaitIndicator(true);

//        http://api.foodporntheory.com/user/1/settings
        String newsURL = Constant.API_BASE_URL+"/user/"+ BaseActivity.userID+"/settings";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("POST_Settings_API ", response.toString());
                getSettingsData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId", BaseActivity.userID);
                params.put("showRelevantFirst", mAppSettings.getShowRelevantFirst());
                params.put("optimizeImages", mAppSettings.getOptimizeImages());
                params.put("notifications", mAppSettings.getNotifications());
                params.put("allowSaving", mAppSettings.getAllowSaving());

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getSettingsData(String response) {
        JSONObject SettingsList = new JSONObject();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
//                    SettingsList = jObject.getJSONArray("responseData");
                    SettingsList = jObject.getJSONObject("responseData");

                    mAppSettings = new AppSettings();

                    mAppSettings.setShowRelevantFirst(SettingsList.getString(Constant.showRelevantFirst));
                    mAppSettings.setAllowSaving(SettingsList.getString(Constant.allowSaving));
                    mAppSettings.setNotifications(SettingsList.getString(Constant.notifications));
                    mAppSettings.setOptimizeImages(SettingsList.getString(Constant.optimizeImages));

                    SetData(mAppSettings);


                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetData(AppSettings mAppSettings){

        if(mAppSettings.getShowRelevantFirst().equalsIgnoreCase("1")){
            switch_relevant.setChecked(true);
        }else{
            switch_relevant.setChecked(false);
        }

        if(mAppSettings.getOptimizeImages().equalsIgnoreCase("1")){
            switch_optimize.setChecked(true);
        }else{
            switch_optimize.setChecked(false);
        }

        if(mAppSettings.getNotifications().equalsIgnoreCase("1")){
            switch_notification.setChecked(true);
        }else{
            switch_notification.setChecked(false);
        }

        if(mAppSettings.getAllowSaving().equalsIgnoreCase("1")){
            switch_allow.setChecked(true);
        }else{
            switch_allow.setChecked(false);
        }

    }

    private void DeleteAccountRequest(){

        mContext.showWaitIndicator(true);

//       http://api.foodporntheory.com/user/delete
        String DeleteAccounURL = Constant.API_BASE_URL+"/user/delete";

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, DeleteAccounURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DeleteAccoun_API ", response.toString());
                getDeleteAccountData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId", BaseActivity.userID);//BaseActivity.userID

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getDeleteAccountData(String response) {
        JSONObject DeleteAccountList = new JSONObject();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
//                    DeleteAccountList = jObject.getString("responseData");

                    Toast.makeText(mContext,""+jObject.getString("message"), Toast.LENGTH_SHORT).show();

                    mContext.setLogin(0);
                    SettingsPreferences.clearDB(mContext);

                    Intent it = new Intent(mContext,LoginActivity.class);
                    startActivity(it);
                    mContext.finish();

                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(false);
        menu.findItem(R.id.action_empty).setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }
}

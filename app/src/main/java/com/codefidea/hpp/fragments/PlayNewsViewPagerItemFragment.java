package com.codefidea.hpp.fragments;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.NewsPlayActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;

import java.util.List;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class PlayNewsViewPagerItemFragment extends Fragment
{
    NewsPlayActivity mContext;

    boolean isInternet;

    ImageView viewpager_item_image;

    static String url;

    public static Fragment newInstance(String path) {
        PlayNewsViewPagerItemFragment fragment = new PlayNewsViewPagerItemFragment();
        url = path;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_paly_news_view_pager_item, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (NewsPlayActivity) getActivity();

        isInternet = new InternetStatus().isInternetOn(mContext);

        Log.e("PATH",url);

        setView(view);
        setClick();
    }

    private void setView(View view)
    {
        viewpager_item_image = view.findViewById(R.id.viewpager_item_image);
    }

    private void setClick() {
        viewpager_item_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternet) {
                    watchYoutubeVideo(mContext,url);
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void watchYoutubeVideo(NewsPlayActivity context, String id){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(id));
        intent.setPackage("com.google.android.youtube");
        context.startActivity(intent);
    }
}

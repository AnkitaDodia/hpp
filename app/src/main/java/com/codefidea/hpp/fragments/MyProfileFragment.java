package com.codefidea.hpp.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.CompleteQuestionaryActivity;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.StaffListActivity;
import com.codefidea.hpp.activities.WeightCeilingActivity;
import com.codefidea.hpp.activities.PhysicalDataActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.AndroidMultiPartEntity;
import com.codefidea.hpp.util.SettingsPreferences;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raavan on 08-Mar-18.
 */

public class MyProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    MainActivity mContext;

    LinearLayout lay_physicaldata, lay_weightceiling, lay_completeque;

    CoordinatorLayout lay_parent_my_profile;
    private FloatingActionButton fab_camera;

    CircleImageView img_profile_person;
    TextView txt_person_name, txt_person_address, txt_about_person;

    private static final int PICK_IMAGE = 1;
    Bitmap bitmap, personBitmap;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;

    String filemanagerstring, selectedImagePath = null;
    ImageLoader imageLoader;
    boolean isAllowEdit;
    EditText edt_person_name, edt_person_address, edt_about_person;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_myprofile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = (MainActivity) getActivity();
        imageLoader = AppController.getInstance().getImageLoader();
        setHasOptionsMenu(true);

        mContext.ll_app_bar_title.setVisibility(View.VISIBLE);
        mContext.ll_app_bar_button.setVisibility(View.GONE);

        inItView(view);
        setClickListener();
        mContext.overrideFonts(mContext, lay_parent_my_profile);

//        ChangeView();
    }

    private void ChangeView() {

        if (isAllowEdit) {

            edt_person_name.setVisibility(View.VISIBLE);
            edt_person_address.setVisibility(View.VISIBLE);
            edt_about_person.setVisibility(View.VISIBLE);

            edt_person_name.setText(txt_person_name.getText().toString());
            edt_person_address.setText(txt_person_address.getText().toString());
            edt_about_person.setText(txt_about_person.getText().toString());

            txt_person_name.setVisibility(View.GONE);
            txt_person_address.setVisibility(View.GONE);
            txt_about_person.setVisibility(View.GONE);

        } else {

            edt_person_name.setVisibility(View.GONE);
            edt_person_address.setVisibility(View.GONE);
            edt_about_person.setVisibility(View.GONE);

            txt_person_name.setVisibility(View.VISIBLE);
            txt_person_address.setVisibility(View.VISIBLE);
            txt_about_person.setVisibility(View.VISIBLE);

            txt_person_name.setText(edt_person_name.getText().toString());
            txt_person_address.setText(edt_person_address.getText().toString());
            txt_about_person.setText(edt_about_person.getText().toString());

        }

    }

    private void inItView(View view) {

        lay_parent_my_profile = (CoordinatorLayout) view.findViewById(R.id.lay_parent_my_profile);

        lay_physicaldata = (LinearLayout) view.findViewById(R.id.lay_physicaldata);
        lay_weightceiling = (LinearLayout) view.findViewById(R.id.lay_weightceiling);
        lay_completeque = (LinearLayout) view.findViewById(R.id.lay_completeque);

        img_profile_person = (CircleImageView) view.findViewById(R.id.img_profile_person);

        txt_person_name = (TextView) view.findViewById(R.id.txt_person_name);
        txt_person_address = (TextView) view.findViewById(R.id.txt_person_address);
        txt_about_person = (TextView) view.findViewById(R.id.txt_about_person);

        txt_person_name.setText(SettingsPreferences.getConsumer(mContext).getName());
        txt_person_address.setText(SettingsPreferences.getConsumer(mContext).getAddress());
        txt_about_person.setText(SettingsPreferences.getConsumer(mContext).getAboutMe());

        String imagePath = SettingsPreferences.getConsumer(mContext).getProfilePic();

        imageLoader.get(imagePath, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    img_profile_person.setImageBitmap(response.getBitmap());
                }
            }
        });

        fab_camera = (FloatingActionButton) view.findViewById(R.id.fab_camera);

        edt_person_name = (EditText) view.findViewById(R.id.edt_person_name);
        edt_person_address = (EditText) view.findViewById(R.id.edt_person_address);
        edt_about_person = (EditText) view.findViewById(R.id.edt_about_person);
    }

    private void setClickListener() {
        lay_physicaldata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, PhysicalDataActivity.class));
            }
        });

        lay_weightceiling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, WeightCeilingActivity.class));
            }
        });

        lay_completeque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, CompleteQuestionaryActivity.class));
            }
        });

        img_profile_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isAllowEdit) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                            && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        AskForPermission();
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, PICK_IMAGE);
                    }

                }
            }
        });

        fab_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(mContext, "Clicked on FAB", Toast.LENGTH_SHORT).show();
                showPickDialog();
            }
        });
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        String createUserURL = Constant.API_BASE_URL + "/user/update";
        long totalSize = 0;

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            mContext.showWaitIndicator(true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(createUserURL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(selectedImagePath);

                // Adding file data to http body
                entity.addPart("photo", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
                entity.addPart("userId", new StringBody(BaseActivity.userID));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("UPLOAD_IMAGE", "Response from server: " + result);
            mContext.showWaitIndicator(false);

            getUserUpdateData(result);
            super.onPostExecute(result);
        }
    }

    private void getUserUpdateData(String result) {
        try {
            JSONObject jObject = new JSONObject(result.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    BaseActivity.userID = jObject.getString("userid");
                    Toast.makeText(mContext, "" + jObject.getString(Constant.message), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AskForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {

        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = mContext.managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void decodeFile(String filePath, int code) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        if (code == 1) {
            bitmap = BitmapFactory.decodeFile(filePath, o2);
            personBitmap = bitmap;
            img_profile_person.setImageBitmap(bitmap);

            new UploadFileToServer().execute();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
        super.onActivityResult(requestCode, requestcode, data);

        if (requestcode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            String filePath = null;

            try {
                // OI FILE Manager
                filemanagerstring = selectedImageUri.getPath();

                // MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);
                Log.e("selectedImagePath", "path : " + selectedImagePath);

                if (selectedImagePath != null) {
                    filePath = selectedImagePath;
                } else if (filemanagerstring != null) {
                    filePath = filemanagerstring;
                } else {
                    Toast.makeText(mContext, "Unknown path", Toast.LENGTH_LONG).show();
                    Log.e("Bitmap", "Unknown path");
                }

                if (filePath != null) {
                    decodeFile(filePath, 1);
                } else {
                    bitmap = null;
                }

            } catch (Exception e) {
                Toast.makeText(mContext, "Internal Error", Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
            }
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Requests given permission.
     * If the permission has been denied previously, a Dialog will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    protected void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mContext, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(mContext,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions(mContext, new String[]{permission}, requestCode);
        }
    }

    /**
     * This method shows dialog with given title & message.
     * Also there is an option to pass onClickListener for positive & negative button.
     *
     * @param title                         - dialog title
     * @param message                       - dialog message
     * @param onPositiveButtonClickListener - listener for positive button
     * @param positiveText                  - positive button text
     * @param onNegativeButtonClickListener - listener for negative button
     * @param negativeText                  - negative button text
     */
    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        AlertDialog mAlertDialog = builder.show();
    }

    public void showPickDialog() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Choose an Option");

        // add a list
        String[] animals = {"Camera", "Gallery"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // Camera
                        break;
                    case 1: // Gallery

                       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            AskForPermission();
                        } else {
                            Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, PICK_IMAGE);
                        }*/

                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_profile).setVisible(true);
        menu.findItem(R.id.action_empty).setVisible(false);

        MenuItem item = menu.findItem(R.id.action_profile);
        item.setIcon(ContextCompat.getDrawable(mContext, R.drawable.ic_edit_profile));
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                isAllowEdit = !isAllowEdit;
                ChangeView();

                if(isAllowEdit){

                    item.setIcon(ContextCompat.getDrawable(mContext, R.drawable.ic_profile));
                }else {

                    item.setIcon(ContextCompat.getDrawable(mContext, R.drawable.ic_edit_profile));
                }

                return false;
            }
        });

        super.onPrepareOptionsMenu(menu);
    }
}
package com.codefidea.hpp.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.model.Comment;
import com.codefidea.hpp.model.Countries;
import com.codefidea.hpp.model.Cuisines;
import com.codefidea.hpp.model.Ingredients;
import com.codefidea.hpp.model.LeaderBoard;
import com.codefidea.hpp.model.LifeStyle;
import com.codefidea.hpp.model.MorphesType;
import com.codefidea.hpp.model.NewsData;
import com.codefidea.hpp.model.Video;
import com.codefidea.hpp.model.Plans;
import com.codefidea.hpp.model.Staff;
import com.codefidea.hpp.model.UserData;
import com.codefidea.hpp.util.SettingsPreferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class BaseActivity extends AppCompatActivity
{
    private Typeface font;

    ProgressDialog mProgressDialog;

//    public static ArrayList<RecipesData> RecipesList = new ArrayList<>();
    public static ArrayList<NewsData> mNewsList = new ArrayList<>();
    public static ArrayList<Comment> comments = new ArrayList<>();
    public static ArrayList<Cuisines> mCuisinesList = new ArrayList<>();
    public static ArrayList<Ingredients> mIngredientsList = new ArrayList<>();
    public static ArrayList<String> mIngredientsSelectedList = new ArrayList<>();
    public static ArrayList<Countries> countryList = new ArrayList<>();
    public static ArrayList<MorphesType> mMorpho = new ArrayList<>();
    public static ArrayList<LifeStyle> mLifeStyle = new ArrayList<>();
    public static ArrayList<Plans> mPlans = new ArrayList<>();
    public static ArrayList<LeaderBoard> mLeaderBoardList = new ArrayList<>();
    public static ArrayList<Staff> mStaffsList = new ArrayList<>();
    public static ArrayList<Video> mVideoUrls = new ArrayList<>();
    public static ArrayList<String> mFoodSelectedList = new ArrayList<>();
    public static ArrayList<String> mRecipeSelectedList = new ArrayList<>();

    public static String userID="1",newsID, RecipesID, SelectedDate="2018-04-09", mCurrentYear="2018", mCurrentMonth="1";

    //Variables for sign up
    public static String countryID,cityID,userName,surName,phone,email,birthDate,sex,profilePic = "null",morpheTypeId,
            lifestyleTypeId,planID,practiceSportsFlag = "1",sportsId = "1",weekTimes = "1",isActive = "1",planPrice,aboutMe,
            fiscalCode;

    //Variable for filter
    public static String Time = "",Difficulty = "",CuisineID="1";
    public static String[] ingredients[] = {};

    boolean isProgressVisible = false;

    public static boolean isFromSocialLogin = false;

    public static boolean isLunchConfirm = false, isDinnerConfirm = false, isBreakfastConfirm = false, isAllDay = false;

    // Code for get and set login
    public void setLogin(int i)
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putInt("Login",i);
        spe.apply();
    }

    public int getLogin()
    {
        SharedPreferences sp = getSharedPreferences("LOGIN",MODE_PRIVATE);
        int i = sp.getInt("Login",0);
        return i;
    }
    // Code for get and set login

    public Typeface getTypeFace()
    {
        font = Typeface.createFromAsset(getAssets(), "fonts/Avenir_Light.otf");
        return font;
    }

    public void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(getTypeFace());
            }else if (v instanceof Button) {
                ((Button) v).setTypeface(getTypeFace());
            }else if (v instanceof EditText) {
                ((EditText) v).setTypeface(getTypeFace());
            }

        } catch (Exception e) {
        }
    }

    public String MonthYearName()
    {
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());

        return  month_name.toUpperCase()+" "+String.valueOf(yy);
    }

    public void showWaitIndicator(boolean state) {
        showWaitIndicator(state, "");
    }

    public Boolean isProgressVisible()
    {
        return isProgressVisible;
    }

    public void showWaitIndicator(boolean state, String message) {
        try {
            try {

                isProgressVisible = state;

                if (state) {

                    mProgressDialog = new ProgressDialog(this, R.style.TransparentProgressDialog);
                    mProgressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
                    mProgressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                } else {
                    mProgressDialog.dismiss();
                }

            } catch (Exception e) {

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public void updateUser(UserData user) {
        SettingsPreferences.storeConsumer(this, user);
    }

    public void setLoginPreference(String name, String email, String photo,String uniqueId)
    {
        SharedPreferences sp = getSharedPreferences("SOCIAL_DETAIL",MODE_PRIVATE);
        SharedPreferences.Editor spe = sp.edit();
        spe.putString("NAME",name);
        spe.putString("EMAIL",email);
        spe.putString("PHOTO",photo);
        spe.putString("UNIQUE_ID",uniqueId);
        spe.apply();
    }

    public SharedPreferences getLoginPreference()
    {
        SharedPreferences sp = getSharedPreferences("SOCIAL_DETAIL",MODE_PRIVATE);
        return sp;
    }

    public String getIMEINumber()
    {
        TelephonyManager mngr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String IMEINumber = mngr.getDeviceId();
        return IMEINumber;
    }
}

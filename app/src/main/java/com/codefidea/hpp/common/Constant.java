package com.codefidea.hpp.common;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class Constant
{
    public static String API_BASE_URL = "http://api.foodporntheory.com";

    //JSON VARIABLES

    public static String id = "id";
    public static String name = "name";
    public static String cuisineId = "cuisineId";
    public static String preparationTime = "preparationTime";
    public static String difficulty = "difficulty";
    public static String rating = "rating";
    public static String description = "description";
    public static String userId = "userId";
    public static String createdAt = "createdAt";
    public static String updatedAt = "updatedAt";
    public static String cuisineName = "cuisineName";
    public static String userFirstName = "userFirstName";
    public static String userLastName = "userLastName";
    public static String userProfilePic = "userProfilePic";
    public static String totalLikes = "totalLikes";
    public static String isLiked = "isLiked";
    public static String image = "image";
    public static String title = "title";
    public static String body = "body";
    public static String videos = "videos";
    public static String videoUrl = "videoUrl";
    public static String comments = "comments";
    public static String comment = "comment";

    public static String ingredient = "ingredient";

    public static String ingredients = "ingredients";
    public static String steps = "steps";
    public static String images = "images";

    public static String stepNumber = "stepNumber";

    public static String sortOrder = "sortOrder";

    public static String showRelevantFirst = "showRelevantFirst";
    public static String optimizeImages = "optimizeImages";
    public static String notifications = "notifications";
    public static String allowSaving = "allowSaving";

    public static String units = "units";
    public static String weight = "weight";
    public static String height = "height";
    public static String navalCircumference = "navalCircumference";
    public static String waistCircumference = "waistCircumference";
    public static String thighCircumference = "thighCircumference";
    public static String calfCircumference = "calfCircumference";
    public static String armCircumference = "armCircumference";
    public static String chestCircumference = "chestCircumference";

    public static String snatch = "snatch";
    public static String powerSnatch = "powerSnatch";
    public static String backSquat = "backSquat";
    public static String frontSquat = "frontSquat";
    public static String pushPress = "pushPress";
    public static String shoulderPress = "shoulderPress";
    public static String PowerClean = "PowerClean";
    public static String deadLift = "deadLift";

//    public static String message = "message";

    public static String durationType = "durationType";
    public static String duration = "duration";
    public static String cost = "cost";
    public static String active = "active";

    public static String firstName = "firstName";
    public static String lastName = "lastName";
    public static String profilePic = "profilePic";
    public static String points = "points";

    public static String fromUserId = "fromUserId";
    public static String toUserId = "toUserId";
    public static String message = "message";
    public static String lastname = "lastname";

    public static String fromFirstName = "fromFirstName";
    public static String fromLastname = "fromLastname";
    public static String fromProfilePic = "fromProfilePic";
    public static String toFirstName = "toFirstName";
    public static String toLastname = "toLastname";
    public static String toProfilePic = "toProfilePic";

    public static String day = "day";
    public static String date = "date";
    public static String flag = "flag";

    //Fragment Name
    public static String recipeTitle = "RECIPE";
    public static String newsTitle = "NEWS";
    public static String myprofileTitle = "PROFILE";
    public static String settingsTitle = "SETTINGS";
    public static String ChatTitle = "CHAT";
    public static String DiaryTitle = "DIARY";
    public static String AboutTitle = "ABOUT";
}

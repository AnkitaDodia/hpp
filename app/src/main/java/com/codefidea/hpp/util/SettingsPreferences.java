package com.codefidea.hpp.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.codefidea.hpp.model.UserData;

import java.io.Serializable;

/**
 * Created by My 7 on 22-Mar-18.
 */

public class SettingsPreferences implements Serializable
{
    private static final String PREFS_NAME = "HPP";

    private static final String DEFAULT_VAL = null;

    private static final String Key_id = "id";
    private static final String Key_userType = "userType";
    private static final String Key_planId = "planId";
    private static final String Key_username = "username";
    private static final String Key_name = "name";
    private static final String Key_surname = "surname";
    private static final String Key_phone = "phone";
    private static final String Key_email = "email";
    private static final String Key_birthDate = "birthDate";
    private static final String Key_gender = "gender";
    private static final String Key_address = "address";
    private static final String Key_aboutMe = "aboutMe";
    private static final String Key_profilePic = "profilePic";
    private static final String Key_countryId = "countryId";
    private static final String Key_cityId = "cityId";
    private static final String Key_fiscalCode = "fiscalCode";
    private static final String Key_morpheTypeId = "morpheTypeId";
    private static final String Key_lifestyleTypeId = "lifestyleTypeId";
    private static final String Key_practiceSportsFlag = "practiceSportsFlag";
    private static final String Key_sportsId = "sportsId";
    private static final String Key_weekTimes = "weekTimes";
    private static final String Key_isActive = "isActive";
    private static final String Key_deviceId = "deviceId";
    private static final String Key_points = "points";


    public static void clearDB(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();
        dbEditor.clear();
        dbEditor.commit();

    }

    public static void storeConsumer(Context context, UserData user) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor dbEditor = prefs.edit();

        dbEditor.putString(Key_id, user.getId());
        dbEditor.putString(Key_userType, user.getUserType());
        dbEditor.putString(Key_planId, user.getPlanId());
        dbEditor.putString(Key_username, user.getUsername());
        dbEditor.putString(Key_name, user.getName());
        dbEditor.putString(Key_surname, user.getSurname());
        dbEditor.putString(Key_phone, user.getPhone());
        dbEditor.putString(Key_email, user.getEmail());
        dbEditor.putString(Key_birthDate, user.getBirthDate());
        dbEditor.putString(Key_gender, user.getGender());
        dbEditor.putString(Key_address, user.getAddress());
        dbEditor.putString(Key_aboutMe, user.getAboutMe());
        dbEditor.putString(Key_profilePic, user.getProfilePic());
        dbEditor.putString(Key_countryId, user.getCountryId());
        dbEditor.putString(Key_cityId, user.getCityId());
        dbEditor.putString(Key_fiscalCode, user.getFiscalCode());
        dbEditor.putString(Key_morpheTypeId, user.getMorpheTypeId());
        dbEditor.putString(Key_lifestyleTypeId, user.getLifestyleTypeId());
        dbEditor.putString(Key_practiceSportsFlag, user.getPracticeSportsFlag());
        dbEditor.putString(Key_sportsId, user.getSportsId());
        dbEditor.putString(Key_weekTimes, user.getWeekTimes());
        dbEditor.putString(Key_isActive, user.getIsActive());
        dbEditor.putString(Key_deviceId, user.getDeviceId());
        dbEditor.putString(Key_points, user.getPoints());


        dbEditor.apply();
    }

    public static UserData getConsumer(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        UserData user = new UserData();

        user.setAboutMe(prefs.getString(Key_aboutMe, DEFAULT_VAL));
        user.setAddress(prefs.getString(Key_address, DEFAULT_VAL));
        user.setBirthDate(prefs.getString(Key_birthDate, DEFAULT_VAL));
        user.setCityId(prefs.getString(Key_cityId, DEFAULT_VAL));
        user.setCountryId(prefs.getString(Key_countryId, DEFAULT_VAL));
        user.setDeviceId(prefs.getString(Key_deviceId, DEFAULT_VAL));
        user.setEmail(prefs.getString(Key_email, DEFAULT_VAL));
        user.setFiscalCode(prefs.getString(Key_fiscalCode, DEFAULT_VAL));
        user.setGender(prefs.getString(Key_gender, DEFAULT_VAL));
        user.setId(prefs.getString(Key_id, DEFAULT_VAL));
        user.setIsActive(prefs.getString(Key_isActive, DEFAULT_VAL));
        user.setLifestyleTypeId(prefs.getString(Key_lifestyleTypeId, DEFAULT_VAL));
        user.setMorpheTypeId(prefs.getString(Key_morpheTypeId, DEFAULT_VAL));
        user.setName(prefs.getString(Key_name, DEFAULT_VAL));
        user.setPhone(prefs.getString(Key_phone, DEFAULT_VAL));
        user.setPlanId(prefs.getString(Key_planId, DEFAULT_VAL));
        user.setPoints(prefs.getString(Key_points, DEFAULT_VAL));
        user.setPracticeSportsFlag(prefs.getString(Key_practiceSportsFlag, DEFAULT_VAL));
        user.setProfilePic(prefs.getString(Key_profilePic, DEFAULT_VAL));
        user.setSportsId(prefs.getString(Key_sportsId, DEFAULT_VAL));
        user.setSurname(prefs.getString(Key_surname, DEFAULT_VAL));
        user.setUsername(prefs.getString(Key_username, DEFAULT_VAL));
        user.setUserType(prefs.getString(Key_userType, DEFAULT_VAL));
        user.setWeekTimes(prefs.getString(Key_weekTimes, DEFAULT_VAL));

        return user;
    }
}

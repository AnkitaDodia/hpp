package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.RecipesData;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by abc on 21-10-2017.
 */

public class SampleIntroActivity extends BaseActivity {

    SampleIntroActivity mContext;

    private LinearLayout dotsLayout, lay_intro_bottom;
    private TextView[] dots;
    private Button btn_sign_in, btn_create_account;
    private ViewPager view_pager_intro;
    private MyViewPagerAdapter myViewPagerAdapter;

    private int no_of_intro_slide = 3;

    boolean doubleBackToExitPressedOnce = false;

    String SplashURL;

    boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        setContentView(R.layout.activity_intro);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        inItView();
        setTypeFace();
        setClickListener();
        setData();
//        getSplashVideoRequest();
    }

    private void inItView() {
        view_pager_intro = (ViewPager) findViewById(R.id.view_pager_intro);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        btn_sign_in = (Button) findViewById(R.id.btn_sign_in);
        btn_create_account = (Button) findViewById(R.id.btn_create_account);

        lay_intro_bottom = (LinearLayout) findViewById(R.id.lay_intro_bottom);
    }

    private void setTypeFace() {
        btn_sign_in.setTypeface(getTypeFace());
        btn_create_account.setTypeface(getTypeFace());

        // adding bottom dots
        addBottomDots(0);
    }


    private void setClickListener() {
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, LoginActivity.class));
//                finish();
            }
        });

        btn_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(mContext, SingUpActivity.class));
//                finish();
            }
        });
    }

    private void setData() {
        myViewPagerAdapter = new MyViewPagerAdapter();
        view_pager_intro.setAdapter(myViewPagerAdapter);
        view_pager_intro.addOnPageChangeListener(viewPagerPageChangeListener);
    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the
            if (position == no_of_intro_slide - 1) {
                // last page. make button text to GOT IT
                lay_intro_bottom.setVisibility(View.VISIBLE);
            } else {

                lay_intro_bottom.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[no_of_intro_slide];

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_dark));
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light));
    }

    private int getItem(int i) {
        return view_pager_intro.getCurrentItem() + i;
    }


    public void getSplashVideoRequest()
    {
        try {
            showWaitIndicator(true);

            String SplashURL = Constant.API_BASE_URL+"/splashvideo";
//            http://api.foodporntheory.com/splashvideo

            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest sr = new StringRequest(Request.Method.GET,SplashURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("splash", "SPLASH_API : "+response.toString());
                    getSplashData(response.toString());
                    showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    return params;
                }
            };
            queue.add(sr);

        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    private void getSplashData(String response)
    {
        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");
                String msg = jObject.getString("message");
                if(res)
                {
                    SplashURL = jObject.getString("responseData");

                    Log.e("SplashURL", "SplashURL :  "+SplashURL);
                }
                else {
                    Toast.makeText(mContext,""+msg,Toast.LENGTH_LONG).show();
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(R.layout.intro_slide, container, false);

            //Update Views API DATA Here

            TextView txt_intro_title = (TextView) view.findViewById(R.id.txt_intro_title);
            TextView txt_intro_content = (TextView) view.findViewById(R.id.txt_intro_content);
            TextView txt_intro_content_main = (TextView) view.findViewById(R.id.txt_intro_content_main);

            ImageView img_intro = (ImageView) view.findViewById(R.id.img_intro);

            LinearLayout ll_parent_intro_slide = (LinearLayout)view.findViewById(R.id.ll_parent_intro_slide);

            txt_intro_content_main.setTypeface(getTypeFace());
            txt_intro_title.setTypeface(getTypeFace());
            txt_intro_content.setTypeface(getTypeFace());

            txt_intro_content_main.setTextColor(getResources().getColor(R.color.SealBrown));
            txt_intro_title.setTextColor(getResources().getColor(R.color.SealBrown));
            txt_intro_content.setTextColor(getResources().getColor(R.color.SealBrown));


            ll_parent_intro_slide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == no_of_intro_slide - 1){
//                        Toast.makeText(mContext, "Clicked on Play", Toast.LENGTH_LONG).show();
                        if (isInternet) {
                            watchYoutubeVideo(mContext,SplashURL);
                        } else {
                            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            final int sdk = android.os.Build.VERSION.SDK_INT;

            if (position == no_of_intro_slide - 1) {

                img_intro.setVisibility(View.GONE);
                txt_intro_content_main.setVisibility(View.GONE);

                txt_intro_title.setVisibility(View.GONE);
                txt_intro_content.setVisibility(View.GONE);

                if (isInternet) {
                    getSplashVideoRequest();
                } else {
                    Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                }

//                ll_parent_intro_slide.setBackgroundColor(getResources().getColor(R.color.white));

                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    ll_parent_intro_slide.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.slide_3) );
                } else {
                    ll_parent_intro_slide.setBackground(ContextCompat.getDrawable(mContext, R.drawable.slide_3));
                }

//                Log.e("position  if", "position   :   " + position);

            } else if(position == no_of_intro_slide - 2){


                img_intro.setVisibility(View.GONE);
                txt_intro_content_main.setVisibility(View.GONE);
                txt_intro_title.setVisibility(View.GONE);
                txt_intro_content.setVisibility(View.GONE);

                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    ll_parent_intro_slide.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.slide_2) );
                } else {
                    ll_parent_intro_slide.setBackground(ContextCompat.getDrawable(mContext, R.drawable.slide_2));
                }
            }else {
                img_intro.setVisibility(View.GONE);
                txt_intro_content_main.setVisibility(View.GONE);
                txt_intro_title.setVisibility(View.GONE);
                txt_intro_content.setVisibility(View.GONE);

                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    ll_parent_intro_slide.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.slide_1) );
                } else {
                    ll_parent_intro_slide.setBackground(ContextCompat.getDrawable(mContext, R.drawable.slide_1));
                }
            }

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return no_of_intro_slide;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }

        public void watchYoutubeVideo(SampleIntroActivity context, String id){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(id));
            intent.setPackage("com.google.android.youtube");
            context.startActivity(intent);
        }
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(mContext , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}

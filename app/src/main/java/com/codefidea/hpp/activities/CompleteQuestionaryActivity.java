package com.codefidea.hpp.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;

/**
 * Created by Raavan on 06-Mar-18.
 */

public class CompleteQuestionaryActivity extends BaseActivity {

    Context mContext;

    LinearLayout lay_parent_complete_que, ll_sedentarylife_back;

    Button button_yes_1_sedentary,button_no_1_sedentary,button_yes_2_sedentary,button_no_2_sedentary,button_yes_3_sedentary,
            button_no_3_sedentary,button_yes_4_sedentary,button_no_4_sedentary,button_yes_5_sedentary,button_no_5_sedentary,
            button_weight_loss_sedentary,button_weight_increase_sedentary,button_improve_sedentary,btn_getSet_sedentary;

    EditText edit_describe_day_sedentary,edit_current_diet_sedentary,edit_describe_supplements_sedentary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sedentarylife);

        mContext = this;

        setView();
        setClickListener();
        overrideFonts(mContext , lay_parent_complete_que);
    }

    private void setView() {
        lay_parent_complete_que = (LinearLayout)findViewById(R.id.lay_parent_complete_que);
        ll_sedentarylife_back = (LinearLayout)findViewById(R.id.ll_sedentarylife_back);

        edit_describe_day_sedentary = (EditText) findViewById(R.id.edit_describe_day_sedentary);
        edit_current_diet_sedentary = (EditText) findViewById(R.id.edit_current_diet_sedentary);
        edit_describe_supplements_sedentary = (EditText) findViewById(R.id.edit_describe_supplements_sedentary);

        button_yes_1_sedentary = (Button) findViewById(R.id.button_yes_1_sedentary);
        button_no_1_sedentary = (Button) findViewById(R.id.button_no_1_sedentary);
        button_yes_2_sedentary = (Button) findViewById(R.id.button_yes_2_sedentary);
        button_no_2_sedentary = (Button) findViewById(R.id.button_no_2_sedentary);
        button_yes_3_sedentary = (Button) findViewById(R.id.button_yes_3_sedentary);
        button_no_3_sedentary = (Button) findViewById(R.id.button_no_3_sedentary);
        button_yes_4_sedentary = (Button) findViewById(R.id.button_yes_4_sedentary);
        button_no_4_sedentary = (Button) findViewById(R.id.button_no_4_sedentary);
        button_yes_5_sedentary = (Button) findViewById(R.id.button_yes_5_sedentary);
        button_no_5_sedentary = (Button) findViewById(R.id.button_no_5_sedentary);
        button_weight_loss_sedentary = (Button) findViewById(R.id.button_weight_loss_sedentary);
        button_weight_increase_sedentary = (Button) findViewById(R.id.button_weight_increase_sedentary);
        button_improve_sedentary = (Button) findViewById(R.id.button_improve_sedentary);
        btn_getSet_sedentary = (Button) findViewById(R.id.btn_getSet_sedentary);
    }

    private void setClickListener()
    {
        ll_sedentarylife_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        button_yes_1_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_no_1_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_yes_2_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_no_2_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_yes_3_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_no_3_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_yes_4_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_no_4_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_yes_5_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_no_5_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_weight_loss_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_weight_increase_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        button_improve_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btn_getSet_sedentary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}

package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.CuisinesAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.Cuisines;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class CuisinesActivity extends BaseActivity {

    CuisinesActivity mContext;
    RecyclerView rcv_cuisine;
    CuisinesAdapter mCuisinesAdapter;
    private ArrayList<Cuisines> mCuisinesList = new ArrayList<>();
    List<Integer> mCuisinesSelectionsList = new ArrayList<>();
    LinearLayout ll_cuisine_back, ll_cuisine_done;

    private boolean isInternet;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisines);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        InitViews();
        setClickListener();

        if (BaseActivity.mCuisinesList.size() > 0) {
            mCuisinesList = BaseActivity.mCuisinesList;
            InitCuisineRecyler();
        } else {
            if (isInternet) {
                sendCuisineRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void InitViews() {
        rcv_cuisine = (RecyclerView) findViewById(R.id.rcv_cuisine);
        ll_cuisine_back = (LinearLayout) findViewById(R.id.ll_cuisine_back);
        ll_cuisine_done = (LinearLayout) findViewById(R.id.ll_cuisine_done);
    }

    private void setClickListener() {

        ll_cuisine_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_cuisine_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = getIntent();
                intent.putExtra("CuisineID", BaseActivity.CuisineID);
                setResult(RESULT_OK, intent);
                finish();
            }
        });


        rcv_cuisine.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcv_cuisine, new ClickListener() {
            @Override
            public void onClick(View view, int position) {


                BaseActivity.CuisineID = mCuisinesList.get(position).getCuisinesID();
//                Toast.makeText(mContext, "" + BaseActivity.CuisineID, Toast.LENGTH_SHORT).show();

//                mCuisinesSelectionsList.clear();
//                FrameThumbList.clear();
//                position++;
                PrepareCuisinesSelection(Integer.valueOf(BaseActivity.CuisineID));
                mCuisinesAdapter.notifyDataSetChanged();
//

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


    private void InitCuisineRecyler() {

        PrepareCuisinesSelection(Integer.valueOf(BaseActivity.CuisineID));

        rcv_cuisine.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_cuisine.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        mCuisinesAdapter = new CuisinesAdapter(mContext, mCuisinesList, mCuisinesSelectionsList);
        rcv_cuisine.setAdapter(mCuisinesAdapter);
        rcv_cuisine.setItemAnimator(new DefaultItemAnimator());
        mCuisinesAdapter.notifyDataSetChanged();
    }

    private void sendCuisineRequest() {
        mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/cuisines
        String newsURL = Constant.API_BASE_URL + "/recipes/cuisines";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Cuisine_API ", response.toString());
                getCuisineData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getCuisineData(String response) {
        JSONArray CuisineList = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    CuisineList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < CuisineList.length(); i++) {
                        Cuisines data = new Cuisines();

                        data.setCuisinesID(CuisineList.getJSONObject(i).getString(Constant.id));
                        data.setCuisinesName(CuisineList.getJSONObject(i).getString(Constant.name));

                        mCuisinesList.add(data);
                    }

                    BaseActivity.mCuisinesList = mCuisinesList;
                    InitCuisineRecyler();
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrepareCuisinesSelection(int position) {

        position--;
        mCuisinesSelectionsList.clear();

        Log.e("SelectionList", "SelectionList size" + mCuisinesList.size());
        for (int i = 0; i <= mCuisinesList.size(); i++) {
            Log.e("SelectionList", "SelectionList" + i);
            if (i == position) {
                mCuisinesSelectionsList.add(1);

            } else {

                mCuisinesSelectionsList.add(0);
            }

        }
    }
}

package com.codefidea.hpp.activities;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.CommentAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.model.Comment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 08-Mar-18.
 */

public class ViewAllCommentsActivity extends BaseActivity
{
    ViewAllCommentsActivity mContext;

    RecyclerView rcv_comments;

    CommentAdapter itemsAdapter;

    ImageView image_back_view_all;

    TextView text_title_view_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_all_comments);

        mContext = this;

        setView();
        InitNewsRecyler();
        setClickListener();
    }

    private void setView()
    {
        rcv_comments = (RecyclerView) findViewById(R.id.rcv_comments);

        image_back_view_all = (ImageView) findViewById(R.id.image_back_view_all);

        text_title_view_all = (TextView) findViewById(R.id.text_title_view_all);
        text_title_view_all.setTypeface(getTypeFace());
    }

    private void setClickListener()
        {
        image_back_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void InitNewsRecyler() {
        rcv_comments.setLayoutManager(new LinearLayoutManager(mContext));
        rcv_comments.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        itemsAdapter = new CommentAdapter(mContext, BaseActivity.comments);
        rcv_comments.setAdapter(itemsAdapter);
        rcv_comments.setItemAnimator(new DefaultItemAnimator());
        itemsAdapter.notifyDataSetChanged();
    }
}

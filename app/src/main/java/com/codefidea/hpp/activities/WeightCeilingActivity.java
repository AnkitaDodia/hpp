package com.codefidea.hpp.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.UnitSpinnerAdapter;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.CeilingInfo;
import com.codefidea.hpp.util.SettingsPreferences;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raavan on 06-Mar-18.
 */

public class WeightCeilingActivity extends BaseActivity {

    BaseActivity mContext;

    private LinearLayout lay_parent_weightceiling, llWeightceilingBack, llWeightceilingEdit;

    private ImageView imgWeightceilingEdit,img_physicaldata_previous, img_physicaldata_next;

    private CircleImageView imgPersonImageWeightceiling;
    private TextView txtPersonNameWeightceiling;
    private TextView txtPersonAddWeightceiling;

    private ImageView imgPreviousWeightceiling;
    private TextView txtMonthYearWeightceiling;
    private ImageView imgNextWeightceiling;

    private EditText edtWeightceilingSnatch;
    private EditText edtWeightceilingPowersnatch;
    private EditText edtWeightceilingBacksquat;
    private EditText edtWeightceilingFrontsquat;
    private EditText edtWeightceilingPushpress;
    private EditText edtWeightceilingShoulderpress;
    private EditText edtWeightceilingPowerclean;
    private EditText edtWeightceilingDeadlift;

    private Spinner spinner_units;

    boolean isInternet, isEditing = false;

    private  String units = "kg/cm";
    ImageLoader imageLoader;

    Calendar calendar = Calendar.getInstance();
    int mMinMonth = 0,mMaxMonth = 0;

    boolean isPreviousClick = false;

    TextView txt_month_year_weightceiling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weightceiling);

        mContext = this;
        imageLoader = AppController.getInstance().getImageLoader();
        setView();
        setClickListener();
        overrideFonts(mContext, lay_parent_weightceiling);

        String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());
        txt_month_year_weightceiling.setText("" + monthName + " " + calendar.get(Calendar.YEAR));

        BaseActivity.mCurrentYear = String.valueOf(calendar.get(Calendar.YEAR));
        BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime()); // coz it's an monthid

        isInternet = new InternetStatus().isInternetOn(mContext);
        if (isInternet) {
            GetCeilingInfoRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        lay_parent_weightceiling = (LinearLayout) findViewById(R.id.lay_parent_weightceiling);
        llWeightceilingBack = (LinearLayout) findViewById(R.id.ll_weightceiling_back);
        llWeightceilingEdit = (LinearLayout) findViewById(R.id.ll_weightceiling_edit);

        imgWeightceilingEdit = (ImageView) findViewById(R.id.img_weightceiling_edit);
        img_physicaldata_previous = (ImageView) findViewById(R.id.img_previous_weightceiling);
        img_physicaldata_next = (ImageView) findViewById(R.id.img_next_weightceiling);

        imgPersonImageWeightceiling = (CircleImageView) findViewById(R.id.img_person_image_weightceiling);
        txtPersonNameWeightceiling = (TextView) findViewById(R.id.txt_person_name_weightceiling);
        txtPersonAddWeightceiling = (TextView) findViewById(R.id.txt_person_add_weightceiling);
        txt_month_year_weightceiling = (TextView) findViewById(R.id.txt_month_year_weightceiling);

        imgPreviousWeightceiling = (ImageView) findViewById(R.id.img_previous_weightceiling);
        txtMonthYearWeightceiling = (TextView) findViewById(R.id.txt_month_year_weightceiling);
        imgNextWeightceiling = (ImageView) findViewById(R.id.img_next_weightceiling);

        edtWeightceilingSnatch = (EditText) findViewById(R.id.edt_weightceiling_snatch);
        edtWeightceilingPowersnatch = (EditText) findViewById(R.id.edt_weightceiling_powersnatch);
        edtWeightceilingBacksquat = (EditText) findViewById(R.id.edt_weightceiling_backsquat);
        edtWeightceilingFrontsquat = (EditText) findViewById(R.id.edt_weightceiling_frontsquat);
        edtWeightceilingPushpress = (EditText) findViewById(R.id.edt_weightceiling_pushpress);
        edtWeightceilingShoulderpress = (EditText) findViewById(R.id.edt_weightceiling_shoulderpress);
        edtWeightceilingPowerclean = (EditText) findViewById(R.id.edt_weightceiling_powerclean);
        edtWeightceilingDeadlift = (EditText) findViewById(R.id.edt_weightceiling_deadlift);

        spinner_units = (Spinner) findViewById(R.id.spinner_units);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.unit_array, android.R.layout.simple_spinner_item);

        // Create an ArrayAdapter using the string array and a default spinner layout
//        ArrayAdapter<CharSequence> spinneradapter = ArrayAdapter.createFromResource(this,
//                R.array.unit_array, android.R.layout.simple_spinner_dropdown_item);
// Specify the layout to use when the list of choices appears
//        spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
//        spinner_units.setAdapter(spinneradapter);

        txtPersonNameWeightceiling.setText(SettingsPreferences.getConsumer(this).getName());
        txtPersonAddWeightceiling.setText(SettingsPreferences.getConsumer(this).getEmail());

        String imagePath = SettingsPreferences.getConsumer(this).getProfilePic();

        imageLoader.get(imagePath, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    imgPersonImageWeightceiling.setImageBitmap(response.getBitmap());
                }
            }
        });

        UnitSpinnerAdapter unitadapter = new UnitSpinnerAdapter(
                mContext,
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.unit_array))
        );

        spinner_units.setAdapter(unitadapter);

    }

    private void setClickListener() {
        llWeightceilingBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llWeightceilingEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditing == true) {

                    MakeViewEditable(false);
                    imgWeightceilingEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_profile));
                    imgWeightceilingEdit.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
                    isEditing = !isEditing;

                    PostCeilingInfoRequest();

                } else {

                    MakeViewEditable(true);
                    imgWeightceilingEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
                    imgWeightceilingEdit.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
                    isEditing = !isEditing;
                }
            }
        });

        spinner_units.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                units = spinner_units.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_physicaldata_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mMinMonth <= 5)
                {
                    isPreviousClick = true;
                    calendar.add(Calendar.MONTH, -1);
                    String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());
                    mMaxMonth = mMinMonth;
                    mMinMonth++;
                    txt_month_year_weightceiling.setText("" + monthName + "  " + calendar.get(Calendar.YEAR));
                    BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime());

                    Log.e("Month Check", " MonthName " + new SimpleDateFormat("M").format(calendar.getTime()));

                    if (isInternet) {
                        GetCeilingInfoRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(mContext, "You can not see that", Toast.LENGTH_SHORT).show();
                }
            }

        });

        img_physicaldata_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPreviousClick)
                {
                    if(mMaxMonth == 0)
                    {
                        isPreviousClick = false;
                    }
                    else
                    {
                        mMaxMonth--;
                    }

                    calendar.add(Calendar.MONTH, 1);
                    mMinMonth--;
                    String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());


                    txt_month_year_weightceiling.setText("" + monthName + "  " + calendar.get(Calendar.YEAR));

                    BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime());
                    Log.e("Month Check", " MonthName " + new SimpleDateFormat("M").format(calendar.getTime()));
//

                    if (isInternet) {
                        GetCeilingInfoRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(mContext, "You can not see that", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void PostCeilingInfoRequest() {
        showWaitIndicator(true);
//        http://api.foodporntheory.com/user/1/ceilinginfo
        String newsURL = Constant.API_BASE_URL + "/user/" + BaseActivity.userID + "/ceilinginfo";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CeilingInfo_API ", response.toString());
                GetCeilingInfoData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId", BaseActivity.userID);
                params.put("month", "1");
                params.put("year", "2018");
                params.put(Constant.units, units);

                params.put(Constant.snatch, edtWeightceilingSnatch.getText().toString());
                params.put(Constant.powerSnatch, edtWeightceilingPowersnatch.getText().toString());
                params.put(Constant.backSquat, edtWeightceilingBacksquat.getText().toString());
                params.put(Constant.frontSquat, edtWeightceilingFrontsquat.getText().toString());
                params.put(Constant.pushPress, edtWeightceilingPushpress.getText().toString());
                params.put(Constant.shoulderPress, edtWeightceilingShoulderpress.getText().toString());
                params.put(Constant.PowerClean, edtWeightceilingPowerclean.getText().toString());
                params.put(Constant.deadLift, edtWeightceilingDeadlift.getText().toString());


                return params;
            }
        };
        queue.add(sr);
    }

    private void GetCeilingInfoRequest() {
        showWaitIndicator(true);

        String newsURL = Constant.API_BASE_URL + "/user/" + BaseActivity.userID + "/ceilinginfo/" + BaseActivity.mCurrentYear + "/" + BaseActivity.mCurrentMonth;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CeilingInfo_API ", response.toString());
                GetCeilingInfoData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(sr);
    }

    private void GetCeilingInfoData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());


            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    JSONObject jResponseData = jObject.getJSONObject("responseData");

                    CeilingInfo mCeilingInfo = new CeilingInfo();

                    mCeilingInfo.setUnits(jResponseData.getString(Constant.units));
                    mCeilingInfo.setSnatch(jResponseData.getString(Constant.snatch));
                    mCeilingInfo.setPowerSnatch(jResponseData.getString(Constant.powerSnatch));
                    mCeilingInfo.setBackSquat(jResponseData.getString(Constant.backSquat));
                    mCeilingInfo.setFrontSquat(jResponseData.getString(Constant.frontSquat));
                    mCeilingInfo.setPushPress(jResponseData.getString(Constant.pushPress));
                    mCeilingInfo.setShoulderPress(jResponseData.getString(Constant.shoulderPress));
                    mCeilingInfo.setPowerClean(jResponseData.getString(Constant.PowerClean));
                    mCeilingInfo.setDeadLift(jResponseData.getString(Constant.deadLift));

                    SetData(mCeilingInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetData(CeilingInfo mCeilingInfo) {

        edtWeightceilingSnatch.setText(mCeilingInfo.getSnatch());
        edtWeightceilingPowersnatch.setText(mCeilingInfo.getPowerSnatch());
        edtWeightceilingBacksquat.setText(mCeilingInfo.getBackSquat());
        edtWeightceilingFrontsquat.setText(mCeilingInfo.getFrontSquat());
        edtWeightceilingPushpress.setText(mCeilingInfo.getPushPress());
        edtWeightceilingShoulderpress.setText(mCeilingInfo.getShoulderPress());
        edtWeightceilingPowerclean.setText(mCeilingInfo.getPowerClean());
        edtWeightceilingDeadlift.setText(mCeilingInfo.getDeadLift());


        MakeViewEditable(false);
    }

    public void MakeViewEditable(boolean isEditable) {

        edtWeightceilingSnatch.setEnabled(isEditable);
        edtWeightceilingPowersnatch.setEnabled(isEditable);
        edtWeightceilingBacksquat.setEnabled(isEditable);
        edtWeightceilingFrontsquat.setEnabled(isEditable);
        edtWeightceilingPushpress.setEnabled(isEditable);
        edtWeightceilingShoulderpress.setEnabled(isEditable);
        edtWeightceilingPowerclean.setEnabled(isEditable);
        edtWeightceilingDeadlift.setEnabled(isEditable);
    }
}

package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.fragments.SignUpContinueFourFragment;
import com.codefidea.hpp.fragments.SignUpContinueOneFragment;
import com.codefidea.hpp.fragments.SignUpContinueThreeFragment;
import com.codefidea.hpp.fragments.SignUpContinueTwoFragment;
import com.codefidea.hpp.ui.NonSwipeableViewPager;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class SignUpPagerActivity extends BaseActivity
{
    public NonSwipeableViewPager view_pager_signup_pager;

    SignupPagerAdapter adapter;

    private LinearLayout layoutDots_signup_pager;

    TextView[] dots;

    public int no_of_sample_tutorial = 4;

    ImageView ic_back_signUp_pager;

    Context mContext;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signuppager);

        mContext = this;

        setView();
        addBottomDots(0);
        setClickListener();
    }

    private void setView() {
        view_pager_signup_pager = (NonSwipeableViewPager) findViewById(R.id.view_pager_signup_pager);

        layoutDots_signup_pager = (LinearLayout) findViewById(R.id.layoutDots_signup_pager);

        adapter = new SignupPagerAdapter(getSupportFragmentManager());
        view_pager_signup_pager.setAdapter(adapter);
        view_pager_signup_pager.addOnPageChangeListener(viewPagerPageChangeListener);
        view_pager_signup_pager.setOffscreenPageLimit(-1);

        ic_back_signUp_pager = (ImageView)findViewById(R.id.ic_back_signUp_pager);
        ic_back_signUp_pager.setBackgroundResource(R.drawable.ic_back_green);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            switch (position)
            {
                case 0:
                    ic_back_signUp_pager.setBackgroundResource(R.drawable.ic_back_green);

                    break;
                case 1:
                    ic_back_signUp_pager.setBackgroundResource(R.drawable.ic_back);

                    break;
                case 2:
                    ic_back_signUp_pager.setBackgroundResource(R.drawable.ic_back);

                    break;
                case 3:
                    ic_back_signUp_pager.setBackgroundResource(R.drawable.ic_back);

                    break;
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[no_of_sample_tutorial];

        layoutDots_signup_pager.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_dark));
            layoutDots_signup_pager.addView(dots[i]);
        }

        if (currentPage == 1 || currentPage == 3)
        {
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light_green));
        }
        else
        {
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light));
        }
    }

    private void setClickListener()
    {
        ic_back_signUp_pager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public class SignupPagerAdapter extends FragmentPagerAdapter {

        int NUM_ITEMS = 4;

        public SignupPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SignUpContinueOneFragment();
                case 1:
                    return new SignUpContinueTwoFragment();
                case 2:
                    return new SignUpContinueThreeFragment();
                case 3:
                    return new SignUpContinueFourFragment();
                default:
                    return new SignUpContinueOneFragment();
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

    }

    public int getItem(int i) {
        return view_pager_signup_pager.getCurrentItem() + i;
    }
}

package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;

/**
 * Created by My 7 on 06-Mar-18.
 */

public class SignupFinishActivity extends BaseActivity
{
    SignupFinishActivity mContext;

    LinearLayout layout_finish_payment_main;

    Button btn_finish_payment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup_finish);

        mContext = this;

        setView();
        overrideFonts(mContext,layout_finish_payment_main);
        setClickListener();
    }

    private void setView()
    {
        layout_finish_payment_main = (LinearLayout) findViewById(R.id.layout_finish_payment_main);

        btn_finish_payment = (Button) findViewById(R.id.btn_finish_payment);
    }

    private void setClickListener()
    {
        btn_finish_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext,LoginActivity.class);
                startActivity(it);
                finish();
            }
        });
    }
}

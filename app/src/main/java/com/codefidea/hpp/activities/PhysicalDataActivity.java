package com.codefidea.hpp.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.UnitSpinnerAdapter;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.ui.YearMonthPickerDialog;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.PhysicalInfo;
import com.codefidea.hpp.util.SettingsPreferences;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Raavan on 06-Mar-18.
 */

public class PhysicalDataActivity extends BaseActivity {

    BaseActivity mContext;
    Context ctx;

    LinearLayout lay_parent_physicaldata, ll_physicaldata_back, ll_physicaldata_edit;
    TextView txt_physicaldata_name, txt_physicaldata_add, txt_physicaldata_month_year;
    ImageView img_physicaldata_edit, img_physicaldata_previous, img_physicaldata_next;

    EditText edt_physicaldata_weight, edt_physicaldata_height, edt_physicaldata_navel, edt_physicaldata_waist, edt_physicaldata_thigh, edt_physicaldata_calf,
            edt_physicaldata_arm, edt_physicaldata_chest;

    private CircleImageView img_profile_physicaldata;

    boolean isInternet, isEditing = false;
    private Spinner spinner_units;
    private String units = "kg/cm";
    ImageLoader imageLoader;

    Calendar calendar = Calendar.getInstance();
    int mMinMonth = 0,mMaxMonth = 0;

    boolean isPreviousClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_physicaldata);

        mContext = this;
        imageLoader = AppController.getInstance().getImageLoader();

        setView();
        setClickListener();
        overrideFonts(mContext, lay_parent_physicaldata);
        isInternet = new InternetStatus().isInternetOn(mContext);

        String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());
        txt_physicaldata_month_year.setText("" + monthName + " " + calendar.get(Calendar.YEAR));

        BaseActivity.mCurrentYear = String.valueOf(calendar.get(Calendar.YEAR));
        BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime()); // coz it's an monthid

        if (isInternet) {
            GetProfileInfoRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        lay_parent_physicaldata = (LinearLayout) findViewById(R.id.lay_parent_physicaldata);
        ll_physicaldata_back = (LinearLayout) findViewById(R.id.ll_physicaldata_back);
        ll_physicaldata_edit = (LinearLayout) findViewById(R.id.ll_physicaldata_edit);

        img_physicaldata_edit = (ImageView) findViewById(R.id.img_physicaldata_edit);

        txt_physicaldata_name = (TextView) findViewById(R.id.txt_physicaldata_name);
        txt_physicaldata_add = (TextView) findViewById(R.id.txt_physicaldata_add);
        img_profile_physicaldata = (CircleImageView) findViewById(R.id.img_profile_physicaldata);

        txt_physicaldata_month_year = (TextView) findViewById(R.id.txt_physicaldata_month_year);
        img_physicaldata_previous = (ImageView) findViewById(R.id.img_physicaldata_previous);
        img_physicaldata_next = (ImageView) findViewById(R.id.img_physicaldata_next);

        edt_physicaldata_weight = (EditText) findViewById(R.id.edt_physicaldata_weight);
        edt_physicaldata_height = (EditText) findViewById(R.id.edt_physicaldata_height);
        edt_physicaldata_navel = (EditText) findViewById(R.id.edt_physicaldata_navel);
        edt_physicaldata_waist = (EditText) findViewById(R.id.edt_physicaldata_waist);
        edt_physicaldata_thigh = (EditText) findViewById(R.id.edt_physicaldata_thigh);
        edt_physicaldata_calf = (EditText) findViewById(R.id.edt_physicaldata_calf);
        edt_physicaldata_arm = (EditText) findViewById(R.id.edt_physicaldata_arm);
        edt_physicaldata_chest = (EditText) findViewById(R.id.edt_physicaldata_chest);

        spinner_units = (Spinner) findViewById(R.id.spinner_units);
        UnitSpinnerAdapter unitadapter = new UnitSpinnerAdapter(
                mContext,
                android.R.layout.simple_spinner_dropdown_item,
                Arrays.asList(getResources().getStringArray(R.array.unit_array))
        );

        spinner_units.setAdapter(unitadapter);

        txt_physicaldata_name.setText(SettingsPreferences.getConsumer(this).getName());
        txt_physicaldata_add.setText(SettingsPreferences.getConsumer(this).getEmail());

        String imagePath = SettingsPreferences.getConsumer(this).getProfilePic();

        imageLoader.get(imagePath, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    img_profile_physicaldata.setImageBitmap(response.getBitmap());
                }
            }
        });
    }

    private void setClickListener() {
        ll_physicaldata_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_physicaldata_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isEditing == true) {
                    MakeViewEditable(false);
                    img_physicaldata_edit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit_profile));
                    img_physicaldata_edit.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
                    isEditing = !isEditing;

                    PostProfileInfoRequest();
                } else {
                    MakeViewEditable(true);
                    img_physicaldata_edit.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
                    img_physicaldata_edit.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
                    isEditing = !isEditing;
                }
            }
        });

        spinner_units.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                units = spinner_units.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        img_physicaldata_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mMinMonth <= 5)
                {
                    isPreviousClick = true;
                    calendar.add(Calendar.MONTH, -1);
                    String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());
                    mMaxMonth = mMinMonth;
                    mMinMonth++;
                    txt_physicaldata_month_year.setText("" + monthName + "  " + calendar.get(Calendar.YEAR));
                    BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime());

                    Log.e("Month Check", " MonthName " + new SimpleDateFormat("M").format(calendar.getTime()));

                    if (isInternet) {
                        GetProfileInfoRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(mContext, "You can not see that", Toast.LENGTH_SHORT).show();
                }
            }

        });

        img_physicaldata_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPreviousClick)
                {
                    if(mMaxMonth == 0)
                    {
                        isPreviousClick = false;
                    }
                    else
                    {
                        mMaxMonth--;
                    }

                    calendar.add(Calendar.MONTH, 1);
                    mMinMonth--;
                    String monthName = new SimpleDateFormat("MMMM").format(calendar.getTime());


                    txt_physicaldata_month_year.setText("" + monthName + "  " + calendar.get(Calendar.YEAR));

                    BaseActivity.mCurrentMonth = new SimpleDateFormat("M").format(calendar.getTime());
                    Log.e("Month Check", " MonthName " + new SimpleDateFormat("M").format(calendar.getTime()));
//

                    if (isInternet) {
                        GetProfileInfoRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(mContext, "You can not see that", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void PostProfileInfoRequest() {
        showWaitIndicator(true);
//        http://api.foodporntheory.com/user/1/physicalinfo
        String newsURL = Constant.API_BASE_URL + "/user/" + BaseActivity.userID + "/physicalinfo";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("PHYSICAL_INFO_API ", response.toString());
                GetProfileInfoData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("userId", BaseActivity.userID);
                params.put("month", BaseActivity.mCurrentMonth);
                params.put("year", BaseActivity.mCurrentYear);
                params.put("units", units);

                params.put("weight", edt_physicaldata_weight.getText().toString());
                params.put("height", edt_physicaldata_height.getText().toString());
                params.put("navalCircumference", edt_physicaldata_navel.getText().toString());
                params.put("waistCircumference", edt_physicaldata_waist.getText().toString());
                params.put("thighCircumference", edt_physicaldata_thigh.getText().toString());
                params.put("calfCircumference", edt_physicaldata_calf.getText().toString());
                params.put("armCircumference", edt_physicaldata_arm.getText().toString());
                params.put("chestCircumference", edt_physicaldata_chest.getText().toString());


                return params;
            }
        };
        queue.add(sr);
    }

    private void GetProfileInfoRequest() {
        showWaitIndicator(true);
//        http://api.foodporntheory.com/user/1/physicalinfo/2018/1
        String newsURL = Constant.API_BASE_URL + "/user/" + BaseActivity.userID + "/physicalinfo/" + BaseActivity.mCurrentYear + "/" + BaseActivity.mCurrentMonth;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("PHYSICAL_INFO_API ", response.toString());
                GetProfileInfoData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(sr);
    }

    private void GetProfileInfoData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());


            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    JSONObject jResponseData = jObject.getJSONObject("responseData");

                    PhysicalInfo mPhysicalInfo = new PhysicalInfo();

                    mPhysicalInfo.setUnits(jResponseData.getString(Constant.units));
                    mPhysicalInfo.setWeight(jResponseData.getString(Constant.weight));
                    mPhysicalInfo.setHeight(jResponseData.getString(Constant.height));
                    mPhysicalInfo.setNavalCircumference(jResponseData.getString(Constant.navalCircumference));
                    mPhysicalInfo.setWaistCircumference(jResponseData.getString(Constant.waistCircumference));
                    mPhysicalInfo.setThighCircumference(jResponseData.getString(Constant.thighCircumference));
                    mPhysicalInfo.setCalfCircumference(jResponseData.getString(Constant.calfCircumference));
                    mPhysicalInfo.setArmCircumference(jResponseData.getString(Constant.armCircumference));
                    mPhysicalInfo.setChestCircumference(jResponseData.getString(Constant.chestCircumference));

                    SetData(mPhysicalInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SetData(PhysicalInfo mPhysicalInfo) {

        edt_physicaldata_weight.setText(mPhysicalInfo.getWeight());
        edt_physicaldata_height.setText(mPhysicalInfo.getHeight());
        edt_physicaldata_navel.setText(mPhysicalInfo.getNavalCircumference());
        edt_physicaldata_waist.setText(mPhysicalInfo.getWaistCircumference());
        edt_physicaldata_thigh.setText(mPhysicalInfo.getThighCircumference());
        edt_physicaldata_calf.setText(mPhysicalInfo.getCalfCircumference());
        edt_physicaldata_arm.setText(mPhysicalInfo.getArmCircumference());
        edt_physicaldata_chest.setText(mPhysicalInfo.getChestCircumference());

        MakeViewEditable(false);
    }

    public void MakeViewEditable(boolean isEditable) {

        edt_physicaldata_weight.setEnabled(isEditable);
        edt_physicaldata_height.setEnabled(isEditable);
        edt_physicaldata_navel.setEnabled(isEditable);
        edt_physicaldata_waist.setEnabled(isEditable);
        edt_physicaldata_thigh.setEnabled(isEditable);
        edt_physicaldata_calf.setEnabled(isEditable);
        edt_physicaldata_arm.setEnabled(isEditable);
        edt_physicaldata_chest.setEnabled(isEditable);
    }
}
package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.interfaces.PaymentMethodNonceCreatedListener;
import com.braintreepayments.api.models.PayPalAccountNonce;
import com.braintreepayments.api.models.PayPalRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.api.models.PostalAddress;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.CeilingInfo;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 06-Mar-18.
 */

public class PaymentActivity extends BaseActivity implements PaymentMethodNonceCreatedListener
{
    PaymentActivity mContext;

    ImageView image_close;
    CardView payment_card;
    Button btn_pay;
    LinearLayout layout_payment_main;
    TextView text_price_payment,text_date_payment;

    BraintreeFragment  mBraintreeFragment;
    private String mAuthorization;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment);

        mContext = this;

        setView();
        setData();
        overrideFonts(mContext,layout_payment_main);
        setClickListener();

        GetClientTokenRequest();
    }

    private void setView()
    {
        image_close = (ImageView) findViewById(R.id.image_close);

        payment_card = (CardView) findViewById(R.id.payment_card);

        btn_pay = (Button) findViewById(R.id.btn_pay);

        layout_payment_main = (LinearLayout) findViewById(R.id.layout_payment_main);

        text_price_payment = (TextView) findViewById(R.id.text_price_payment);
        text_date_payment = (TextView) findViewById(R.id.text_date_payment);
    }

    private void setData()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("E, MMMM dd");
        String strDate = mdformat.format(calendar.getTime());

        text_date_payment.setText(strDate);

//        if(BaseActivity.planPrice.equalsIgnoreCase("0.00"))
//        {
//            text_price_payment.setText("Free");
//        }
//        else
//        {
//            text_price_payment.setText("$"+BaseActivity.planPrice);
//        }
    }

    private void setClickListener()
    {
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        payment_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setupBraintreeAndStartExpressCheckout();

            }
        });

        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(PaymentActivity.this,SignupFinishActivity.class);
                startActivity(it);
            }
        });
    }

    private void GetClientTokenRequest() {
        showWaitIndicator(true);
//        http://api.foodporntheory.com/user/clienttoken
        String ClientTokenURL = Constant.API_BASE_URL + "/user/clienttoken";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, ClientTokenURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ClientToken_API ", response.toString());
                GetClientTokenData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(sr);
    }

    private void GetClientTokenData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());


            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    JSONObject jResponseData = jObject.getJSONObject("responseData");

                    mAuthorization = jResponseData.getString("clientToken");
                    Log.e("clientToken", "clientToken  : "+mAuthorization);

                    Toast.makeText(mContext, "Proceed to payment ", Toast.LENGTH_SHORT).show();

                    PrepareBraintree();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PrepareBraintree(){
        try {
            mBraintreeFragment = BraintreeFragment.newInstance(this, mAuthorization);
            // mBraintreeFragment is ready to use!
        } catch (InvalidArgumentException e) {
            // There was an issue with your authorization string.
            Log.e("catch", "catch  : "+e);
        }
    }

    public void setupBraintreeAndStartExpressCheckout() {
        PayPalRequest request = new PayPalRequest("1")
                .currencyCode("USD")
                .intent(PayPalRequest.INTENT_AUTHORIZE);
        PayPal.requestOneTimePayment(mBraintreeFragment, request);
    }

    @Override
    public void onPaymentMethodNonceCreated(PaymentMethodNonce paymentMethodNonce) {
        // Send nonce to server
        String nonce = paymentMethodNonce.getNonce();

        Log.e("nonce","nonce:  "+nonce);

        Toast.makeText(mContext, "nonce : "+nonce, Toast.LENGTH_SHORT).show();

        if (paymentMethodNonce instanceof PayPalAccountNonce) {
            PayPalAccountNonce payPalAccountNonce = (PayPalAccountNonce)paymentMethodNonce;

            // Access additional information
            String email = payPalAccountNonce.getEmail();
            String firstName = payPalAccountNonce.getFirstName();
            String lastName = payPalAccountNonce.getLastName();
            String phone = payPalAccountNonce.getPhone();

            // See PostalAddress.java for details
            PostalAddress billingAddress = payPalAccountNonce.getBillingAddress();
            PostalAddress shippingAddress = payPalAccountNonce.getShippingAddress();
        }
    }
}

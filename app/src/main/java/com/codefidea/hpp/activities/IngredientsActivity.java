package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.SelectableAdapter;
import com.codefidea.hpp.adapters.SelectableItem;
import com.codefidea.hpp.adapters.SelectableViewHolder;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.Ingredients;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class IngredientsActivity extends BaseActivity implements SelectableViewHolder.OnItemSelectedListener{

    IngredientsActivity mContext;
    RecyclerView rcv_cuisine;
    private ArrayList<Ingredients> mIngredientsList = new ArrayList<>();
    LinearLayout ll_cuisine_back, ll_cuisine_done;

    private boolean isInternet;

    SelectableAdapter mIngredientsAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisines);

        mContext = this;
        isInternet = new InternetStatus().isInternetOn(mContext);

        InitViews();
        setClickListener();


        if(BaseActivity.mIngredientsList.size() > 0)
        {
            mIngredientsList = BaseActivity.mIngredientsList;
            InitIngredientsRecyler();
        }
        else
        {
            if (isInternet) {
                sendIngredientsRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void InitViews(){
        rcv_cuisine = (RecyclerView)findViewById(R.id.rcv_cuisine);
        ll_cuisine_back = (LinearLayout)findViewById(R.id.ll_cuisine_back);
        ll_cuisine_done = (LinearLayout)findViewById(R.id.ll_cuisine_done);
    }

    private void setClickListener(){

        ll_cuisine_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_cuisine_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = getIntent();
//                intent.putExtra("IngredientsList", selectedItems);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void InitIngredientsRecyler() {
        rcv_cuisine.setLayoutManager(new LinearLayoutManager(mContext));
        mIngredientsAdapter = new SelectableAdapter(this, mIngredientsList,true);
        rcv_cuisine.setAdapter(mIngredientsAdapter);
        rcv_cuisine.setItemAnimator(new DefaultItemAnimator());
        mIngredientsAdapter.notifyDataSetChanged();
    }

    private void sendIngredientsRequest()
    {
        mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/ingredients
        String newsURL = Constant.API_BASE_URL+"/recipes/ingredients";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Ingredients_API ", response.toString());
                getingredientsData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("recipeId", "1");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getingredientsData(String response)
    {
        JSONArray ingredientsList = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if(res)
                {
                    ingredientsList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < ingredientsList.length(); i++)
                    {
                        Ingredients data = new Ingredients();

                        data.setIngredientsID(ingredientsList.getJSONObject(i).getString(Constant.id));
                        data.setIngredientsName(ingredientsList.getJSONObject(i).getString(Constant.ingredient));

                        mIngredientsList.add(data);
                    }

                    BaseActivity.mIngredientsList = mIngredientsList;

                    InitIngredientsRecyler();
                }
                else
                {

                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(SelectableItem selectableItem) {

        BaseActivity.mIngredientsSelectedList = mIngredientsAdapter.getSelectedItems();
//        Snackbar.make(rcv_cuisine,"Selected item is "+selectableItem.getIngredientsName()+
//                ", Totally  selectem item count is "+mIngredientsSelectedList.size(), Snackbar.LENGTH_LONG).show();
    }
}

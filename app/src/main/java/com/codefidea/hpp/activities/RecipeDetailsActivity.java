package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.RecipeImagesGridAdapter;
import com.codefidea.hpp.adapters.RecipeIngredientsAdapter;
import com.codefidea.hpp.adapters.RecipePrepStepAdapter;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.Video;
import com.codefidea.hpp.model.RecipeDetails;
import com.codefidea.hpp.model.RecipeImages;
import com.codefidea.hpp.model.RecipeIngredients;
import com.codefidea.hpp.model.RecipePrepStep;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raavan on 07-Mar-18.
 */

public class RecipeDetailsActivity extends BaseActivity {

    public static String TAG = "RecipeDetailsActivity";
    RecipeDetailsActivity mContext;
    private boolean isInternet;

    LinearLayout ll_parent_recipe_details ;

    private LinearLayout llRecipeDetailsBack, llRecipeDetailsFav;
    private ImageView imgRecipeDetailsFav, imgRecipeImage;
    private RelativeLayout rlRecipeImage;
    private RatingBar rtngbarRecipeDetails;
    private TextView txtRecipeDetailsName, txtRecipeDetailsTime, txtRecipeDetailsFavNo, txtRecipeDetailsDes, txtRecipeDetailsCusinename,
            txtRecipeOpenShoppinglist;

    RecyclerView rcv_prepration_steps, rcv_recipe_images, rcv_ingredients;

    ProgressBar progress_recipe_item;

    RecipePrepStepAdapter mRecipePrepStepAdapter;
    ArrayList<Integer> StepSelecList = new ArrayList<>();

    RecipeImagesGridAdapter mRecipeImagesGridAdapter;
    RecipeIngredientsAdapter mRecipeIngredientsAdapter;

    private ArrayList<RecipeIngredients> RecipeIngredientsList = new ArrayList<>();
    private ArrayList<RecipePrepStep> RecipePrepStepList = new ArrayList<>();
    private ArrayList<Video> RecipeVideosList = new ArrayList<>();
    private ArrayList<RecipeImages> RecipeImagesList = new ArrayList<>();

    private String isLiked;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipedetails);

        mContext = this;
        imageLoader = AppController.getInstance().getImageLoader();

        setView();
        setClickListener();
        overrideFonts(mContext , ll_parent_recipe_details);

        Log.e(TAG, ""+getIntent().getExtras().getString("id"));

        isInternet = new InternetStatus().isInternetOn(mContext);

        if (isInternet) {
            sendRecipesDetailRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        ll_parent_recipe_details = (LinearLayout)findViewById(R.id.ll_parent_recipe_details);

//        ll_image_container = (LinearLayout)findViewById(R.id.ll_image_container);
//        rl_recipe_image = (ImageView)findViewById(R.id.rl_recipe_image);

        llRecipeDetailsBack = (LinearLayout)findViewById( R.id.ll_recipe_details_back );
        llRecipeDetailsFav = (LinearLayout)findViewById( R.id.ll_recipe_details_fav );

        imgRecipeDetailsFav = (ImageView)findViewById( R.id.img_recipe_details_fav );
        imgRecipeImage = (ImageView)findViewById( R.id.img_recipe_image );

        rtngbarRecipeDetails = (RatingBar)findViewById( R.id.rtngbar_recipe_details );

        txtRecipeDetailsName = (TextView)findViewById( R.id.txt_recipe_details_name );
        txtRecipeDetailsTime = (TextView)findViewById( R.id.txt_recipe_details_time );
        txtRecipeDetailsFavNo = (TextView)findViewById( R.id.txt_recipe_details_fav_no );
        txtRecipeDetailsDes = (TextView)findViewById( R.id.txt_recipe_details_des );
        txtRecipeDetailsCusinename = (TextView)findViewById( R.id.txt_recipe_details_cusinename );

//        txtRecipeIngredientsList = (TextView)findViewById( R.id.txt_recipe_ingredients_list );
        txtRecipeOpenShoppinglist = (TextView)findViewById( R.id.txt_recipe_open_shoppinglist );

        rcv_prepration_steps = (RecyclerView)findViewById(R.id.rcv_prepration_steps);
        rcv_recipe_images = (RecyclerView)findViewById(R.id.rcv_recipe_images);
        rcv_ingredients = (RecyclerView)findViewById(R.id.rcv_ingredients);

        progress_recipe_item = (ProgressBar) findViewById(R.id.progress_recipe_item);

        rcv_prepration_steps.setNestedScrollingEnabled(false);
        rcv_recipe_images.setNestedScrollingEnabled(false);
        rcv_ingredients.setNestedScrollingEnabled(false);
    }

    private void setClickListener() {
        imgRecipeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.mVideoUrls = RecipeVideosList;

                Intent it = new Intent(mContext,NewsPlayActivity.class);
                startActivity(it);
            }
        });

//        ll_image_container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent it = new Intent(mContext,NewsPlayActivity.class);
//                startActivity(it);
//            }
//        });

        llRecipeDetailsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llRecipeDetailsFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLiked.equalsIgnoreCase("1"))
                {
                    //send unfavorite request
                    setUnFavoriteRecipe();

                }
                else
                {
                    //send favorite request
                    sendFavoriteRecipe();
                }
            }
        });

        rcv_prepration_steps.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcv_prepration_steps, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

//                Toast.makeText(mContext, "" + BaseActivity.CuisineID, Toast.LENGTH_SHORT).show();

                PrepareStepSelection(position);
                mRecipePrepStepAdapter.notifyDataSetChanged();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void sendRecipesDetailRequest()
    {
        showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/1/load/1
        String newsURL = Constant.API_BASE_URL+"/recipes/"+BaseActivity.userID+"/load/"+BaseActivity.RecipesID;
        Log.e("newsURL",""+newsURL);

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RECIPES_DETAILS_API ", response.toString());
                getRecipesDetailData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(sr);
    }

    private void getRecipesDetailData(String s)
    {
        Log.e(TAG, "In RecipesDetailData  ");

        try {
            JSONObject jObject = new JSONObject(s.toString());

            JSONArray ingredientsList = new JSONArray();
            JSONArray stepsList = new JSONArray();
            JSONArray videosList = new JSONArray();
            JSONArray imagesList = new JSONArray();

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if(res) {

                    Log.e(TAG, "In RecipesDetailData  "+ res);

                    JSONObject jResponseData = jObject.getJSONObject("responseData");

                    ingredientsList = jResponseData.getJSONArray(Constant.ingredients);
                    stepsList = jResponseData.getJSONArray(Constant.steps);
                    videosList = jResponseData.getJSONArray(Constant.videos);
                    imagesList = jResponseData.getJSONArray(Constant.images);

                    if(ingredientsList.length() > 0)
                    {
                        for (int i = 0; i < ingredientsList.length(); i++)
                        {
                            RecipeIngredients RecipeIngredientsData = new RecipeIngredients();

                            RecipeIngredientsData.setIngredientsID(ingredientsList.getJSONObject(i).getString(Constant.id));
                            RecipeIngredientsData.setIngredientsName(ingredientsList.getJSONObject(i).getString(Constant.ingredient));

                            RecipeIngredientsList.add(RecipeIngredientsData);
                        }
                    }

                    if(stepsList.length() > 0)
                    {
                        RecipePrepStepList.clear();

                        for (int i = 0; i < stepsList.length(); i++)
                        {
                            RecipePrepStep mRecipePrepStep = new RecipePrepStep();

                            mRecipePrepStep.setStepID(stepsList.getJSONObject(i).getString(Constant.id));
                            mRecipePrepStep.setStepNumb(stepsList.getJSONObject(i).getString(Constant.stepNumber));
                            mRecipePrepStep.setStepDiscription(stepsList.getJSONObject(i).getString(Constant.description));

                            RecipePrepStepList.add(mRecipePrepStep);
                        }
                    }

                    if(videosList.length() > 0)
                    {
                        for (int i = 0; i < videosList.length(); i++)
                        {
                            Video mRecipeVideos = new Video();

                            mRecipeVideos.setId(videosList.getJSONObject(i).getString(Constant.id));
                            mRecipeVideos.setVideoUrl(videosList.getJSONObject(i).getString(Constant.videoUrl));

//                            mRecipeVideos.setRecipeVideosID(videosList.getJSONObject(i).getString(Constant.id));
//                            mRecipeVideos.setRecipeVideosUrl(videosList.getJSONObject(i).getString(Constant.videoUrl));
//                            mRecipeVideos.setRecipeVideosShortOrder(videosList.getJSONObject(i).getString(Constant.sortOrder));

                            RecipeVideosList.add(mRecipeVideos);
                        }
                    }

                    if(imagesList.length() > 0)
                    {
                        RecipeImagesList.clear();
                        for (int i = 0; i < imagesList.length(); i++)
                        {
                            RecipeImages mRecipeImages = new RecipeImages();

                            mRecipeImages.setRecipeImagesID(imagesList.getJSONObject(i).getString(Constant.id));
                            mRecipeImages.setRecipeImagesUrl(imagesList.getJSONObject(i).getString(Constant.image));
                            mRecipeImages.setRecipeImagesShortOrder(imagesList.getJSONObject(i).getString(Constant.sortOrder));

                            RecipeImagesList.add(mRecipeImages);
                        }
                    }

                    RecipeDetails mRecipeDetails = new RecipeDetails();

                    mRecipeDetails.setRecipeID(jResponseData.getString(Constant.id));
                    mRecipeDetails.setRecipeName(jResponseData.getString(Constant.name));
                    mRecipeDetails.setRecipePrepTime(jResponseData.getString(Constant.preparationTime));
                    mRecipeDetails.setRecipeDifficulty(jResponseData.getString(Constant.difficulty));
                    mRecipeDetails.setRecipeRatings(jResponseData.getString(Constant.rating));
                    mRecipeDetails.setRecipeDiscription(jResponseData.getString(Constant.description));
                    mRecipeDetails.setRecipeCuisineName(jResponseData.getString(Constant.cuisineName));
                    mRecipeDetails.setRecipeIsLiked(jResponseData.getString(Constant.isLiked));
                    mRecipeDetails.setRecipeTotalLike(jResponseData.getString(Constant.totalLikes));
                    mRecipeDetails.setRecipeDisplayImagePath(jResponseData.getString(Constant.image));
//                    mRecipeDetails.setRecipeDisplayImagePath(jResponseData.getString(Constant.userProfilePic));
//                    mRecipeDetails.setRecipeIngredientsList(RecipeIngredientsList);
//                    mRecipeDetails.setRecipePrepStepList(RecipePrepStepList);
//                    mRecipeDetails.setRecipeVideosList(RecipeVideosList);
//                    mRecipeDetails.setRecipeImagesList(RecipeImagesList);

                    setData(mRecipeDetails);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setData(RecipeDetails mRecipeDetails)
    {
        Log.e(TAG, "URl "+RecipeImagesList.get(0).getRecipeImagesUrl());

        imageLoader.get(RecipeImagesList.get(0).getRecipeImagesUrl(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progress_recipe_item.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    imgRecipeImage.setImageBitmap(response.getBitmap());
                    progress_recipe_item.setVisibility(View.GONE);
                }
            }
        });

        txtRecipeDetailsName.setText(mRecipeDetails.getRecipeName());
        txtRecipeDetailsTime.setText(mRecipeDetails.getRecipePrepTime());
        txtRecipeDetailsFavNo.setText(mRecipeDetails.getRecipeTotalLike());
        txtRecipeDetailsDes.setText(mRecipeDetails.getRecipeDiscription());
        txtRecipeDetailsCusinename.setText(mRecipeDetails.getRecipeCuisineName());

        isLiked = mRecipeDetails.getRecipeIsLiked();


        if(mRecipeDetails.getRecipeIsLiked().equalsIgnoreCase("1"))
        {
            imgRecipeDetailsFav.setImageResource(R.drawable.ic_fav_selected);
        }
        else
        {
            imgRecipeDetailsFav.setImageResource(R.drawable.ic_fav_unselect);
        }

        rtngbarRecipeDetails.setRating(Float.parseFloat(mRecipeDetails.getRecipeRatings()));

        InitPrepStepsRecyler();
        InitRecipesImageRecyler();
        InitRecipeIngredientsRecyler();
    }

    private void InitPrepStepsRecyler() {

        PrepareStepSelection(0);

        rcv_prepration_steps.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_cuisine.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        mRecipePrepStepAdapter = new RecipePrepStepAdapter(mContext, RecipePrepStepList, StepSelecList);
        rcv_prepration_steps.setAdapter(mRecipePrepStepAdapter);
        rcv_prepration_steps.setItemAnimator(new DefaultItemAnimator());
        mRecipePrepStepAdapter.notifyDataSetChanged();
    }

    private void InitRecipesImageRecyler() {

//        rcv_prepration_steps.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_cuisine.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        rcv_recipe_images.setLayoutManager(new GridLayoutManager(this, 3));
        mRecipeImagesGridAdapter = new RecipeImagesGridAdapter(mContext, RecipeImagesList);
        rcv_recipe_images.setAdapter(mRecipeImagesGridAdapter);
        rcv_recipe_images.setItemAnimator(new DefaultItemAnimator());
        mRecipeImagesGridAdapter.notifyDataSetChanged();
    }

    private void InitRecipeIngredientsRecyler() {

        rcv_ingredients.setLayoutManager(new LinearLayoutManager(mContext));
//        rcv_cuisine.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));

        mRecipeIngredientsAdapter = new RecipeIngredientsAdapter(mContext, RecipeIngredientsList);
        rcv_ingredients.setAdapter(mRecipeIngredientsAdapter);
        rcv_ingredients.setItemAnimator(new DefaultItemAnimator());
        mRecipeIngredientsAdapter.notifyDataSetChanged();
    }

    private void sendFavoriteRecipe() {
//            mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/like
        String newsFavURL = Constant.API_BASE_URL+"/recipes/like";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RECIPE_Favorite_API ", response.toString());
                getFavoriteData(response.toString());
//                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("recipeId", BaseActivity.RecipesID);
                params.put("userId", BaseActivity.userID);

                return params;
            }
        };
        queue.add(sr);

        sendRecipesDetailRequest();
    }

    private void setUnFavoriteRecipe()
    {
//            mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/unlike
        String newsFavURL = Constant.API_BASE_URL+"/recipes/unlike";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RECIPE_Favorite_API ", response.toString());
                getFavoriteData(response.toString());
//                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("recipeId", BaseActivity.RecipesID);
                params.put("userId", BaseActivity.userID);

                return params;
            }
        };
        queue.add(sr);

        sendRecipesDetailRequest();
    }

    private void getFavoriteData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
                else {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }



    private void PrepareStepSelection(int position) {
        StepSelecList.clear();

        Log.e("SelectionList", "SelectionList size" + mCuisinesList.size());
        for (int i = 0; i <= RecipePrepStepList.size(); i++) {
            Log.e("SelectionList", "SelectionList" + i);
            if (i == position) {
                StepSelecList.add(1);

            } else {

                StepSelecList.add(0);
            }

        }
    }

}

package com.codefidea.hpp.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 23-Mar-18.
 */

public class ForgotPasswordActivity extends BaseActivity
{
    ForgotPasswordActivity mContext;

    boolean isInternet;

    LinearLayout layout_forgot_main,ll_forgot_back;

    EditText edt_forgot_username;

    Button btn_forgot;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setClickListener();
    }

    private void setView()
    {
        layout_forgot_main = findViewById(R.id.layout_forgot_main);
        ll_forgot_back = findViewById(R.id.ll_forgot_back);

        edt_forgot_username = findViewById(R.id.edt_forgot_username);

        btn_forgot = findViewById(R.id.btn_forgot);
    }

    private void setClickListener()
    {
        ll_forgot_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_forgot_username.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter valid user name or email", Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendForgotPasswordRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void sendForgotPasswordRequest()
    {
        showWaitIndicator(true);

        String forgotURL = Constant.API_BASE_URL + "/user/forgotpassword";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST, forgotURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("FORGOT_API ", response.toString());
                getForgotData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", edt_forgot_username.getText().toString());

                return params;
            }
        };
        queue.add(sr);
    }

    private void getForgotData(String s)
    {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    edt_forgot_username.setText("");
                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                } else {
                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

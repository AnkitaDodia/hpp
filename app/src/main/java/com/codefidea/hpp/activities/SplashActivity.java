package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    Context mContext;


//    ProgressBar mPrg_splash;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mContext = this;

//        mPrg_splash = (ProgressBar)findViewById(R.id.mPrg_splash);
//        mPrg_splash.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (getLogin() == 1) {
                    Intent i = new Intent(mContext, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(mContext, SampleIntroActivity.class);
                    startActivity(i);
                    overridePendingTransitionEnter();
//                    overridePendingTransitionEnter();
//                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
//        overridePendingTransition(R.anim.slide_up, R.anim.slide_up);
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }
}

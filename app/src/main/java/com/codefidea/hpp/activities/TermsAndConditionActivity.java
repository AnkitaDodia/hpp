package com.codefidea.hpp.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;

/**
 * Created by My 7 on 06-Mar-18.
 */

public class TermsAndConditionActivity extends BaseActivity {

    Context mContext;

    LinearLayout ll_terms_back;

    boolean isInternet;

    WebView webview_terms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);

        mContext= this;

        setView();
        setClickListener();

        isInternet = new InternetStatus().isInternetOn(mContext);

        if (isInternet) {
            String termsURL = Constant.API_BASE_URL+"/terms";

            webview_terms.loadUrl(termsURL);
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {

        ll_terms_back = findViewById(R.id.ll_terms_back);

        webview_terms = findViewById(R.id.webview_terms);
    }

    private void setClickListener() {
        ll_terms_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

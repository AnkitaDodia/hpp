package com.codefidea.hpp.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.UserData;
import com.codefidea.hpp.util.InternetStatus;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;

/**
 * Created by My 7 on 05-Mar-18.
 */

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener
{
    Context mContext;

    LinearLayout ll_login_parent;

    EditText edt_login_username, edt_login_password;

    Button btn_login;

    TextView txt_signup,txt_forgotPassword;

    ImageButton imgbtn_google,imgbtn_fb,imgbtn_twitter;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    boolean isInternet;

    String socialPhotoPath;

    //For Google
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private ProgressDialog mProgressDialog;

    //For FB
    private final String PUBLICPROFILE_PERMISSION = "public_profile";
    private final String EMAIL_PERMISSION = "email";
    private final String USERABOUT_ME = "user_about_me";
    private CallbackManager callbackManager;
    private String profile_picture,name,uniq_user_id,str_email,linkProfile;
    private static boolean fromFB = false;

    //For twitter
    private TwitterAuthClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        initGoogle();
        initFacebook();
        initTwitter();
        setView();
        setClickListener();
        overrideFonts(mContext , ll_login_parent);
    }

    private void initGoogle() {
        //Code for google login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initFacebook() {
        //Code for facebook login

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile profile = Profile.getCurrentProfile();

                        GraphRequest request = GraphRequest.newMeRequest(
                                accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {

                                        if (object != null) {

                                            try {

                                                JSONObject picture = object.getJSONObject("picture");
                                                JSONObject data = picture.getJSONObject("data");

                                                profile_picture = data.getString("url");
                                                name = object.getString("name");
                                                uniq_user_id = object.getString("id");
                                                str_email = object.getString("email");
                                                linkProfile = object.getString("link");

                                                Log.e("USER",""+profile_picture);
                                                Log.e("USER",""+name);
                                                Log.e("USER",""+uniq_user_id);
                                                Log.e("USER",""+str_email);
                                                Log.e("USER",""+linkProfile);

                                                setLoginPreference(name,str_email,profile_picture,uniq_user_id);

                                                isFromSocialLogin = true;
                                                startActivity(new Intent(LoginActivity.this,SingUpActivity.class));

                                            } catch (Exception e) {

                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture,link");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    private void initTwitter()
    {
        client = new TwitterAuthClient();
    }

    private void setView()
    {
        ll_login_parent= (LinearLayout)findViewById(R.id.ll_login_parent);

        edt_login_username = (EditText) findViewById(R.id.edt_login_username);
        edt_login_password = (EditText) findViewById(R.id.edt_login_password);

        txt_signup = (TextView) findViewById(R.id.txt_signup);
        txt_forgotPassword = (TextView) findViewById(R.id.txt_forgotPassword);

        btn_login = (Button) findViewById(R.id.btn_login);

        imgbtn_google = (ImageButton) findViewById(R.id.imgbtn_google);
        imgbtn_fb = (ImageButton) findViewById(R.id.imgbtn_fb);
        imgbtn_twitter = (ImageButton) findViewById(R.id.imgbtn_twitter);
    }

    private void setClickListener()
    {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent it = new Intent(LoginActivity.this,MainActivity.class);
//                startActivity(it);
//                finish();

                if(edt_login_username.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else if(edt_login_password.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter password",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (isInternet) {
                        sendLoginRequest();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        txt_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(LoginActivity.this,SingUpActivity.class);
                startActivity(it);
            }
        });

        txt_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(it);
            }
        });

        imgbtn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    // Make the call to GoogleApiClient
                    signIn();
                }
            }
        });

        imgbtn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromFB = true;
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile"));
            }
        });

        imgbtn_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private TwitterSession getTwitterSession() {
        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();

        //NOTE : if you want to get token and secret too use uncomment the below code
        /*TwitterAuthToken authToken = session.getAuthToken();
        String token = authToken.token;
        String secret = authToken.secret;*/

        return session;
    }

    private void sendLoginRequest()
    {
        showWaitIndicator(true);

        String loginURL = Constant.API_BASE_URL + "/user/login";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.POST, loginURL, new com.android.volley.Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                Log.e("LOGIN_API ", response.toString());
                getLoginData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", edt_login_username.getText().toString());
                params.put("password", edt_login_password.getText().toString());

                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void getLoginData(String s)
    {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("success");

                if (res) {
                    JSONObject responseData = jObject.getJSONObject("responseData");

                    UserData data = new UserData();

                    data.setWeekTimes(responseData.getString("weekTimes"));
                    data.setUserType(responseData.getString("userType"));
                    data.setUsername(responseData.getString("username"));
                    data.setSurname(responseData.getString("surname"));
                    data.setSportsId(responseData.getString("sportsId"));
                    data.setProfilePic(responseData.getString("profilePic"));
                    data.setPracticeSportsFlag(responseData.getString("practiceSportsFlag"));
                    data.setPoints(responseData.getString("points"));
                    data.setPlanId(responseData.getString("planId"));
                    data.setPhone(responseData.getString("phone"));
                    data.setName(responseData.getString("name"));
                    data.setMorpheTypeId(responseData.getString("morpheTypeId"));
                    data.setLifestyleTypeId(responseData.getString("lifestyleTypeId"));
                    data.setIsActive(responseData.getString("isActive"));
                    data.setId(responseData.getString("id"));
                    data.setGender(responseData.getString("gender"));
                    data.setFiscalCode(responseData.getString("fiscalCode"));
                    data.setEmail(responseData.getString("email"));
                    data.setDeviceId(responseData.getString("deviceId"));
                    data.setCountryId(responseData.getString("countryId"));
                    data.setCityId(responseData.getString("cityId"));
                    data.setBirthDate(responseData.getString("birthDate"));
                    data.setAddress(responseData.getString("address"));
                    data.setAboutMe(responseData.getString("aboutMe"));

                    updateUser(data);

                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();

                    setLogin(1);

                    Intent it = new Intent(LoginActivity.this,MainActivity.class);
                    startActivity(it);
                    finish();
                } else {
                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        try {
            Log.d("LOGIN_ACTIVITY", "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();

                Log.e("LOGIN_ACTIVITY", "acct: " + acct);

                String personName = acct.getDisplayName();
                Uri personPhoto = acct.getPhotoUrl();
                String email = acct.getEmail();
                String uniq_user_id = acct.getId();

                if(personPhoto != null)
                {
                    socialPhotoPath = personPhoto.toString();
                }

                Log.e("LOGIN_ACTIVITY", "Name: " + personName + ", email: " + email
                        + ", Image: " + personPhoto+ ", Unique_id: " + uniq_user_id);

                setLoginPreference(personName,email,socialPhotoPath,uniq_user_id);

                signOut();

                isFromSocialLogin = true;
                startActivity(new Intent(LoginActivity.this,SingUpActivity.class));
            } else {
                // Signed out, show unauthenticated UI.
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Please wait..!");
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    // [START auth_with_twitter]
    public void customLoginTwitter()
    {
        if (getTwitterSession() == null) {

            //if user is not authenticated start authenticating
            client.authorize(this, new Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {

                    // Do something with result, which provides a TwitterSession for making API calls
                    TwitterSession twitterSession = result.data;

                    //call fetch email only when permission is granted
                    fetchTwitterDetail(twitterSession);
                }

                @Override
                public void failure(TwitterException e) {
                    // Do something on failure
                    Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is already authenticated direct call fetch twitter email api
            Toast.makeText(this, "User already authenticated", Toast.LENGTH_SHORT).show();
            fetchTwitterDetail(getTwitterSession());
        }
    }

    private void fetchTwitterDetail(final TwitterSession twitterSession)
    {
        if (twitterSession != null)
        {
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();

            Call<User> call = twitterApiClient.getAccountService().verifyCredentials(true, false, true);
            call.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {
                    User user = result.data;

                    String imageProfileUrl = user.profileImageUrl;
                    Log.e("Login_activity", "Data : " + imageProfileUrl);

                    imageProfileUrl = imageProfileUrl.replace("_normal", "");

                    setLoginPreference(user.screenName,user.email,imageProfileUrl,String.valueOf(user.id));

                    isFromSocialLogin = true;
                    startActivity(new Intent(LoginActivity.this,SingUpActivity.class));
                }

                @Override
                public void failure(TwitterException exception) {
                    Toast.makeText(LoginActivity.this, "Failed to authenticate. Please try again.", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            //if user is not authenticated first ask user to do authentication
            Toast.makeText(this, "First to Twitter auth to Verify Credentials.", Toast.LENGTH_SHORT).show();
        }
    }

    // [END auth_with_twitter]

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        else if(fromFB)
        {
            fromFB = false;
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        else
        {
            if (client != null)
                client.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("LOGIN_ACTIVITY", "onConnectionFailed:" + connectionResult);
    }
}

package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.fragments.NewsFragment;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.model.Comment;
import com.codefidea.hpp.model.NewsDetail;
import com.codefidea.hpp.model.Video;
import com.example.supernova_emoji_library.Actions.EmojIconActions;
import com.example.supernova_emoji_library.Helper.EmojiconEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class NewsDetailActivity extends BaseActivity
{
    NewsDetailActivity mContext;

    View lay_parent_news_detail;

    ImageView image_back_news_detail,image_favorite_news_detail,img_news_detail,emoji_btn;

    TextView text_view_all,text_editor_name_detail_news,text_time_detail_news,text_news_des,text_location_news_detail,
            text_review_news_detail,text_coments_news_detail,text_editor_name_comment_1,text_editor_name_comment_2,
            text_time_comment_1,text_time_comment_2,text_des_comment_1,text_des_comment_2;

//    RelativeLayout lay_bg_list_news_detail;

    ProgressBar news_detail_progress,news_detail_progress_person_image,progress_comment_1,progress_comment_2;

    CircleImageView user_profile_news_detail,image_view_comment_1,image_view_comment_2;

    LinearLayout layout_comment_news_detail,layout_send;

    EmojiconEditText emojiconEditText, emojiconEditText2;

    boolean isInternet;

    private ArrayList<Video> videos = new ArrayList<>();
    private ArrayList<Comment> comments = new ArrayList<>();

    ImageLoader imageLoader;

    String isLiked,commentString;

    EmojIconActions emojIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        imageLoader = AppController.getInstance().getImageLoader();

        setView();
        overrideFonts(mContext , lay_parent_news_detail);
        setClickListener();

        if (isInternet) {
            sendNewsDetailRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setView() {
        layout_comment_news_detail = (LinearLayout) findViewById(R.id.layout_comment_news_detail);
        layout_send = (LinearLayout) findViewById(R.id.layout_send);

        lay_parent_news_detail = findViewById(R.id.lay_parent_news_detail);
//        lay_bg_list_news_detail = (RelativeLayout) findViewById(R.id.lay_bg_list_news_detail);

        image_back_news_detail = (ImageView) findViewById(R.id.image_back_news_detail);
        image_favorite_news_detail = (ImageView) findViewById(R.id.image_favorite_news_detail);
        img_news_detail = (ImageView) findViewById(R.id.img_news_detail);
        emoji_btn = (ImageView) findViewById(R.id.emoji_btn);

        text_view_all = (TextView) findViewById(R.id.text_view_all);
        text_editor_name_detail_news = (TextView) findViewById(R.id.text_editor_name_detail_news);
        text_time_detail_news = (TextView) findViewById(R.id.text_time_detail_news);
        text_news_des = (TextView) findViewById(R.id.text_news_des);
        text_location_news_detail = (TextView) findViewById(R.id.text_location_news_detail);
        text_review_news_detail = (TextView) findViewById(R.id.text_review_news_detail);
        text_coments_news_detail = (TextView) findViewById(R.id.text_coments_news_detail);
        text_editor_name_comment_1 = (TextView) findViewById(R.id.text_editor_name_comment_1);
        text_editor_name_comment_2 = (TextView) findViewById(R.id.text_name_comment_2);
        text_time_comment_1 = (TextView) findViewById(R.id.text_time_comment_1);
        text_time_comment_2 = (TextView) findViewById(R.id.text_time_comment_2);
        text_des_comment_1 = (TextView) findViewById(R.id.text_des_comment_1);
        text_des_comment_2 = (TextView) findViewById(R.id.text_des_comment_2);

        news_detail_progress = (ProgressBar) findViewById(R.id.news_detail_progress);
        news_detail_progress_person_image = (ProgressBar) findViewById(R.id.news_detail_progress_person_image);
        progress_comment_1 = (ProgressBar) findViewById(R.id.progress_comment_1);
        progress_comment_2 = (ProgressBar) findViewById(R.id.progress_comment_2);

        user_profile_news_detail = (CircleImageView) findViewById(R.id.user_profile_news_detail);
        image_view_comment_1 = (CircleImageView) findViewById(R.id.image_view_comment_1);
        image_view_comment_2 = (CircleImageView) findViewById(R.id.image_view_comment_2);

        emojiconEditText = (EmojiconEditText) findViewById(R.id.edit_comment_news_detail);
        emojiconEditText2 = (EmojiconEditText) findViewById(R.id.emojicon_edit_text2);

        emojIcon = new EmojIconActions(this, lay_parent_news_detail, emojiconEditText, emoji_btn);
        emojIcon.ShowEmojIcon();
        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
            @Override
            public void onKeyboardOpen() {
                Log.e("Keyboard", "open");
            }

            @Override
            public void onKeyboardClose() {
                Log.e("Keyboard", "close");
            }
        });

        emojIcon.addEmojiconEditTextList(emojiconEditText2);
    }

    private void setClickListener()
    {
        image_back_news_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_news_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.mVideoUrls = videos;

                Intent it = new Intent(NewsDetailActivity.this,NewsPlayActivity.class);
                startActivity(it);
            }
        });

        text_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext,ViewAllCommentsActivity.class);
                startActivity(it);
            }
        });

        image_favorite_news_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLiked.equalsIgnoreCase("1"))
                {
                    //send unfavorite request
                    setUnFavoriteNews();

                }
                else
                {
                    //send favorite request
                    sendFavoriteNews();
                }
            }
        });

        layout_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentString = emojiconEditText.getText().toString();

                emojiconEditText.setText("");
                emojiconEditText2.setText("");
                sendCommentRequest();
            }
        });
    }

    private void sendNewsDetailRequest()
    {
        if(isProgressVisible())
        {
            showWaitIndicator(false);
        }
        showWaitIndicator(true);

        String newsURL = Constant.API_BASE_URL+"/news/"+BaseActivity.userID+"/load/"+BaseActivity.newsID;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET,newsURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("News_detail_API ", response.toString());
                getNewsDetailData(response.toString());
                showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }
        };
        queue.add(sr);
    }

    private void getNewsDetailData(String s)
    {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            JSONArray newsVideoList = new JSONArray();
            JSONArray newsCommentList = new JSONArray();

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if(res) {
                    JSONObject jResponseData = jObject.getJSONObject("responseData");

                    newsVideoList = jResponseData.getJSONArray(Constant.videos);
                    newsCommentList = jResponseData.getJSONArray(Constant.comments);

                    if(newsVideoList.length() > 0)
                    {
                        for (int i = 0; i < newsVideoList.length(); i++)
                        {
                            Video videoData = new Video();

                            videoData.setId(newsVideoList.getJSONObject(i).getString(Constant.id));
                            videoData.setVideoUrl(newsVideoList.getJSONObject(i).getString(Constant.videoUrl));

                            videos.add(videoData);
                        }
                    }

                    if(newsCommentList.length() > 0)
                    {
                        for (int i = 0; i < newsCommentList.length(); i++)
                        {
                            Comment commentData = new Comment();

                            commentData.setId(newsCommentList.getJSONObject(i).getString(Constant.id));
                            commentData.setUserId(newsCommentList.getJSONObject(i).getString(Constant.userId));
                            commentData.setComment(newsCommentList.getJSONObject(i).getString(Constant.comment));
                            commentData.setCreatedAt(newsCommentList.getJSONObject(i).getString(Constant.createdAt));
                            commentData.setUpdatedAt(newsCommentList.getJSONObject(i).getString(Constant.updatedAt));
                            commentData.setUserFirstName(newsCommentList.getJSONObject(i).getString(Constant.userFirstName));
                            commentData.setUserLastName(newsCommentList.getJSONObject(i).getString(Constant.userLastName));
                            commentData.setUserProfilePic(newsCommentList.getJSONObject(i).getString(Constant.userProfilePic));

                            comments.add(commentData);
                        }

                        BaseActivity.comments = comments;
                    }

                    NewsDetail data = new NewsDetail();

                    data.setId(jResponseData.getString(Constant.id));
                    data.setTitle(jResponseData.getString(Constant.title));
                    data.setImage(jResponseData.getString(Constant.image));
                    data.setBody(jResponseData.getString(Constant.body));
                    data.setUserId(jResponseData.getString(Constant.userId));
                    data.setCreatedAt(jResponseData.getString(Constant.createdAt));
                    data.setUpdatedAt(jResponseData.getString(Constant.updatedAt));
                    data.setUserFirstName(jResponseData.getString(Constant.userFirstName));
                    data.setUserLastName(jResponseData.getString(Constant.userLastName));
                    data.setUserProfilePic(jResponseData.getString(Constant.userProfilePic));
                    data.setTotalLikes(jResponseData.getString(Constant.totalLikes));
                    data.setIsLiked(jResponseData.getString(Constant.isLiked));
                    data.setVideos(videos);
                    data.setComments(comments);

                    setData(data);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setData(NewsDetail data)
    {
        isLiked = data.getIsLiked();

        text_editor_name_detail_news.setText(data.getUserFirstName()+" "+data.getUserLastName());
        text_time_detail_news.setText(data.getCreatedAt());
        text_news_des.setText(data.getBody());
        text_location_news_detail.setText(data.getCreatedAt());
        text_review_news_detail.setText(data.getTotalLikes());
        text_coments_news_detail.setText(data.getComments().size() + " Comments");

        if(data.getComments().size() > 0)
        {
            layout_comment_news_detail.setVisibility(View.VISIBLE);

            text_editor_name_comment_1.setText(data.getComments().get(0).getUserFirstName() + data.getComments().get(0).getUserLastName());
            text_editor_name_comment_2.setText(data.getComments().get(1).getUserFirstName() + data.getComments().get(1).getUserLastName());
            text_time_comment_1.setText(data.getComments().get(0).getCreatedAt());
            text_time_comment_2.setText(data.getComments().get(1).getCreatedAt());
            text_des_comment_1.setText(data.getComments().get(0).getComment());
            text_des_comment_2.setText(data.getComments().get(1).getComment());
        }
        else
        {
            layout_comment_news_detail.setVisibility(View.GONE);
        }

        imageLoader.get(data.getImage(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                news_detail_progress.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    img_news_detail.setImageBitmap(response.getBitmap());
                    news_detail_progress.setVisibility(View.GONE);
                }
            }
        });

        imageLoader.get(data.getUserProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                news_detail_progress_person_image.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    user_profile_news_detail.setImageBitmap(response.getBitmap());
                    news_detail_progress_person_image.setVisibility(View.GONE);
                }
            }
        });

        if(data.getIsLiked().equalsIgnoreCase("1"))
        {
            image_favorite_news_detail.setImageResource(R.drawable.ic_fav_selected);
        }
        else
        {
            image_favorite_news_detail.setImageResource(R.drawable.ic_fav_unselect);
        }

        imageLoader.get(data.getComments().get(0).getUserProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    image_view_comment_1.setImageBitmap(response.getBitmap());
                    progress_comment_1.setVisibility(View.GONE);
                }
            }
        });

        imageLoader.get(data.getComments().get(1).getUserProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                progress_comment_2.setVisibility(View.GONE);
                progress_comment_2.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    image_view_comment_2.setImageBitmap(response.getBitmap());
                    progress_comment_2.setVisibility(View.GONE);
                }
            }
        });
    }

    private void sendFavoriteNews() {
        mContext.showWaitIndicator(true);

        String newsFavURL = Constant.API_BASE_URL+"/news/like";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("News_Favorite_API ", response.toString());
                getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("newsId", BaseActivity.newsID);
                params.put("userId", BaseActivity.userID);

                return params;
            }
        };
        queue.add(sr);
    }

    private void getFavoriteData(String s) {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();

                    NewsFragment.shouldRefreshOnResume = true;
                    sendNewsDetailRequest();
                }
                else {
                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setUnFavoriteNews()
    {
        mContext.showWaitIndicator(true);

        String newsFavURL = Constant.API_BASE_URL+"/news/unlike";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("News_Favorite_API ", response.toString());
                getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("newsId", BaseActivity.newsID);
                params.put("userId", BaseActivity.userID);

                return params;
            }
        };
        queue.add(sr);
    }

    private void sendCommentRequest()
    {
        mContext.showWaitIndicator(true);

        String newsCommentURL = Constant.API_BASE_URL+"/news/comment/add";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsCommentURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("News_Comment_API ", response.toString());
                getCommentData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("newsId", BaseActivity.newsID);
                params.put("userId", BaseActivity.userID);
                params.put("comment", commentString);

                return params;
            }
        };
        queue.add(sr);

    }

    private void getCommentData(String s)
    {
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                    sendNewsDetailRequest();
                }
                else {
                    Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

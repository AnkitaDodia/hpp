package com.codefidea.hpp.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.AndroidMultiPartEntity;
import com.codefidea.hpp.util.ImageConverter;
import com.codefidea.hpp.util.InternetStatus;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by My 7 on 05-Mar-18.
 */

public class SingUpActivity extends BaseActivity {

    Context mContext;

    LinearLayout ll_signup_parent, ll_signup_back;

    TextView text_title, setProfileHint, text_info_signup;

    EditText edit_userName_signup, edit_email_signup, edit_password_signup, edit_confirm_password;

    Button btn_signup;

    String first = "By, Creating Account,you are automatically accepting all the";
    String second = "<font color='#9ccb58'> Terms & Conditions</font>";
    String third = " related to HHP";

    CircleImageView user_pic_image;

    ImageView image_user_pic_image;

    String filemanagerstring, selectedImagePath = null;

    private static final int PICK_IMAGE = 1;
    Bitmap bitmap,personBitmap;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    String userName,userEmail,userPassword;

    boolean isInternet;

    String IMEI_number;

    public static final int REQUEST_READ_PHONE_STATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        setView();
        setClickListener();
        overrideFonts(mContext , ll_signup_parent);
        setSocialData();

        //Code for check run time permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        } else {
            //TODO
            IMEI_number = getIMEINumber();
//            IMEI_number = Static_IMEI;
            Log.e("IMEI_NUMBER" , IMEI_number);
        }
    }

    private void setView() {

        ll_signup_parent = (LinearLayout)findViewById(R.id.ll_signup_parent);
        ll_signup_back = (LinearLayout)findViewById(R.id.ll_signup_back);

        text_title = (TextView) findViewById(R.id.text_title);
        setProfileHint = (TextView) findViewById(R.id.setProfileHint);
        text_info_signup = (TextView) findViewById(R.id.text_info_signup);

        edit_userName_signup = (EditText) findViewById(R.id.edit_userName_signup);
        edit_email_signup = (EditText) findViewById(R.id.edit_email_signup);
        edit_password_signup = (EditText) findViewById(R.id.edit_password_signup);
        edit_confirm_password = (EditText) findViewById(R.id.edit_confirm_password);

        btn_signup = (Button) findViewById(R.id.btn_signup);

        user_pic_image = (CircleImageView) findViewById(R.id.user_pic_image);

        image_user_pic_image = (ImageView) findViewById(R.id.image_user_pic_image);

        text_info_signup.setText(Html.fromHtml(first + second + third));
    }

    private void setSocialData()
    {
        if(isFromSocialLogin)
        {
            isFromSocialLogin = false;

            ImageLoader imageLoader = AppController.getInstance().getImageLoader();

            SharedPreferences sp = getLoginPreference();

            profilePic = sp.getString("PHOTO","");

            edit_userName_signup.setText(sp.getString("NAME",""));
            edit_email_signup.setText(sp.getString("EMAIL",""));

            if(!profilePic.equalsIgnoreCase("null"))
            {
                imageLoader.get(profilePic, new ImageLoader.ImageListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROR", "Image Load Error: " + error.getMessage());
//                    holder.progress_chat.setVisibility(View.GONE);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            user_pic_image.setImageBitmap(response.getBitmap());
//                        holder.progress_chat.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }
    }

    private void setClickListener() {
        ll_signup_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        text_info_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(mContext, TermsAndConditionActivity.class);
                startActivity(it);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.email = edit_email_signup.getText().toString();
                BaseActivity.profilePic = selectedImagePath;

//                Intent it = new Intent(mContext, SignUpPagerActivity.class);
//                startActivity(it);

                if(selectedImagePath == null)
                {
                    Toast.makeText(mContext,"Please select profile image",Toast.LENGTH_LONG).show();
                }
                else if(edit_userName_signup.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter user name",Toast.LENGTH_LONG).show();
                }
                else if(edit_email_signup.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter valid email",Toast.LENGTH_LONG).show();
                }
                else if (!edit_email_signup.getText().toString().matches(emailPattern) && edit_email_signup.getText().toString().length() > 0)
                {
                    Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                }
                else if(edit_password_signup.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter password",Toast.LENGTH_LONG).show();
                }
                else if(edit_confirm_password.getText().toString().length() == 0)
                {
                    Toast.makeText(mContext,"Please enter password",Toast.LENGTH_LONG).show();
                }
                else if(!edit_confirm_password.getText().toString().equalsIgnoreCase(edit_password_signup.getText().toString()))
                {
                    Toast.makeText(mContext,"Password must be same",Toast.LENGTH_LONG).show();
                }
                else
                {
                    userName = edit_userName_signup.getText().toString();
                    userEmail = edit_email_signup.getText().toString();
                    userPassword = edit_confirm_password.getText().toString();

                    if (isInternet) {
                        new UploadFileToServer().execute();
                    } else {
                        Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        user_pic_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                        && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    AskForPermission();
                }
                else
                {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
            }
        });
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void decodeFile(String filePath, int code) {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        if (code == 1) {
            bitmap = BitmapFactory.decodeFile(filePath, o2);
            personBitmap = bitmap;
            user_pic_image.setVisibility(View.VISIBLE);
            image_user_pic_image.setVisibility(View.GONE);
            user_pic_image.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int requestcode, Intent data) {
        super.onActivityResult(requestCode, requestcode, data);

        if (requestcode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            String filePath = null;

            try {
                // OI FILE Manager
                filemanagerstring = selectedImageUri.getPath();

                // MEDIA GALLERY
                selectedImagePath = getPath(selectedImageUri);
                Log.e("selectedImagePath", "path : "+selectedImagePath);

                if (selectedImagePath != null) {
                    filePath = selectedImagePath;
                } else if (filemanagerstring != null) {
                    filePath = filemanagerstring;
                } else {
                    Toast.makeText(mContext,"Unknown path",Toast.LENGTH_LONG).show();
                    Log.e("Bitmap", "Unknown path");
                }

                if (filePath != null) {
//                    decodeFile(filePath, 1);
                    bitmap = BitmapFactory.decodeFile(filePath);
                    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                    user_pic_image.setImageBitmap(circularBitmap);
                    user_pic_image.setVisibility(View.VISIBLE);
                    image_user_pic_image.setVisibility(View.GONE);
                } else {
                    bitmap = null;
                }

            } catch (Exception e)
            {
                Toast.makeText(mContext,"Internal Error",Toast.LENGTH_LONG).show();
                Log.e(e.getClass().getName(), e.getMessage(), e);
            }
        }
    }

    private void AskForPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_read_storage_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {

        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE);
                }
                break;
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    IMEI_number = getIMEINumber();
//                    IMEI_number = Static_IMEI;
                    Log.e("IMEI_NUMBER_OVERRIDE" , IMEI_number);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Requests given permission.
     * If the permission has been denied previously, a Dialog will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    protected void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            showAlertDialog(getString(R.string.permission_title_rationale), rationale,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(SingUpActivity.this,
                                    new String[]{permission}, requestCode);
                        }
                    }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }
    /**
     * This method shows dialog with given title & message.
     * Also there is an option to pass onClickListener for positive & negative button.
     *
     * @param title                         - dialog title
     * @param message                       - dialog message
     * @param onPositiveButtonClickListener - listener for positive button
     * @param positiveText                  - positive button text
     * @param onNegativeButtonClickListener - listener for negative button
     * @param negativeText                  - negative button text
     */
    protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                   @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                   @NonNull String positiveText,
                                   @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                   @NonNull String negativeText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
        AlertDialog mAlertDialog = builder.show();
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String>
    {
        String createUserURL = Constant.API_BASE_URL+"/user/add";
        long totalSize = 0;

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            showWaitIndicator(true);
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(createUserURL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(selectedImagePath);

                // Adding file data to http body
                entity.addPart("photo", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
                entity.addPart("username",new StringBody(userName));
                entity.addPart("email", new StringBody(userEmail));
                entity.addPart("password", new StringBody(userPassword));
                entity.addPart("deviceId", new StringBody(IMEI_number));
                entity.addPart("os", new StringBody("android"));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("UPLOAD_IMAGE", "Response from server: " + result);
            showWaitIndicator(false);

            getUserCreateData(result);
            super.onPostExecute(result);
        }
    }

    private void getUserCreateData(String result)
    {
        try {
            JSONObject jObject = new JSONObject(result.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    JSONObject response = jObject.getJSONObject("responseData");

                    BaseActivity.userID = response.getString(Constant.id);
                    Toast.makeText(mContext,""+response.getString(Constant.message),Toast.LENGTH_LONG).show();

                    Intent it = new Intent(mContext, SignUpPagerActivity.class);
                    startActivity(it);
                }
                else
                {
                    Toast.makeText(mContext,""+jObject.getString(Constant.message),Toast.LENGTH_LONG).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

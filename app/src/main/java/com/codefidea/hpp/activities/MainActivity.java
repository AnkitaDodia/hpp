package com.codefidea.hpp.activities;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.fragments.AboutHPPFragment;
import com.codefidea.hpp.fragments.ChatFragment;
import com.codefidea.hpp.fragments.DiaryFragment;
import com.codefidea.hpp.fragments.LeaderBoardFragment;
import com.codefidea.hpp.fragments.MyProfileFragment;
import com.codefidea.hpp.fragments.NewsFragment;
import com.codefidea.hpp.fragments.RecipesFragment;
import com.codefidea.hpp.fragments.SettingFragment;
import com.codefidea.hpp.ui.CustomTypefaceSpan;
import com.codefidea.hpp.util.SettingsPreferences;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener
{
    MainActivity mContext;

    View headerLayout;

    CircleImageView imageView_nav;

    TextView name_nav, mail_nav, tv_title;

    public LinearLayout ll_app_bar_title, ll_app_bar_button, lay_bottom_home, lay_bottom_chat, lay_bottom_diary;

    public ImageView img_bottom_home, img_bottom_diary, img_bottom_chat, img_bottom_profile;

    public Button btn_recipe_grid, btn_recipe_list;

    android.app.Fragment currentFragment;

    //For exit from application
    boolean doubleBackToExitPressedOnce = false;
    //For exit from application

    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;


        imageLoader = AppController.getInstance().getImageLoader();
        setView();


        if (savedInstanceState == null) {
            NewsFragment newsFragment = new NewsFragment();
            replaceFragment(newsFragment, Constant.newsTitle);
        }

        lay_bottom_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);

                NewsFragment newsFragment = new NewsFragment();
                replaceFragment(newsFragment, Constant.newsTitle);
            }
        });

        lay_bottom_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);

                ChatFragment mChatFragment = new ChatFragment();
                replaceFragment(mChatFragment, Constant.ChatTitle);
            }
        });

        lay_bottom_diary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);


                DiaryFragment mDiaryFragment= new DiaryFragment();
                replaceFragment(mDiaryFragment, Constant.DiaryTitle);
            }
        });

        img_bottom_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
                img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);


                MyProfileFragment mMyProfileFragment= new MyProfileFragment();
                replaceFragment(mMyProfileFragment, Constant.myprofileTitle);
            }
        });
    }

    private void setView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);


        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        toggle.setHomeAsUpIndicator(R.drawable.ic_slidemenu);

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        setNaveMenuTypeFace(navigationView);
        getNavigationHeader(navigationView);

        ll_app_bar_title = (LinearLayout) findViewById(R.id.ll_app_bar_title);
        ll_app_bar_button = (LinearLayout) findViewById(R.id.ll_app_bar_button);

        btn_recipe_grid = (Button) findViewById(R.id.btn_recipe_grid);
        btn_recipe_list = (Button) findViewById(R.id.btn_recipe_list);

        tv_title = (TextView) findViewById(R.id.tv_title);

        lay_bottom_home = (LinearLayout) findViewById(R.id.lay_bottom_home);
        lay_bottom_chat = (LinearLayout) findViewById(R.id.lay_bottom_chat);
        lay_bottom_diary = (LinearLayout) findViewById(R.id.lay_bottom_diary);

        img_bottom_home= (ImageView) findViewById(R.id.img_bottom_home);
        img_bottom_diary= (ImageView) findViewById(R.id.img_bottom_diary);
        img_bottom_chat= (ImageView) findViewById(R.id.img_bottom_chat);
        img_bottom_profile= (ImageView) findViewById(R.id.img_bottom_profile);
    }

    private void getNavigationHeader(NavigationView navigationView) {
        headerLayout = navigationView.getHeaderView(0);

        imageView_nav = (CircleImageView) headerLayout.findViewById(R.id.imageView_nav);

        name_nav = (TextView) headerLayout.findViewById(R.id.name_nav);
        mail_nav = (TextView) headerLayout.findViewById(R.id.mail_nav);

        name_nav.setTypeface(getTypeFace());
        mail_nav.setTypeface(getTypeFace());

        if(getLogin() == 1)
        {
            name_nav.setText(SettingsPreferences.getConsumer(this).getName());
            mail_nav.setText(SettingsPreferences.getConsumer(this).getEmail());

            String imagePath = SettingsPreferences.getConsumer(this).getProfilePic();

            imageLoader.get(imagePath, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        imageView_nav.setImageBitmap(response.getBitmap());
                    }
                }
            });
        }
    }

    private void setNaveMenuTypeFace(NavigationView navView) {
        Menu m = navView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", getTypeFace()), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void replaceFragment(Fragment fragment, String title) {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.fragment_container, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();

        setTitle(title);

        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment,fragment.getClass().getName());
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commitAllowingStateLoss();

        currentFragment = fragment;
    }

    public void setTitle(String mTitle) {

        tv_title.setText(mTitle);
    }

    public void performBackTransaction()
    {
        android.app.FragmentManager fm = getFragmentManager();

        try {
            if(fm.getBackStackEntryCount() > 1)
            {
                Log.i("MainActivity","poping back stack: "+fm.getBackStackEntryCount());

                fm.popBackStackImmediate();

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                Log.e("MainActivity","Current fragment tag is: "+tag);

                findTheFragmentWhilePoping(tag);
            }
            else
            {
                Log.i("MainActivity","Nothing in back stack call super");
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();

                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    System.exit(0);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(mContext , "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findTheFragmentWhilePoping(String tag)
    {
        android.app.FragmentManager fm = getFragmentManager();

        if (tag.equalsIgnoreCase(AboutHPPFragment.class.getName())) {
            AboutHPPFragment f = (AboutHPPFragment) fm.findFragmentByTag(AboutHPPFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.AboutTitle);
        }
        else if (tag.equalsIgnoreCase(ChatFragment.class.getName())) {
            ChatFragment f = (ChatFragment) fm.findFragmentByTag(ChatFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.ChatTitle);

            img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        else if (tag.equalsIgnoreCase(DiaryFragment.class.getName())) {
            DiaryFragment f = (DiaryFragment) fm.findFragmentByTag(DiaryFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.DiaryTitle);

            img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        else if (tag.equalsIgnoreCase(LeaderBoardFragment.class.getName())) {
            LeaderBoardFragment f = (LeaderBoardFragment) fm.findFragmentByTag(LeaderBoardFragment.class.getName());
            currentFragment = f;
        }
        else if (tag.equalsIgnoreCase(MyProfileFragment.class.getName())) {
            MyProfileFragment f = (MyProfileFragment) fm.findFragmentByTag(MyProfileFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.myprofileTitle);
        }
        else if (tag.equalsIgnoreCase(NewsFragment.class.getName())) {
            NewsFragment f = (NewsFragment) fm.findFragmentByTag(NewsFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.newsTitle);

            img_bottom_home.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_diary.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_chat.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
            img_bottom_profile.setColorFilter(mContext.getResources().getColor(R.color.grey), android.graphics.PorterDuff.Mode.SRC_IN);
        }
        else if (tag.equalsIgnoreCase(RecipesFragment.class.getName())) {
            RecipesFragment f = (RecipesFragment) fm.findFragmentByTag(RecipesFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.recipeTitle);
        }
        else if (tag.equalsIgnoreCase(SettingFragment.class.getName())) {
            SettingFragment f = (SettingFragment) fm.findFragmentByTag(SettingFragment.class.getName());
            currentFragment = f;

            setTitle(Constant.settingsTitle);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_profile) {

            MyProfileFragment myProfileFragment = new MyProfileFragment();
            replaceFragment(myProfileFragment, Constant.myprofileTitle);

        } else if (id == R.id.nav_chats) {

            ChatFragment mChatFragment = new ChatFragment();
            replaceFragment(mChatFragment, Constant.ChatTitle);

        } else if (id == R.id.nav_diary) {

            DiaryFragment mDiaryFragment= new DiaryFragment();
            replaceFragment(mDiaryFragment, Constant.DiaryTitle);

        } else if (id == R.id.nav_recipes) {

            RecipesFragment mRecipesFragment = new RecipesFragment();
            replaceFragment(mRecipesFragment, Constant.recipeTitle);

        } else if (id == R.id.nav_leaderboard) {

            LeaderBoardFragment leaderBoardFragment = new LeaderBoardFragment();
            replaceFragment(leaderBoardFragment, MonthYearName());

        } else if (id == R.id.nav_settings) {

            SettingFragment settingFragment = new SettingFragment();
            replaceFragment(settingFragment, Constant.settingsTitle);

        } else if (id == R.id.nav_about) {

            AboutHPPFragment mAboutHPPFragment = new AboutHPPFragment();
            replaceFragment(mAboutHPPFragment, Constant.AboutTitle);

        } else if (id == R.id.nav_sign_out) {
            setLogin(0);
            SettingsPreferences.clearDB(mContext);

            Intent it = new Intent(mContext,LoginActivity.class);
            startActivity(it);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            performBackTransaction();
        }
    }
}

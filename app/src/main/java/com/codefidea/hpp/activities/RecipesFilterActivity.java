package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class RecipesFilterActivity extends BaseActivity {

    Context mContext;
    LinearLayout layParentRecipesFilter, llRecipesFilterBack, llPrepTime15min, llPrepTime30min, llPrepTime60min,
            llDifficultyEasy, llDifficultyMedium, llDifficultyHard, layCuisine, layIngredients;
    ImageView imgPrepTime15min, imgPrepTime30min, imgPrepTime60min;
    TextView txtPrepTime15min, txtPrepTime30min, txtPrepTime60min, txtDifficultyEasy, txtDifficultyMedium, txtDifficultyHard;
    Button btnRecipeFilterReset, btnRecipeFilterApply;

    public static final int CUISINE_REQUEST_CODE = 1, INGREDIENS_REQUEST_CODE = 2;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipes_filter_activity);

        mContext = this;

        InitViews();
        setClickListener();

        PrepTimeViewSetup(BaseActivity.Time);
        DifficultyViewSetup(BaseActivity.Difficulty);
    }

    private void InitViews() {

        layParentRecipesFilter = (LinearLayout) findViewById(R.id.lay_parent_recipes_filter);

        llRecipesFilterBack = (LinearLayout) findViewById(R.id.ll_recipes_filter_back);

        llPrepTime15min = (LinearLayout) findViewById(R.id.ll_prep_time_15min);
        imgPrepTime15min = (ImageView) findViewById(R.id.img_prep_time_15min);
        txtPrepTime15min = (TextView) findViewById(R.id.txt_prep_time_15min);

        llPrepTime30min = (LinearLayout) findViewById(R.id.ll_prep_time_30min);
        imgPrepTime30min = (ImageView) findViewById(R.id.img_prep_time_30min);
        txtPrepTime30min = (TextView) findViewById(R.id.txt_prep_time_30min);

        llPrepTime60min = (LinearLayout) findViewById(R.id.ll_prep_time_60min);
        imgPrepTime60min = (ImageView) findViewById(R.id.img_prep_time_60min);
        txtPrepTime60min = (TextView) findViewById(R.id.txt_prep_time_60min);

        llDifficultyEasy = (LinearLayout) findViewById(R.id.ll_difficulty_easy);
        llDifficultyMedium = (LinearLayout) findViewById(R.id.ll_difficulty_medium);
        llDifficultyHard = (LinearLayout) findViewById(R.id.ll_difficulty_hard);

        txtDifficultyEasy = (TextView) findViewById(R.id.txt_difficulty_easy);
        txtDifficultyMedium = (TextView) findViewById(R.id.txt_difficulty_medium);
        txtDifficultyHard = (TextView) findViewById(R.id.txt_difficulty_hard);

        layCuisine = (LinearLayout) findViewById(R.id.lay_cuisine);

        layIngredients = (LinearLayout) findViewById(R.id.lay_ingredients);

        btnRecipeFilterReset = (Button) findViewById(R.id.btn_recipe_filter_reset);
        btnRecipeFilterApply = (Button) findViewById(R.id.btn_recipe_filter_apply);

    }

    private void setClickListener() {

        llRecipesFilterBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = getIntent();
//                intent.putExtra("Time", BaseActivity.Time);
//                intent.putExtra("Difficulty", BaseActivity.Difficulty);
//                intent.putExtra("CuisineID", BaseActivity.CuisineID);
                intent.putExtra("FilterStatus", "Cancle");

                setResult(RESULT_OK, intent);
                finish();
            }
        });

        btnRecipeFilterReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = getIntent();
//                intent.putExtra("FilterStatus", "Reset");
//
//                setResult(RESULT_OK, intent);
//                finish();

                BaseActivity.Time = "";
                BaseActivity.Difficulty = "";
                BaseActivity.CuisineID = "";
                BaseActivity.mIngredientsSelectedList.clear();
            }
        });

        btnRecipeFilterApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = getIntent();
//                intent.putExtra("Time", BaseActivity.Time);
//                intent.putExtra("Difficulty", BaseActivity.Difficulty);
//                intent.putExtra("CuisineID", BaseActivity.CuisineID);
                intent.putExtra("FilterStatus", "Apply");

                setResult(RESULT_OK, intent);
                finish();
            }
        });

        llPrepTime15min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrepTimeViewSetup("15");

                BaseActivity.Time = "15";
            }
        });

        llPrepTime30min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrepTimeViewSetup("30");

                BaseActivity.Time = "30";

            }
        });

        llPrepTime60min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PrepTimeViewSetup("60");

                BaseActivity.Time = "60";
            }
        });

        llDifficultyEasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                BaseActivity.Difficulty = "easy";
                DifficultyViewSetup(BaseActivity.Difficulty);


            }
        });

        llDifficultyMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.Difficulty = "medium";
                DifficultyViewSetup(BaseActivity.Difficulty);

            }
        });

        llDifficultyHard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.Difficulty = "hard";
                DifficultyViewSetup(BaseActivity.Difficulty);
            }
        });

        txtDifficultyEasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.Difficulty = "easy";
                DifficultyViewSetup(BaseActivity.Difficulty);

            }
        });

        txtDifficultyMedium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BaseActivity.Difficulty = "medium";
                DifficultyViewSetup(BaseActivity.Difficulty);

            }
        });

        txtDifficultyHard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.Difficulty = "hard";
                DifficultyViewSetup(BaseActivity.Difficulty);
            }
        });

        layCuisine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(mContext, CuisinesActivity.class));
                Intent intent_cuisines = new Intent(getApplicationContext(), CuisinesActivity.class);
                startActivityForResult(intent_cuisines, CUISINE_REQUEST_CODE);
            }
        });

        layIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_cuisines = new Intent(getApplicationContext(), IngredientsActivity.class);
                startActivityForResult(intent_cuisines, INGREDIENS_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == CUISINE_REQUEST_CODE && resultCode == RESULT_OK) {

                BaseActivity.CuisineID = data.getStringExtra("CuisineID");
//                Toast.makeText(mContext, "Result :   "+CuisineID, Toast.LENGTH_SHORT).show();
            }

            if (requestCode == INGREDIENS_REQUEST_CODE && resultCode == RESULT_OK) {

//                ArrayList<Ingredients> IngredientsList = (ArrayList<Ingredients>) getIntent().getSerializableExtra("IngredientsList");

                for(int i =0; i< BaseActivity.mIngredientsSelectedList.size();i++){

                    Log.e("IngredientsList", ""+BaseActivity.mIngredientsSelectedList.get(i));


                }

//                android.text.TextUtils.join(",", mIngredientsSelectedList);
//                String resvult = StringUtils.join(mIngredientsSelectedList, ',');
//                String result = TextUtils.join(",", mIngredientsSelectedList);
                Log.e("Ingredients String", ""+BaseActivity.mIngredientsSelectedList.toString());

                /*ArrayList to Array Conversion */
//                String array[] = new String[mIngredientsSelectedList.size()];
//                for(int j =0;j<mIngredientsSelectedList.size();j++){
//                    array[j] = mIngredientsSelectedList.get(j).getIngredientsName();
//                }
            }

        } catch (Exception ex) {
            Log.e("Recipefilter", "" + ex.toString());
        }

    }

    private void PrepTimeViewSetup(String PrepTime){

        if(PrepTime.equalsIgnoreCase("")){

            llPrepTime15min.setBackgroundResource(0);
            llPrepTime30min.setBackgroundResource(0);
            llPrepTime60min.setBackgroundResource(0);

            imgPrepTime15min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime30min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime60min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);

            txtPrepTime15min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            txtPrepTime30min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            txtPrepTime60min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));

        }else if(PrepTime.equalsIgnoreCase("15")){

            llPrepTime15min.setBackgroundResource(R.drawable.button_selector);
            llPrepTime30min.setBackgroundResource(0);
            llPrepTime60min.setBackgroundResource(0);

            imgPrepTime15min.setColorFilter(mContext.getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime30min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime60min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);

            txtPrepTime15min.setTextColor(mContext.getResources().getColor(R.color.white));
            txtPrepTime30min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            txtPrepTime60min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));

        }else if(PrepTime.equalsIgnoreCase("30")){

            llPrepTime30min.setBackgroundResource(R.drawable.button_selector);
            llPrepTime15min.setBackgroundResource(0);
            llPrepTime60min.setBackgroundResource(0);

            imgPrepTime30min.setColorFilter(mContext.getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime15min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime60min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);

            txtPrepTime30min.setTextColor(mContext.getResources().getColor(R.color.white));
            txtPrepTime15min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            txtPrepTime60min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));

        }else if(PrepTime.equalsIgnoreCase("60")){

            llPrepTime60min.setBackgroundResource(R.drawable.button_selector);
            llPrepTime15min.setBackgroundResource(0);
            llPrepTime30min.setBackgroundResource(0);

            imgPrepTime60min.setColorFilter(mContext.getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime30min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);
            imgPrepTime15min.setColorFilter(mContext.getResources().getColor(R.color.SealBrown), android.graphics.PorterDuff.Mode.SRC_IN);

            txtPrepTime60min.setTextColor(mContext.getResources().getColor(R.color.white));
            txtPrepTime30min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            txtPrepTime15min.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
        }
    }

    private void DifficultyViewSetup(String difficulty){

        if(difficulty.equalsIgnoreCase("")){

            llDifficultyEasy.setBackgroundResource(0);
            llDifficultyMedium.setBackgroundResource(0);
            llDifficultyHard.setBackgroundResource(0);

            txtDifficultyEasy.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            txtDifficultyMedium.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            txtDifficultyHard.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));

        }else if(difficulty.equalsIgnoreCase("easy")){

            llDifficultyEasy.setBackgroundResource(R.drawable.button_selector);
            llDifficultyMedium.setBackgroundResource(0);
            llDifficultyHard.setBackgroundResource(0);

            txtDifficultyEasy.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            txtDifficultyMedium.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            txtDifficultyHard.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));

        }else if(difficulty.equalsIgnoreCase("medium")){

            llDifficultyMedium.setBackgroundResource(R.drawable.button_selector);
            llDifficultyEasy.setBackgroundResource(0);
            llDifficultyHard.setBackgroundResource(0);

            txtDifficultyMedium.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            txtDifficultyEasy.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            txtDifficultyHard.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));

        }else if(difficulty.equalsIgnoreCase("hard")){

            llDifficultyHard.setBackgroundResource(R.drawable.button_selector);
            llDifficultyEasy.setBackgroundResource(0);
            llDifficultyMedium.setBackgroundResource(0);

            txtDifficultyHard.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            txtDifficultyMedium.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            txtDifficultyEasy.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
        }
    }
}

package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.InternetStatus;
import com.codefidea.hpp.customviews.AlphabetItem;
import com.codefidea.hpp.customviews.AlphabetRecyclerViewAdapter;
import com.codefidea.hpp.customviews.DataHelper;
import com.codefidea.hpp.customviews.RVIndex;
import com.codefidea.hpp.model.Cuisines;
import com.codefidea.hpp.model.Staff;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Raavan on 08-Mar-18.
 */

public class StaffListActivity extends BaseActivity {

    StaffListActivity mContext;

    LinearLayout lay_parent_stafflist, ll_stafflist_back;

    RVIndex mAlphabet_Recycler;

//    private List<String> mDataArray;
    private ArrayList<Staff> mStaffsList = new ArrayList<>();
    String [] Alphabes = new String [] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    private boolean isInternet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stafflist);

        mContext = this;

        setView();
        setClickListener();
        overrideFonts(mContext , lay_parent_stafflist);

        isInternet = new InternetStatus().isInternetOn(mContext);

        if (BaseActivity.mStaffsList.size() > 0) {
            mStaffsList = BaseActivity.mStaffsList;
            InitAlphabetRecycler();
        } else {
            if (isInternet) {
                sendCuisineRequest();
            } else {
                Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setView() {
        lay_parent_stafflist = (LinearLayout)findViewById(R.id.lay_parent_physicaldata);
        ll_stafflist_back = (LinearLayout)findViewById(R.id.ll_stafflist_back);
        mAlphabet_Recycler = (RVIndex)findViewById(R.id.mAlphabet_Recycler);
    }

    private void setClickListener()
    {
        ll_stafflist_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAlphabet_Recycler.addOnItemTouchListener(new BaseActivity.RecyclerTouchListener(mContext, mAlphabet_Recycler, new BaseActivity.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent it = new Intent(mContext, ChatActivity.class);
                it.putExtra("fromid", mStaffsList.get(position).getId());
                it.putExtra("fromname", mStaffsList.get(position).getFirstName());
                mContext.startActivity(it);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    protected void initialiseData() {
        //Recycler view data
//        mDataArray = DataHelper.getAlphabetData();

        //Alphabet fast scroller data
//        mStaffsList = new ArrayList<>();

//        List<String> strAlphabets = new ArrayList<String>(Arrays.asList(Alphabes));

//        List<String> strAlphabets = new ArrayList<>();
//        for (int i = 0; i < mStaffsList.size(); i++) {
//            String name = mStaffsList.get(i).getFirstName();
//            if (name == null || name.trim().isEmpty())
//                continue;
//
//            String word = name.substring(0, 1);
//            if (!strAlphabets.contains(word)) {
//                strAlphabets.add(word);
////                mStaffsList.add(new AlphabetItem(i, word, false));
//            }
//        }
    }

    protected void InitAlphabetRecycler() {
//        initialiseData();
        mAlphabet_Recycler.setLayoutManager(new LinearLayoutManager(this));
        mAlphabet_Recycler.setAdapter(new AlphabetRecyclerViewAdapter(mContext, mStaffsList));

        mAlphabet_Recycler.setIndexTextSize(12);
        mAlphabet_Recycler.setIndexBarColor("#33334c");
        mAlphabet_Recycler.setIndexBarCornerRadius(0);
        mAlphabet_Recycler.setIndexBarTransparentValue((float) 0.4);
        mAlphabet_Recycler.setIndexbarMargin(0);
        mAlphabet_Recycler.setIndexbarWidth(40);
        mAlphabet_Recycler.setPreviewPadding(0);
        mAlphabet_Recycler.setIndexBarTextColor("#FFFFFF");

        mAlphabet_Recycler.setIndexBarVisibility(true);
        mAlphabet_Recycler.setIndexbarHighLateTextColor("#33334c");
        mAlphabet_Recycler.setIndexBarHighLateTextVisibility(true);
    }

    private void sendCuisineRequest() {
        mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/cuisines
//        http://api.foodporntheory.com/messages/staff

        String StaffURL = Constant.API_BASE_URL + "/messages/staff";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, StaffURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("STAFF_API ", response.toString());
                getStaffData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getStaffData(String response) {
        JSONArray StaffList = new JSONArray();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    StaffList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < StaffList.length(); i++) {
                        Staff mStaff = new Staff();

                        mStaff.setId(StaffList.getJSONObject(i).getString(Constant.id));
                        mStaff.setFirstName(StaffList.getJSONObject(i).getString(Constant.firstName));
                        mStaff.setLastName(StaffList.getJSONObject(i).getString(Constant.lastName));
                        mStaff.setProfilePic(StaffList.getJSONObject(i).getString(Constant.profilePic));

                        mStaffsList.add(mStaff);
                    }

                    BaseActivity.mStaffsList = mStaffsList;
                    InitAlphabetRecycler();
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
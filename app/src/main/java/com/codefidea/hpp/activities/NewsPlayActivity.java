package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codefidea.hpp.R;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.fragments.PlayNewsViewPagerItemFragment;
import com.codefidea.hpp.ui.ViewPagerArrowIndicator;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class NewsPlayActivity extends BaseActivity
{
    Context mContext;


    RelativeLayout layout_play_activity;

    private ViewPager viewPager_news_play;

    LinearLayout layoutDots_play;
    LinearLayout ll_back_news_play, ll_news_left, ll_news_right;

    private TextView[] dots;
    private int no_of_sample_tutorial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news_paly);

        mContext = this;

        no_of_sample_tutorial = BaseActivity.mVideoUrls.size();

        setView();
        setClickListener();
        overrideFonts(mContext , layout_play_activity);
        addBottomDots(0);
    }

    private void setView() {

        ll_back_news_play = (LinearLayout) findViewById(R.id.ll_back_news_play);
        ll_news_left = (LinearLayout) findViewById(R.id.ll_news_left);
        ll_news_right = (LinearLayout) findViewById(R.id.ll_news_right);

        layoutDots_play = (LinearLayout) findViewById(R.id.layoutDots_play);

        viewPager_news_play = (ViewPager) findViewById(R.id.viewPager_news_play);
        viewPager_news_play.addOnPageChangeListener(viewPagerPageChangeListener);

        viewPager_news_play.setAdapter(new NewsPagerAdapter(getSupportFragmentManager()));

    }

    private void setClickListener() {
        ll_back_news_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_news_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(+1);
                if (current <= no_of_sample_tutorial -1) {
                    viewPager_news_play.setCurrentItem(current);
                    changeview(viewPager_news_play.getCurrentItem());
                }
            }
        });

        ll_news_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = getItem(-1);
                if (current >= 0) {
                    viewPager_news_play.setCurrentItem(current);
                    changeview(viewPager_news_play.getCurrentItem());
                }
            }
        });
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            changeview(viewPager_news_play.getCurrentItem());
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    private void addBottomDots(int currentPage) {
        dots = new TextView[no_of_sample_tutorial];

        layoutDots_play.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_dark));
            layoutDots_play.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(getResources().getColor(R.color.dot_light));
    }

    private class NewsPagerAdapter extends FragmentPagerAdapter {
        public NewsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            return PlayNewsViewPagerItemFragment.newInstance(BaseActivity.mVideoUrls.get(pos).getVideoUrl());
        }

        @Override
        public int getCount() {
            return no_of_sample_tutorial;
        }
    }

    private int getItem(int i) {
        return viewPager_news_play.getCurrentItem() + i;
    }

    public void changeview(int pager_no){

        if(pager_no == 0){

            ll_news_left.setVisibility(View.GONE);
            ll_news_right.setVisibility(View.VISIBLE);
        }else if(pager_no == no_of_sample_tutorial-1){

            ll_news_left.setVisibility(View.VISIBLE);
            ll_news_right.setVisibility(View.GONE);
        }else{
            ll_news_left.setVisibility(View.VISIBLE);
            ll_news_right.setVisibility(View.VISIBLE);
        }
    }
}

package com.codefidea.hpp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.util.RestClient;
import com.codefidea.hpp.model.NewsData;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class NewsSearchActivity extends BaseActivity
{
    NewsSearchActivity mContext;

    RelativeLayout layout_search_main;

    ImageView ImageView;

    LinearLayout ll_nodata;

    RecyclerView searchResults;

    SearchView search;

    String found = "N";

    NewsSearchAdapter adapter;

    myAsyncTask mTask;

    boolean isInternet;

    ArrayList<NewsData> NewsSearchResults = new ArrayList<NewsData>();

    String searchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_news_search);

        mContext = this;

        setView();
        overrideFonts(mContext,layout_search_main);
        setClickListener();
    }

    private void setView() {
        layout_search_main = (RelativeLayout) findViewById(R.id.layout_search_main);

        ImageView = (ImageView) findViewById(R.id.btn_back);

        ll_nodata = (LinearLayout) findViewById(R.id.ll_nodata);

        search = (SearchView) findViewById(R.id.searchView);
        search.setQueryHint("Search here");
        search.setIconified(false);

        searchResults = (RecyclerView) findViewById(R.id.rcv_news_search);
        searchResults.setLayoutManager(new LinearLayoutManager(mContext));
        searchResults.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
    }

    /*private void InitNewsRecyler() {
        adapter = new NewsSearchAdapter(mContext, NewsSearchResults);
        searchResults.setAdapter(adapter);
        searchResults.setItemAnimator(new DefaultItemAnimator());
        adapter.notifyDataSetChanged();
    }*/

    private void setClickListener()
    {
        ImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        search.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK)
                {
                    search.clearFocus();
                    finish();
                }
                return true;
            }
        });

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // TODO Auto-generated method stub

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (newText.equalsIgnoreCase("")) {

                    if (mTask != null) {

                        mTask.cancel(true);
                    }
                    hideProgressBar(search);
                    NewsSearchResults.clear();
                    adapter = null;
                    searchResults.setAdapter(adapter);
                    ll_nodata.setVisibility(View.VISIBLE);
                    searchResults.setVisibility(View.GONE);

                    Log.d("EMPTY TEXT", "_");

                } else if (newText.length() > 2)
                {
                    searchText = newText;

                    if (mTask != null)
                    {
                        mTask.cancel(true);
                        mTask = (myAsyncTask) new myAsyncTask().execute(newText);
                    } else
                    {
                        mTask = (myAsyncTask) new myAsyncTask().execute(newText);
                    }
                }
                return false;
            }
        });
    }

    public void showProgressBar(SearchView searchView, Context context)
    {
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        if (searchView.findViewById(id).findViewById(R.id.search_progress_bar) != null) {
            searchView.findViewById(id).findViewById(R.id.search_progress_bar).animate().setDuration(200).alpha(1).start();
        }
        else
        {
            View v = LayoutInflater.from(context).inflate(R.layout.other, null);
            ((ViewGroup) searchView.findViewById(id)).addView(v, 1);
        }
    }

    public void hideProgressBar(SearchView searchView)
    {
        int id = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
        if (searchView.findViewById(id).findViewById(R.id.search_progress_bar) != null)
            searchView.findViewById(id).findViewById(R.id.search_progress_bar)
                    .animate().setDuration(200).alpha(0).start();
    }

    class myAsyncTask extends AsyncTask<String, Void, String>
    {
        RestClient jParser;
        JSONArray newsList;
        String url = new String();
        String textSearch;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            newsList = new JSONArray();
            jParser = new RestClient();

            showProgressBar(search, mContext);
        }

        @Override
        protected String doInBackground(String... sText)
        {
            Log.d("SEARCH TEXT HERE .... ", sText[0] + " ");

            url = "http://api.foodporntheory.com/news/"+ userID+"/search";
            String word = sText[0];
            String returnResult = getProductList(url,word);
            this.textSearch = sText[0];

            return returnResult;
        }

        public String getProductList(String url,String word) {

            String matchFound = "N";

            try
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("query",""+word);

                JSONObject jString = jParser.makeHttpRequest(url, "POST", params);

                JSONObject json = jString;

                boolean response = json.getBoolean("response");

                String STR =String.valueOf(response);

                if(STR.equalsIgnoreCase("true")){

                    newsList = json.getJSONArray("responseData");
                    Log.d("RESULT",""+newsList);

                    NewsSearchResults = new ArrayList<NewsData>();

                    // parse date for dateList
                    for (int i = 0; i < newsList.length(); i++)
                    {
                        NewsData data = new NewsData();

                        data.setId(newsList.getJSONObject(i).getString(Constant.id));
                        data.setTitle(newsList.getJSONObject(i).getString(Constant.title));
                        data.setImage(newsList.getJSONObject(i).getString(Constant.image));
                        data.setBody(newsList.getJSONObject(i).getString(Constant.body));
                        data.setUserId(newsList.getJSONObject(i).getString(Constant.userId));
                        data.setCreatedAt(newsList.getJSONObject(i).getString(Constant.createdAt));
                        data.setUpdatedAt(newsList.getJSONObject(i).getString(Constant.updatedAt));
                        data.setUserFirstName(newsList.getJSONObject(i).getString(Constant.userFirstName));
                        data.setUserLastName(newsList.getJSONObject(i).getString(Constant.userLastName));
                        data.setUserProfilePic(newsList.getJSONObject(i).getString(Constant.userProfilePic));
                        data.setTotalLikes(newsList.getJSONObject(i).getString(Constant.totalLikes));
                        data.setIsLiked(newsList.getJSONObject(i).getString(Constant.isLiked));

                        NewsSearchResults.add(data);
                    }
                }else {

                }

                return STR;

            } catch (Exception e)
            {
                e.printStackTrace();
                return ("Exception Caught");
            }
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try
            {
                if(result != null)
                {
                    if (result.equalsIgnoreCase("Exception Caught"))
                    {
                        Log.d("Exception Caught","Unable to connect to server,please try later");

                        if (mTask != null) {

                            mTask.cancel(true);
                        }
                        hideProgressBar(search);
                        NewsSearchResults.clear();
                        adapter = null;
                        searchResults.setAdapter(adapter);
                        ll_nodata.setVisibility(View.VISIBLE);
                        searchResults.setVisibility(View.GONE);
                    }
                    else if (result.equalsIgnoreCase("false"))
                    {

                        if (mTask != null) {

                            mTask.cancel(true);
                        }
                        hideProgressBar(search);
                        NewsSearchResults.clear();
                        adapter = null;
                        searchResults.setAdapter(adapter);
                        ll_nodata.setVisibility(View.VISIBLE);
                        searchResults.setVisibility(View.GONE);
                    }
                    else
                    {
                        ll_nodata.setVisibility(View.GONE);
                        searchResults.setVisibility(View.VISIBLE);

                        hideProgressBar(search);

                        adapter = new NewsSearchAdapter(mContext,NewsSearchResults);
                        searchResults.setAdapter(adapter);
                        searchResults.setItemAnimator(new DefaultItemAnimator());
                        adapter.notifyDataSetChanged();

                        searchResults.setAdapter(adapter);
                    }
                }
                else
                {
                    hideProgressBar(search);
                    NewsSearchResults.clear();
                    adapter = null;
                    searchResults.setAdapter(adapter);
                    ll_nodata.setVisibility(View.VISIBLE);
                    searchResults.setVisibility(View.GONE);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public class NewsSearchAdapter extends RecyclerView.Adapter<NewsSearchAdapter.SearchNewsHolder>
    {
        private ArrayList<NewsData> mNewsList;

        NewsSearchActivity mContext;

        ImageLoader imageLoader;

        public class SearchNewsHolder extends RecyclerView.ViewHolder
        {
            TextView txt_news_title,txt_new_sub_title;

            ImageView img_news, image_news_thumb, img_news_fav;

            RelativeLayout lay_list_news;

            ProgressBar news_progress;

            public SearchNewsHolder(View view) {
                super(view);

                lay_list_news = (RelativeLayout) view.findViewById(R.id.lay_list_news);

                txt_news_title = (TextView) view.findViewById(R.id.txt_news_title);
                txt_new_sub_title = (TextView) view.findViewById(R.id.txt_new_sub_title);

                img_news = (ImageView) view.findViewById(R.id.img_news);
                image_news_thumb = (ImageView) view.findViewById(R.id.image_news_thumb);
                img_news_fav = (ImageView) view.findViewById(R.id.img_news_fav);

                news_progress = (ProgressBar) view.findViewById(R.id.news_progress);
            }
        }

        public NewsSearchAdapter(NewsSearchActivity mContext, ArrayList<NewsData> mNewsList) {
            this.mNewsList = mNewsList;
            this.mContext = mContext;

            imageLoader = AppController.getInstance().getImageLoader();
        }

        @Override
        public NewsSearchAdapter.SearchNewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View itemView = inflater.inflate(R.layout.news_row_layout_list, parent, false);
            NewsSearchAdapter.SearchNewsHolder newsVh = new NewsSearchAdapter.SearchNewsHolder(itemView);
            return newsVh;
        }

        @Override
        public void onBindViewHolder(final SearchNewsHolder holder, final int position) {
            final NewsData mNews = mNewsList.get(position);

            holder.txt_news_title.setText(mNews.getTitle());
            holder.txt_new_sub_title.setText(mNews.getCreatedAt());

            mContext.overrideFonts(mContext,holder.lay_list_news);

            if(mNews.getIsLiked().equalsIgnoreCase("1"))
            {
                holder.img_news_fav.setImageResource(R.drawable.ic_fav_selected);
            }
            else
            {
                holder.img_news_fav.setImageResource(R.drawable.ic_fav_unselect);
            }

            imageLoader.get(mNews.getImage(), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Image Load Error: " + error.getMessage());
                    holder.news_progress.setVisibility(View.GONE);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        holder.img_news.setImageBitmap(response.getBitmap());
                        holder.news_progress.setVisibility(View.GONE);
                    }
                }
            });

            imageLoader.get(mNews.getUserProfilePic(), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Image Load Error: " + error.getMessage());
//                    holder.news_progress.setVisibility(View.GONE);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        holder.image_news_thumb.setImageBitmap(response.getBitmap());
//                        holder.news_progress.setVisibility(View.GONE);
                    }
                }
            });

            holder.lay_list_news.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.newsID = mNewsList.get(position).getId();
                    BaseActivity.userID = mNewsList.get(position).getUserId();

                    Intent it = new Intent(mContext, NewsDetailActivity.class);
                    mContext.startActivity(it);
                    finish();
                }
            });

            holder.img_news_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BaseActivity.newsID = mNewsList.get(position).getId();
                    BaseActivity.userID = mNewsList.get(position).getUserId();

//                    Toast.makeText(mContext,"POS : "+position+" FAVE : "+mNewsList.get(position).getIsLiked(),Toast.LENGTH_LONG).show();

                    if(mNewsList.get(position).getIsLiked().equalsIgnoreCase("1"))
                    {
                        //send unfavorite request
                        setUnFavoriteNews();
                    }
                    else
                    {
                        //send favorite request
                        sendFavoriteNews();
                    }
                }
            });
        }

        private void sendFavoriteNews() {
            mContext.showWaitIndicator(true);

            String newsFavURL = Constant.API_BASE_URL+"/news/like";

            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("News_Favorite_API ", response.toString());
                    getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("newsId", BaseActivity.newsID);
                    params.put("userId", BaseActivity.userID);

                    return params;
                }
            };
            queue.add(sr);

            if (mTask != null)
            {
                mTask.cancel(true);
                mTask = (myAsyncTask) new myAsyncTask().execute(searchText);
            } else
            {
                mTask = (myAsyncTask) new myAsyncTask().execute(searchText);
            }
        }

        private void getFavoriteData(String s) {
            try {
                JSONObject jObject = new JSONObject(s.toString());

                if (jObject.toString() != null) {
                    boolean res = jObject.getBoolean("response");

                    if (res) {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                    }
                    else {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        private void setUnFavoriteNews()
        {
            mContext.showWaitIndicator(true);

            String newsFavURL = Constant.API_BASE_URL+"/news/unlike";

            RequestQueue queue = Volley.newRequestQueue(mContext);
            StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,newsFavURL, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("News_Favorite_API ", response.toString());
                    getFavoriteData(response.toString());
                    mContext.showWaitIndicator(false);
                }

            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mContext.showWaitIndicator(false);
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("newsId", BaseActivity.newsID);
                    params.put("userId", BaseActivity.userID);

                    return params;
                }
            };
            queue.add(sr);

            if (mTask != null)
            {
                mTask.cancel(true);
                mTask = (myAsyncTask) new myAsyncTask().execute(searchText);
            } else
            {
                mTask = (myAsyncTask) new myAsyncTask().execute(searchText);
            }
        }

        @Override
        public int getItemCount() {
            return mNewsList.size();
        }
    }
}

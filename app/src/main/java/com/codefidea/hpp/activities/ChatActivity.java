package com.codefidea.hpp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.codefidea.hpp.R;
import com.codefidea.hpp.adapters.MessageListAdapter;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.common.Constant;
import com.codefidea.hpp.model.ChatMessages;
import com.codefidea.hpp.util.InternetStatus;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class ChatActivity extends BaseActivity {

    ChatActivity mContext;

    RecyclerView rcv_chat;
    LinearLayout ll_chat_back, ll_chat_option, lay_chat_emoji, lay_chat_send;

    TextView txt_chat_title;
    EditText edt_chat_messages;

    MessageListAdapter mMessageListAdapter;

    private ArrayList<ChatMessages> mChatMessagesList = new ArrayList<>();

    boolean isInternet;

    private String MsgStr, FromID;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mContext = this;

        isInternet = new InternetStatus().isInternetOn(mContext);

        InitViews();
        setClickListener();

        Log.e("ChatMessagesActivity", ""+getIntent().getExtras().getString("fromid"));

        FromID = getIntent().getExtras().getString("fromid");
        txt_chat_title.setText(getIntent().getExtras().getString("fromname"));

//        if (BaseActivity.mChatMessagesList.size() > 0) {
//            mChatMessagesList = BaseActivity.mChatMessagesList;
//            InitChatRecyler();
//        } else {
//
//        }

        if (isInternet) {
            GetChatMessagesRequest();
        } else {
            Toast.makeText(mContext, "No internet connection found", Toast.LENGTH_SHORT).show();
        }
    }

    private void InitViews() {
        rcv_chat = (RecyclerView) findViewById(R.id.rcv_chat);
        ll_chat_back = (LinearLayout) findViewById(R.id.ll_chat_back);
        ll_chat_option = (LinearLayout) findViewById(R.id.ll_chat_option);

        txt_chat_title = (TextView) findViewById(R.id.txt_chat_title);
        edt_chat_messages = (EditText) findViewById(R.id.edt_chat_messages);

        lay_chat_emoji = (LinearLayout) findViewById(R.id.lay_chat_emoji);
        lay_chat_send = (LinearLayout) findViewById(R.id.lay_chat_send);

    }

    private void setClickListener() {

        ll_chat_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ll_chat_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        rcv_chat.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), rcv_chat, new ClickListener() {
            @Override
            public void onClick(View view, int position) {

//                mChatMessagesList.get(position).getId();
//                Toast.makeText(mContext, ""+mChatMessagesList.get(position).getId(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        lay_chat_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MsgStr = edt_chat_messages.getText().toString().trim();

                if(MsgStr != null && !MsgStr.isEmpty()) {

//                     && !MsgStr.matches(".*[a-z].*")
                    /* do your stuffs here */
                  /*  if(MsgStr.trim().length() > 0){

                    }*/
                    sendChatMessage();
                }
            }
        });

        ll_chat_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteChatMessage();
            }
        });
    }


    private void InitChatRecyler() {

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
//        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        rcv_chat.setLayoutManager(mLayoutManager);
//        rcv_cuisine.addItemDecoration(new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));


        mMessageListAdapter = new MessageListAdapter(mContext, mChatMessagesList);
        rcv_chat.setAdapter(mMessageListAdapter);
        rcv_chat.setItemAnimator(new DefaultItemAnimator());
        mMessageListAdapter.notifyDataSetChanged();
    }

    private void GetChatMessagesRequest() {
        mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/messages/messages/1/4/
        String chatURL = Constant.API_BASE_URL + "/messages/messages/"+BaseActivity.userID+"/"+FromID;

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(Request.Method.GET, chatURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ChatMessages_API ", response.toString());
                getChatData(response.toString());
                mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                params.put("query", "");

                return params;
            }

            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }*/
        };
        queue.add(sr);
    }

    private void getChatData(String response) {
        JSONArray ChatMessagesList = new JSONArray();
        mChatMessagesList.clear();

        try {
            JSONObject jObject = new JSONObject(response.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
                    ChatMessagesList = jObject.getJSONArray("responseData");

                    for (int i = 0; i < ChatMessagesList.length(); i++) {

                        ChatMessages mChatMessages = new ChatMessages();

                        mChatMessages.setId(ChatMessagesList.getJSONObject(i).getString(Constant.id));
                        mChatMessages.setFromUserId(ChatMessagesList.getJSONObject(i).getString(Constant.fromUserId));
                        mChatMessages.setToUserId(ChatMessagesList.getJSONObject(i).getString(Constant.toUserId));
                        mChatMessages.setMessage(ChatMessagesList.getJSONObject(i).getString(Constant.message));
                        mChatMessages.setCreatedAt(ChatMessagesList.getJSONObject(i).getString(Constant.createdAt));
                        mChatMessages.setFromFirstName(ChatMessagesList.getJSONObject(i).getString(Constant.fromFirstName));
                        mChatMessages.setFromLastname(ChatMessagesList.getJSONObject(i).getString(Constant.fromLastname));
                        mChatMessages.setFromProfilePic(ChatMessagesList.getJSONObject(i).getString(Constant.fromProfilePic));
                        mChatMessages.setToFirstName(ChatMessagesList.getJSONObject(i).getString(Constant.toFirstName));
                        mChatMessages.setToLastname(ChatMessagesList.getJSONObject(i).getString(Constant.toLastname));
                        mChatMessages.setToProfilePic(ChatMessagesList.getJSONObject(i).getString(Constant.toProfilePic));

                        mChatMessagesList.add(mChatMessages);
                    }



                } else {

                }

                InitChatRecyler();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendChatMessage() {
//            mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/recipes/like
        String SendChatMessageURL = Constant.API_BASE_URL+"/messages/sendmessage";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,SendChatMessageURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Send_Message_API ", response.toString());
                getChatMessageData(response.toString());
//                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("fromUserId", FromID);
                params.put("toUserId", BaseActivity.userID);
                params.put("message", MsgStr);

                return params;
            }
        };
        queue.add(sr);


    }

    private void getChatMessageData(String s) {
        GetChatMessagesRequest();
        edt_chat_messages.setText("");
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
                else {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DeleteChatMessage() {
//            mContext.showWaitIndicator(true);
//        http://api.foodporntheory.com/messages/clearchat
        String SendChatMessageURL = Constant.API_BASE_URL+"/messages/clearchat";

        RequestQueue queue = Volley.newRequestQueue(mContext);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,SendChatMessageURL, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("DeleteChatMessage_API ", response.toString());
                DeleteChatMessageData(response.toString());
//                    mContext.showWaitIndicator(false);
            }

        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                    mContext.showWaitIndicator(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("fromUserId", FromID);
                params.put("toUserId", BaseActivity.userID);

                return params;
            }
        };
        queue.add(sr);


    }

    private void DeleteChatMessageData(String s) {
        GetChatMessagesRequest();
        try {
            JSONObject jObject = new JSONObject(s.toString());

            if (jObject.toString() != null) {
                boolean res = jObject.getBoolean("response");

                if (res) {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
                else {
//                        Toast.makeText(mContext,""+jObject.getString("message"),Toast.LENGTH_LONG).show();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }



}

package com.codefidea.hpp.model;

/**
 * Created by MyInnos on 01-02-2017.
 */

public class Staff {

    public String id;
    public String firstName;
    public String lastName;
    public String profilePic;


    public Staff() {
    }

    public Staff(String id, String firstName, String lastName, String profilePic) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.profilePic = profilePic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }


}
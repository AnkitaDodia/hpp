package com.codefidea.hpp.model;

import java.io.Serializable;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class RecipePrepStep implements Serializable
{

    private String StepID;
    private String StepNumb;
    private String StepDiscription;


    public String getStepID() {
        return StepID;
    }

    public void setStepID(String stepID) {
        StepID = stepID;
    }

    public String getStepNumb() {
        return StepNumb;
    }

    public void setStepNumb(String stepNumb) {
        StepNumb = stepNumb;
    }

    public String getStepDiscription() {
        return StepDiscription;
    }

    public void setStepDiscription(String stepdiscription) {
        StepDiscription = stepdiscription;
    }


}

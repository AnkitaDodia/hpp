package com.codefidea.hpp.model;

/**
 * Created by Raavan on 15-Mar-18.
 */

public class PhysicalInfo {

    String units;
    String weight;
    String height;
    String navalCircumference;
    String waistCircumference;
    String thighCircumference;
    String calfCircumference;
    String armCircumference;
    String chestCircumference;

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getNavalCircumference() {
        return navalCircumference;
    }

    public void setNavalCircumference(String navalCircumference) {
        this.navalCircumference = navalCircumference;
    }

    public String getWaistCircumference() {
        return waistCircumference;
    }

    public void setWaistCircumference(String waistCircumference) {
        this.waistCircumference = waistCircumference;
    }

    public String getThighCircumference() {
        return thighCircumference;
    }

    public void setThighCircumference(String thighCircumference) {
        this.thighCircumference = thighCircumference;
    }

    public String getCalfCircumference() {
        return calfCircumference;
    }

    public void setCalfCircumference(String calfCircumference) {
        this.calfCircumference = calfCircumference;
    }

    public String getArmCircumference() {
        return armCircumference;
    }

    public void setArmCircumference(String armCircumference) {
        this.armCircumference = armCircumference;
    }

    public String getChestCircumference() {
        return chestCircumference;
    }

    public void setChestCircumference(String chestCircumference) {
        this.chestCircumference = chestCircumference;
    }




}

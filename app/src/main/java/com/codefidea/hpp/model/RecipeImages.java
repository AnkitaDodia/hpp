package com.codefidea.hpp.model;

import java.io.Serializable;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class RecipeImages implements Serializable {


    private String RecipeImagesID;
    private String RecipeImagesUrl;
    private String RecipeImagesShortOrder;


    public String getRecipeImagesID() {
        return RecipeImagesID;
    }

    public void setRecipeImagesID(String recipeImagesID) {
        RecipeImagesID = recipeImagesID;
    }

    public String getRecipeImagesUrl() {
        return RecipeImagesUrl;
    }

    public void setRecipeImagesUrl(String recipeImagesUrl) {
        RecipeImagesUrl = recipeImagesUrl;
    }

    public String getRecipeImagesShortOrder() {
        return RecipeImagesShortOrder;
    }

    public void setRecipeImagesShortOrder(String recipeImagesShortOrder) {
        RecipeImagesShortOrder = recipeImagesShortOrder;
    }


}

package com.codefidea.hpp.model;

import java.io.Serializable;

/**
 * Created by My 7 on 22-Mar-18.
 */

public class UserData implements Serializable
{
    private String id;
    private String userType;
    private String planId;
    private String username;
    private String name;
    private String surname;
    private String phone;
    private String email;
    private String birthDate;
    private String gender;
    private String address;
    private String aboutMe;
    private String profilePic;
    private String countryId;
    private String cityId;
    private String fiscalCode;
    private String morpheTypeId;
    private String lifestyleTypeId;
    private String practiceSportsFlag;
    private String sportsId;
    private String weekTimes;
    private String isActive;
    private String deviceId;
    private String points;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getMorpheTypeId() {
        return morpheTypeId;
    }

    public void setMorpheTypeId(String morpheTypeId) {
        this.morpheTypeId = morpheTypeId;
    }

    public String getLifestyleTypeId() {
        return lifestyleTypeId;
    }

    public void setLifestyleTypeId(String lifestyleTypeId) {
        this.lifestyleTypeId = lifestyleTypeId;
    }

    public String getPracticeSportsFlag() {
        return practiceSportsFlag;
    }

    public void setPracticeSportsFlag(String practiceSportsFlag) {
        this.practiceSportsFlag = practiceSportsFlag;
    }

    public String getSportsId() {
        return sportsId;
    }

    public void setSportsId(String sportsId) {
        this.sportsId = sportsId;
    }

    public String getWeekTimes() {
        return weekTimes;
    }

    public void setWeekTimes(String weekTimes) {
        this.weekTimes = weekTimes;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}

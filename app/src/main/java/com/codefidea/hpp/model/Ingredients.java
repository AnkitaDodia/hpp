package com.codefidea.hpp.model;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class Ingredients {

    String IngredientsID;
    String IngredientsName;

    public Ingredients() {
    }

    public Ingredients( String mIngredientsID, String mIngredientsName) {
        this.IngredientsID = mIngredientsID;
        this.IngredientsName = mIngredientsName;
    }


    public String getIngredientsID() {
        return IngredientsID;
    }

    public void setIngredientsID(String ingredientsID) {
        IngredientsID = ingredientsID;
    }

    public String getIngredientsName() {
        return IngredientsName;
    }

    public void setIngredientsName(String ingredientsName) {
        IngredientsName = ingredientsName;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;

        Ingredients itemCompare = (Ingredients) obj;
        if(itemCompare.getIngredientsName().equals(this.getIngredientsName()))
            return true;

        return false;
    }

}

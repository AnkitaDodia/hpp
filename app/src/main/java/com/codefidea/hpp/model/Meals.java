package com.codefidea.hpp.model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.io.Serializable;
import java.util.ArrayList;

public class Meals implements Serializable {

    private String optionId;
    private String optionType;
    private String quantity;
    private String quantityType;
    private String foodName;
    private String recipeName;
    private String recipeId;
    private ArrayList<MealsItem> ingredients = new ArrayList<>();

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantityType() {
        return quantityType;
    }

    public void setQuantityType(String quantityType) {
        this.quantityType = quantityType;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public ArrayList<MealsItem> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<MealsItem> ingredients) {
        this.ingredients = ingredients;
    }

}

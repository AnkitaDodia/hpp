package com.codefidea.hpp.model;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class ChatMessages {
    String id;
    String fromUserId;
    String toUserId;
    String message;
    String createdAt;
    String fromFirstName;
    String fromLastname;
    String fromProfilePic;
    String toFirstName;
    String toLastname;
    String toProfilePic;


    public ChatMessages() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromFirstName() {
        return fromFirstName;
    }

    public void setFromFirstName(String fromFirstName) {
        this.fromFirstName = fromFirstName;
    }

    public String getFromLastname() {
        return fromLastname;
    }

    public void setFromLastname(String fromLastname) {
        this.fromLastname = fromLastname;
    }

    public String getFromProfilePic() {
        return fromProfilePic;
    }

    public void setFromProfilePic(String fromProfilePic) {
        this.fromProfilePic = fromProfilePic;
    }

    public String getToFirstName() {
        return toFirstName;
    }

    public void setToFirstName(String toFirstName) {
        this.toFirstName = toFirstName;
    }

    public String getToLastname() {
        return toLastname;
    }

    public void setToLastname(String toLastname) {
        this.toLastname = toLastname;
    }

    public String getToProfilePic() {
        return toProfilePic;
    }

    public void setToProfilePic(String toProfilePic) {
        this.toProfilePic = toProfilePic;
    }
}

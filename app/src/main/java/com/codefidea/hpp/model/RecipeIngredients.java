package com.codefidea.hpp.model;

import java.io.Serializable;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class RecipeIngredients implements Serializable
{
    private String IngredientsID;
    private String IngredientsName;

    public String getIngredientsID() {
        return IngredientsID;
    }

    public void setIngredientsID(String ingredientsID) {
        IngredientsID = ingredientsID;
    }

    public String getIngredientsName() {
        return IngredientsName;
    }

    public void setIngredientsName(String ingredientsName) {
        IngredientsName = ingredientsName;
    }
}

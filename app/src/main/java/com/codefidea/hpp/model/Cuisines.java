package com.codefidea.hpp.model;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class Cuisines
{

    String CuisinesID;
    String CuisinesName;

    public Cuisines() {
    }

    public Cuisines( String mCuisinesID, String mCuisinesName) {
        this.CuisinesID = mCuisinesID;
        this.CuisinesName = mCuisinesName;
    }

    public String getCuisinesID() {
        return CuisinesID;
    }

    public void setCuisinesID(String cuisinesID) {
        CuisinesID = cuisinesID;
    }

    public String getCuisinesName() {
        return CuisinesName;
    }

    public void setCuisinesName(String cuisinesName) {
        CuisinesName = cuisinesName;
    }




}

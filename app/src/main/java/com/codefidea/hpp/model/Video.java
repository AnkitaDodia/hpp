package com.codefidea.hpp.model;

import java.io.Serializable;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class Video implements Serializable
{
    private String id;
    private String videoUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}

package com.codefidea.hpp.model;

/**
 * Created by Raavan on 15-Mar-18.
 */

public class CeilingInfo {

    String units;
    String snatch;
    String powerSnatch;
    String backSquat;
    String frontSquat;
    String pushPress;
    String shoulderPress;
    String PowerClean;
    String deadLift;

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getSnatch() {
        return snatch;
    }

    public void setSnatch(String snatch) {
        this.snatch = snatch;
    }

    public String getPowerSnatch() {
        return powerSnatch;
    }

    public void setPowerSnatch(String powerSnatch) {
        this.powerSnatch = powerSnatch;
    }

    public String getBackSquat() {
        return backSquat;
    }

    public void setBackSquat(String backSquat) {
        this.backSquat = backSquat;
    }

    public String getFrontSquat() {
        return frontSquat;
    }

    public void setFrontSquat(String frontSquat) {
        this.frontSquat = frontSquat;
    }

    public String getPushPress() {
        return pushPress;
    }

    public void setPushPress(String pushPress) {
        this.pushPress = pushPress;
    }

    public String getShoulderPress() {
        return shoulderPress;
    }

    public void setShoulderPress(String shoulderPress) {
        this.shoulderPress = shoulderPress;
    }

    public String getPowerClean() {
        return PowerClean;
    }

    public void setPowerClean(String powerClean) {
        PowerClean = powerClean;
    }

    public String getDeadLift() {
        return deadLift;
    }

    public void setDeadLift(String deadLift) {
        this.deadLift = deadLift;
    }


}

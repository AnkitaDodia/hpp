package com.codefidea.hpp.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by My 7 on 13-Mar-18.
 */

public class RecipeDetails implements Serializable {


    private String RecipeID;
    private String RecipeName;
    private String RecipePrepTime;
    private String RecipeDifficulty;
    private String RecipeRatings;
    private String RecipeDiscription;
    private String RecipeCuisineName;
    private String RecipeIsLiked;
    private String RecipeTotalLike;
    private String RecipeDisplayImagePath;

    private ArrayList<RecipeIngredients> RecipeIngredientsList;
    private ArrayList<RecipePrepStep> RecipePrepStepList ;
    private ArrayList<Video> RecipeVideosList ;
    private ArrayList<RecipeImages> RecipeImagesList ;

    public String getRecipeID() {
        return RecipeID;
    }

    public void setRecipeID(String recipeID) {
        RecipeID = recipeID;
    }

    public String getRecipeName() {
        return RecipeName;
    }

    public void setRecipeName(String recipeName) {
        RecipeName = recipeName;
    }

    public String getRecipePrepTime() {
        return RecipePrepTime;
    }

    public void setRecipePrepTime(String recipePrepTime) {
        RecipePrepTime = recipePrepTime;
    }

    public String getRecipeDifficulty() {
        return RecipeDifficulty;
    }

    public void setRecipeDifficulty(String recipeDifficulty) {
        RecipeDifficulty = recipeDifficulty;
    }

    public String getRecipeRatings() {
        return RecipeRatings;
    }

    public void setRecipeRatings(String recipeRatings) {
        RecipeRatings = recipeRatings;
    }

    public String getRecipeDiscription() {
        return RecipeDiscription;
    }

    public void setRecipeDiscription(String recipeDiscription) {
        RecipeDiscription = recipeDiscription;
    }

    public String getRecipeCuisineName() {
        return RecipeCuisineName;
    }

    public void setRecipeCuisineName(String recipeCuisineName) {
        RecipeCuisineName = recipeCuisineName;
    }

    public String getRecipeIsLiked() {
        return RecipeIsLiked;
    }

    public void setRecipeIsLiked(String recipeIsLiked) {
        RecipeIsLiked = recipeIsLiked;
    }

    public String getRecipeTotalLike() {
        return RecipeTotalLike;
    }

    public void setRecipeTotalLike(String recipeTotalLike) {
        RecipeTotalLike = recipeTotalLike;
    }

    public String getRecipeDisplayImagePath() {
        return RecipeDisplayImagePath;
    }

    public void setRecipeDisplayImagePath(String recipeDisplayImagePath) {
        RecipeDisplayImagePath = recipeDisplayImagePath;
    }

    public ArrayList<RecipeIngredients> getRecipeIngredientsList() {
        return RecipeIngredientsList;
    }

    public void setRecipeIngredientsList(ArrayList<RecipeIngredients> recipeIngredientsList) {
        RecipeIngredientsList = recipeIngredientsList;
    }

    public ArrayList<RecipePrepStep> getRecipePrepStepList() {
        return RecipePrepStepList;
    }

    public void setRecipePrepStepList(ArrayList<RecipePrepStep> recipePrepStepList) {
        RecipePrepStepList = recipePrepStepList;
    }

    public ArrayList<Video> getRecipeVideosList() {
        return RecipeVideosList;
    }

    public void setRecipeVideosList(ArrayList<Video> recipeVideosList) {
        RecipeVideosList = recipeVideosList;
    }

    public ArrayList<RecipeImages> getRecipeImagesList() {
        return RecipeImagesList;
    }

    public void setRecipeImagesList(ArrayList<RecipeImages> recipeImagesList) {
        RecipeImagesList = recipeImagesList;
    }

}

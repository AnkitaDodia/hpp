package com.codefidea.hpp.model;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class AppSettings
{
    String showRelevantFirst;
    String optimizeImages;
    String notifications;
    String allowSaving;

    public String getShowRelevantFirst() {
        return showRelevantFirst;
    }

    public void setShowRelevantFirst(String showRelevantFirst) {
        this.showRelevantFirst = showRelevantFirst;
    }

    public String getOptimizeImages() {
        return optimizeImages;
    }

    public void setOptimizeImages(String optimizeImages) {
        this.optimizeImages = optimizeImages;
    }

    public String getNotifications() {
        return notifications;
    }

    public void setNotifications(String notifications) {
        this.notifications = notifications;
    }

    public String getAllowSaving() {
        return allowSaving;
    }

    public void setAllowSaving(String allowSaving) {
        this.allowSaving = allowSaving;
    }


}

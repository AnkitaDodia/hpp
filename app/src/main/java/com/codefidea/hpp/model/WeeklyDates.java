package com.codefidea.hpp.model;

/**
 * Created by Raavan on 24-Mar-18.
 */

public class WeeklyDates {

    String day;
    String date;
    boolean daystatus;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isDaystatus() {
        return daystatus;
    }

    public void setDaystatus(boolean daystatus) {
        this.daystatus = daystatus;
    }

}

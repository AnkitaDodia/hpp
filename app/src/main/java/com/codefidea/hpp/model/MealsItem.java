package com.codefidea.hpp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class MealsItem implements Serializable {

  private String id;
  private String ingredient;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIngredient() {
    return ingredient;
  }

  public void setIngredient(String ingredient) {
    this.ingredient = ingredient;
  }

}


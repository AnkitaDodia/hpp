package com.codefidea.hpp.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.model.Ingredients;
import com.codefidea.hpp.model.Meals;

import java.util.ArrayList;

/**
 * Created by Raavan on 24-Mar-18.
 */

public class DairyMealsAdapter extends RecyclerView.Adapter<DairyMealsAdapter.MyViewHolder> {

    MainActivity mContext;
    private ArrayList<Meals> mMealsDataItems = new ArrayList<>();

    public DairyMealsAdapter(MainActivity mContext, ArrayList<Meals> MealsDataItems) {
        this.mMealsDataItems = MealsDataItems;
        this.mContext = mContext;
    }

    @Override
    public DairyMealsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent_child_listing, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DairyMealsAdapter.MyViewHolder holder, int position) {
        Meals mMeals = mMealsDataItems.get(position);

        if(mMeals.getOptionType().equalsIgnoreCase("Recipe"))
        {
            holder.textView_parentName.setText(mMeals.getRecipeName());
        }
        else
        {
            holder.textView_parentName.setText(mMeals.getFoodName());
        }


        int noOfChildTextViews = holder.linearLayout_childItems.getChildCount();
        int noOfChild = mMeals.getIngredients().size();
        if (noOfChild < noOfChildTextViews) {
            for (int index = noOfChild; index < noOfChildTextViews; index++) {
                TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(index);
                currentTextView.setVisibility(View.GONE);
            }
        }
        for (int textViewIndex = 0; textViewIndex < noOfChild; textViewIndex++) {
            TextView currentTextView = (TextView) holder.linearLayout_childItems.getChildAt(textViewIndex);
            currentTextView.setText(mMeals.getIngredients().get(textViewIndex).getIngredient());
                /*currentTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext, "" + ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });*/
        }
    }

    @Override
    public int getItemCount() {
        return mMealsDataItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Context context;
        private TextView textView_parentName;
        private LinearLayout linearLayout_childItems;

        MyViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            textView_parentName = itemView.findViewById(R.id.tv_parentName);
            linearLayout_childItems = itemView.findViewById(R.id.ll_child_items);
            linearLayout_childItems.setVisibility(View.GONE);
            int intMaxNoOfChild = 0;

            for (int index = 0; index < mMealsDataItems.size(); index++) {
                int intMaxSizeTemp = mMealsDataItems.get(index).getIngredients().size();
                Log.e("intMaxNoOfChild", ""+intMaxSizeTemp);
                if (intMaxSizeTemp > intMaxNoOfChild) intMaxNoOfChild = intMaxSizeTemp;
            }

            for (int indexView = 0; indexView < intMaxNoOfChild; indexView++) {
                TextView textView = new TextView(context);
                textView.setId(indexView);
                textView.setPadding(0, 20, 0, 20);
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
                textView.setTextAppearance(context, android.R.style.TextAppearance_Medium);
                textView.setTypeface(mContext.getTypeFace());
                textView.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_sub_child_text));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 120);
                textView.setOnClickListener(this);
                linearLayout_childItems.addView(textView, layoutParams);
            }
            textView_parentName.setTypeface(mContext.getTypeFace());
            textView_parentName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.tv_parentName) {
                if (linearLayout_childItems.getVisibility() == View.VISIBLE) {
                    linearLayout_childItems.setVisibility(View.GONE);
                } else {
                    linearLayout_childItems.setVisibility(View.VISIBLE);
                }
            } else {
                TextView textViewClicked = (TextView) view;
                Toast.makeText(context, " item == " + textViewClicked.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.ChatActivity;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.ChatMessages;
import com.codefidea.hpp.model.RecetChats;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class ChatMessagesAdapter extends RecyclerView.Adapter<ChatMessagesAdapter.NewsHolder>
{
    private ArrayList<ChatMessages> mChatMessagesList = new ArrayList<>();
    ChatActivity mContext;

    ImageLoader imageLoader;

    public class NewsHolder extends RecyclerView.ViewHolder
    {
        public TextView txt_chatname,txt_chatdetails, txt_lastchattime;

        public ImageView img_chatperson;
        public LinearLayout lay_parent_chat_item;


        public NewsHolder(View view) {
            super(view);

            txt_chatname = (TextView) view.findViewById(R.id.txt_chatname);
            txt_chatdetails = (TextView) view.findViewById(R.id.txt_chatdetails);
            txt_lastchattime = (TextView) view.findViewById(R.id.txt_lastchattime);

            lay_parent_chat_item = (LinearLayout) view.findViewById(R.id.lay_parent_chat_item);

            img_chatperson = (ImageView) view.findViewById(R.id.img_chatperson);


        }
    }

    public ChatMessagesAdapter(ChatActivity mContext, ArrayList<ChatMessages> mChatMessagesList) {
        this.mChatMessagesList = mChatMessagesList;
        this.mContext = mContext;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public ChatMessagesAdapter.NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.chat_row_layout_list, parent, false);
        NewsHolder newsVh = new NewsHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(final NewsHolder holder, int position)
    {
        ChatMessages mChatMessages = mChatMessagesList.get(position);

//        holder.txt_chatname.setText(mChatMessages.getFirstName());
        holder.txt_chatdetails.setText(mChatMessages.getMessage());
//        holder.txt_lastchattime.setText(mRecetChats.getLastchattime());

        mContext.overrideFonts(mContext,holder.lay_parent_chat_item);

//        holder.img_chatperson.setImageResource(R.drawable.ic_profileman);

        holder.lay_parent_chat_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

//        imageLoader.get(mChatMessages.getProfilePic(), new ImageLoader.ImageListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("ERROR", "Image Load Error: " + error.getMessage());
//            }
//
//            @Override
//            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                if (response.getBitmap() != null) {
//                    // load image into imageview
//                    holder.img_chatperson.setImageBitmap(response.getBitmap());
////                    holder.news_progress.setVisibility(View.GONE);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mChatMessagesList.size();
    }
}

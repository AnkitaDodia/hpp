package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.ViewAllCommentsActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.Comment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder>
{
    private List<Comment> CommentList;

    ViewAllCommentsActivity ctx;

    ImageLoader imageLoader;

    public class CommentHolder extends RecyclerView.ViewHolder
    {
        public TextView text_editor_name,text_time_view_all,text_view_all_des;

        CircleImageView image_view_all_comment;

        LinearLayout view_all_item_main;

        ProgressBar progress_view_all_comment;

        public CommentHolder(View view) {
            super(view);
            text_editor_name = (TextView) view.findViewById(R.id.text_editor_name);
            text_time_view_all = (TextView) view.findViewById(R.id.text_time_view_all);
            text_view_all_des = (TextView) view.findViewById(R.id.text_view_all_des);

            image_view_all_comment = (CircleImageView) view.findViewById(R.id.image_view_all_comment);

            view_all_item_main = (LinearLayout) view.findViewById(R.id.view_all_item_main);

            progress_view_all_comment = (ProgressBar) view.findViewById(R.id.progress_view_all_comment);
        }
    }

    public CommentAdapter(ViewAllCommentsActivity ctx, List<Comment> CommentList) {
        this.CommentList = CommentList;
        this.ctx = ctx;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public CommentAdapter.CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View itemView = inflater.inflate(R.layout.view_all_comments_item, parent, false);
        CommentHolder newsVh = new CommentHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(final CommentHolder holder, int position)
    {
        Comment mComment = CommentList.get(position);

        ctx.overrideFonts(ctx,holder.view_all_item_main);

        holder.text_editor_name.setText(mComment.getUserFirstName()+" "+mComment.getUserLastName());
        holder.text_time_view_all.setText(mComment.getCreatedAt());
        holder.text_view_all_des.setText(mComment.getComment());

        try {
            imageLoader.get(mComment.getUserProfilePic(), new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Image Load Error: " + error.getMessage());
                    holder.progress_view_all_comment.setVisibility(View.GONE);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        // load image into imageview
                        holder.image_view_all_comment.setImageBitmap(response.getBitmap());
                        holder.progress_view_all_comment.setVisibility(View.GONE);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return CommentList.size();
    }
}

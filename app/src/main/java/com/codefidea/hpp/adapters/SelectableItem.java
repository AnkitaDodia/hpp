package com.codefidea.hpp.adapters;

import com.codefidea.hpp.model.Ingredients;

/**
 * Created by Raavan on 13-Mar-18.
 */

public class SelectableItem extends Ingredients{

    private boolean isSelected = false;


    public SelectableItem(Ingredients item,boolean isSelected) {
        super(item.getIngredientsID(),item.getIngredientsName());
        this.isSelected = isSelected;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.RecetChats;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.NewsHolder>
{
    private List<RecetChats> ChatNameList;
    MainActivity mContext;

    ImageLoader imageLoader;

    public class NewsHolder extends RecyclerView.ViewHolder
    {
        TextView txt_chatname,txt_chatdetails, txt_lastchattime;

        CircleImageView img_chatperson;

        LinearLayout lay_parent_chat_item;

        ProgressBar progress_chat;

        public NewsHolder(View view) {
            super(view);

            txt_chatname = (TextView) view.findViewById(R.id.txt_chatname);
            txt_chatdetails = (TextView) view.findViewById(R.id.txt_chatdetails);
            txt_lastchattime = (TextView) view.findViewById(R.id.txt_lastchattime);

            lay_parent_chat_item = (LinearLayout) view.findViewById(R.id.lay_parent_chat_item);

            img_chatperson = (CircleImageView) view.findViewById(R.id.img_chatperson);

            progress_chat = (ProgressBar) view.findViewById(R.id.progress_chat);
        }
    }

    public ChatAdapter(MainActivity mContext, List<RecetChats> mChatNameList) {
        this.ChatNameList = mChatNameList;
        this.mContext = mContext;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public ChatAdapter.NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.chat_row_layout_list, parent, false);
        NewsHolder newsVh = new NewsHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(final NewsHolder holder, int position)
    {
        RecetChats mRecetChats = ChatNameList.get(position);

        holder.txt_chatname.setText(mRecetChats.getFirstName());
        holder.txt_chatdetails.setText(mRecetChats.getMessage());
//        holder.txt_lastchattime.setText(mRecetChats.getLastchattime());

        mContext.overrideFonts(mContext,holder.lay_parent_chat_item);

//        holder.img_chatperson.setImageResource(R.drawable.ic_profileman);

        holder.lay_parent_chat_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        imageLoader.get(mRecetChats.getProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.progress_chat.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.img_chatperson.setImageBitmap(response.getBitmap());
                    holder.progress_chat.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ChatNameList.size();
    }
}

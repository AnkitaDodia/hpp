package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.RecipeDetailsActivity;
import com.codefidea.hpp.model.RecipeDetails;
import com.codefidea.hpp.model.RecipePrepStep;
import com.codefidea.hpp.model.RecipesData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codfidea on 22-05-2016.
 */


public class RecipePrepStepAdapter extends RecyclerView.Adapter<RecipePrepStepAdapter.MyViewHolder> {

    private ArrayList<RecipePrepStep> mRecipePrepStepList;
    RecipeDetailsActivity mContext;
    private ArrayList<Integer> StepSelecList = new ArrayList<>();


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_recipes_prepstep_disc;
        public Button btn_recipe_step_num;
        public LinearLayout lay_recipe_prepsteps;

        public MyViewHolder(View view) {
            super(view);
            txt_recipes_prepstep_disc = (TextView) view.findViewById(R.id.txt_recipes_prepstep_disc);
            btn_recipe_step_num = (Button) view.findViewById(R.id.btn_recipe_step_num);
            lay_recipe_prepsteps = (LinearLayout) view.findViewById(R.id.lay_recipe_prepsteps);
        }
    }


    public RecipePrepStepAdapter(RecipeDetailsActivity mContext, ArrayList<RecipePrepStep> mRecipePrepStepList, ArrayList<Integer> mStepSelecList) {
        this.mContext = mContext;
        this.mRecipePrepStepList = mRecipePrepStepList;
        this.StepSelecList = mStepSelecList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recipe_prepsteps, parent, false);

//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(isViewGrid ? R.layout.recipe_row_layout_list : R.layout.recipe_row_layout_grid, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.txt_recipes_prepstep_disc.setTypeface(mContext.getTypeFace());
        holder.btn_recipe_step_num.setTypeface(mContext.getTypeFace());

        RecipePrepStep mRecipePrepStep = mRecipePrepStepList.get(position);

        holder.txt_recipes_prepstep_disc.setText(mRecipePrepStep.getStepDiscription());


        if(StepSelecList.get(position) == 1){
            holder.txt_recipes_prepstep_disc.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            holder.btn_recipe_step_num.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
            holder.btn_recipe_step_num.setBackground(mContext.getResources().getDrawable(R.drawable.ic_right_tick));
            holder.btn_recipe_step_num.setText("");
        }else{
            holder.txt_recipes_prepstep_disc.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            holder.btn_recipe_step_num.setTextColor(mContext.getResources().getColor(R.color.SealBrownLight));
            holder.btn_recipe_step_num.setBackground(mContext.getResources().getDrawable(R.drawable.round_border));
            holder.btn_recipe_step_num.setText(mRecipePrepStep.getStepNumb());
        }

    }

    @Override
    public int getItemCount() {
        Log.e("Tag","size"+ mRecipePrepStepList.size());
        return mRecipePrepStepList.size();
    }
}

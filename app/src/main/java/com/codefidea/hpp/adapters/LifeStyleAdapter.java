package com.codefidea.hpp.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.LifeStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 16-Mar-18.
 */

public class LifeStyleAdapter extends RecyclerView.Adapter<LifeStyleAdapter.LifeStyleHolder>
{
    SignUpPagerActivity mContext;

    private ArrayList<LifeStyle> mList = new ArrayList<>();
    List<Integer> mMorphoSelectionsList = new ArrayList<>();

    ImageLoader imageLoader;

    public class LifeStyleHolder extends  RecyclerView.ViewHolder
    {
        CardView crdv_life_style_item;

        LinearLayout layout_main_life_style;

        ImageView image_life_style_item;

        TextView txt_title_life_style,txt_description_life_style;

        ProgressBar life_style_progress;

        public LifeStyleHolder(View view) {
            super(view);

            crdv_life_style_item = (CardView) view.findViewById(R.id.crdv_life_style_item);

            layout_main_life_style = (LinearLayout) view.findViewById(R.id.layout_main_life_style);

            image_life_style_item = (ImageView) view.findViewById(R.id.image_life_style_item);

            txt_title_life_style = (TextView) view.findViewById(R.id.txt_title_life_style);
            txt_description_life_style = (TextView) view.findViewById(R.id.txt_description_life_style);

            life_style_progress = (ProgressBar) view.findViewById(R.id.life_style_progress);
        }
    }

    public LifeStyleAdapter(SignUpPagerActivity mContext, ArrayList<LifeStyle> mLifeStyle, List<Integer> mMorphoSelectionsList)
    {
        this.mContext = mContext;
        this.mList = mLifeStyle;
        this.mMorphoSelectionsList = mMorphoSelectionsList;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public LifeStyleAdapter.LifeStyleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_item_life_style, parent, false);
        LifeStyleAdapter.LifeStyleHolder morphoVh = new LifeStyleAdapter.LifeStyleHolder(itemView);
        return morphoVh;
    }

    @Override
    public void onBindViewHolder(final LifeStyleHolder holder, final int position) {
        mContext.overrideFonts(mContext,holder.layout_main_life_style);

        final LifeStyle mData = mList.get(position);

        holder.txt_title_life_style.setText(mData.getName());
        holder.txt_description_life_style.setText(mData.getDescription());

        if(mMorphoSelectionsList.size() > 0 && mMorphoSelectionsList.get(position) == 1){
            holder.crdv_life_style_item.setCardElevation(30.0f);
            holder.crdv_life_style_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.dot_light_green));
        }else{
            holder.crdv_life_style_item.setCardElevation(2.0f);
            holder.crdv_life_style_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        }


        imageLoader.get(mData.getImage(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.life_style_progress.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.image_life_style_item.setImageBitmap(response.getBitmap());
                    holder.life_style_progress.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}

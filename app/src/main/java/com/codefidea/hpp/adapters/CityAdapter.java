package com.codefidea.hpp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.common.BaseActivity;
import com.codefidea.hpp.model.Cities;

import java.util.ArrayList;

/**
 * Created by My 7 on 15-Mar-18.
 */

public class CityAdapter extends BaseAdapter {
    SignUpPagerActivity mContext;

    ArrayList<Cities> cityList = new ArrayList<>();

    LayoutInflater inflter;

    public CityAdapter(SignUpPagerActivity mContext, ArrayList<Cities> cityList)
    {
        this.mContext = mContext;
        this.cityList = cityList;

        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        view = inflter.inflate(R.layout.row_countries_city_layout, null);

        TextView names = (TextView) view.findViewById(R.id.txt_country_name);

        names.setTypeface(mContext.getTypeFace());

        Cities data = cityList.get(position);

        names.setText(data.getName());

//        names.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BaseActivity.cityID = cityList.get(position).getId();
//            }
//        });

        return view;
    }
}

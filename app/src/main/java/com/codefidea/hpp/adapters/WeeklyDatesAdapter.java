package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.model.WeeklyDates;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class WeeklyDatesAdapter extends RecyclerView.Adapter<WeeklyDatesAdapter.WeeklyDatesHolder> {

    private ArrayList<WeeklyDates> mWeeklyDatesList = new ArrayList<>();
    private List<Integer> mWeeklyDatesSelecList = new ArrayList<>();

    Date stringCurrentDate;

    MainActivity mContext;
    int mParentWidth,mPosition;


    public class WeeklyDatesHolder extends RecyclerView.ViewHolder {
        TextView txt_weekday, txt_weekdate;
        LinearLayout lay_parent_date_item, lay_date_status;


        public WeeklyDatesHolder(View view) {
            super(view);

            txt_weekday = (TextView) view.findViewById(R.id.txt_weekday);
            txt_weekdate = (TextView) view.findViewById(R.id.txt_weekdate);

            lay_parent_date_item = (LinearLayout) view.findViewById(R.id.lay_parent_date_item);
            lay_date_status = (LinearLayout) view.findViewById(R.id.lay_date_status);
        }
    }

    public WeeklyDatesAdapter(MainActivity mContext, ArrayList<WeeklyDates> mWeeklyDatesList, List<Integer> mWeeklyDatesSelecList) {
        this.mWeeklyDatesList = mWeeklyDatesList;
        this.mWeeklyDatesSelecList = mWeeklyDatesSelecList;
        this.mContext = mContext;
    }

    @Override
    public WeeklyDatesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_weekly_dates, parent, false);
        WeeklyDatesHolder WeeklyDatesVh = new WeeklyDatesHolder(itemView);
        return WeeklyDatesVh;
    }

    @Override
    public void onBindViewHolder(WeeklyDatesHolder holder, int position) {
        final WeeklyDates mWeeklyDates = mWeeklyDatesList.get(position);

        int width = mParentWidth / 8;

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(8, 0, 8, 0);
        holder.lay_parent_date_item.setLayoutParams(lp);
//
//        holder.txt_weekday.setWidth(width);
//        holder.txt_weekdate.setWidth(width);

        holder.txt_weekday.setText(firstTwo(mWeeklyDates.getDay()));
        holder.txt_weekdate.setText(lastTwo(mWeeklyDates.getDate()));

        mContext.overrideFonts(mContext, holder.lay_parent_date_item);

        holder.lay_parent_date_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if (mWeeklyDates.isDaystatus() == true) {
            holder.lay_date_status.setVisibility(View.VISIBLE);
        } else {
            holder.lay_date_status.setVisibility(View.INVISIBLE);
        }

        if(mWeeklyDatesSelecList.get(position) == 1){
            holder.txt_weekday.setBackgroundResource(R.drawable.textbg);
        }else{
            holder.txt_weekday.setBackgroundResource(R.drawable.unselectedtext_bg);
        }
    }

    public String firstTwo(String str) {
        return str.length() < 2 ? str : str.substring(0, 2);
    }

    public String lastTwo(String str) {
        return str.substring(Math.max(str.length() - 2, 0));
    }

    public void setParentListView(int width) {
        mParentWidth = width;
    }


    @Override
    public int getItemCount() {
        return mWeeklyDatesList.size();
    }

    public int getPos()
    {
        return mPosition;
    }
}

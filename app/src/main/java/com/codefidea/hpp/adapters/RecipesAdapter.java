package com.codefidea.hpp.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.activities.RecipeDetailsActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.RecipesData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by codfidea on 22-05-2016.
 */


public class RecipesAdapter extends RecyclerView.Adapter<RecipesAdapter.MyViewHolder> {

    private List<RecipesData> mRecipesList;

    MainActivity mContext;

    Boolean isViewGrid;

    ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_recipes_title, txt_recipes_total_like;

        ImageView img_recipes, img_recipes_play, img_recipes_fav;

        RelativeLayout lay_list_recipe;

        ProgressBar progress_recipe_item;

        public MyViewHolder(View view) {
            super(view);
            txt_recipes_title = (TextView) view.findViewById(R.id.txt_recipes_title);
            txt_recipes_total_like = (TextView) view.findViewById(R.id.txt_recipes_total_like);

            img_recipes = (ImageView) view.findViewById(R.id.img_recipes);
            img_recipes_play = (ImageView) view.findViewById(R.id.img_recipes_play);
            img_recipes_fav = (ImageView) view.findViewById(R.id.img_recipes_fav);

            lay_list_recipe = (RelativeLayout) view.findViewById(R.id.lay_list_recipe);

            progress_recipe_item = (ProgressBar) view.findViewById(R.id.progress_recipe_item);
        }
    }

    public RecipesAdapter(MainActivity mContext, ArrayList<RecipesData> mRecipesList, Boolean isViewGrid) {
        this.mContext = mContext;
        this.mRecipesList = mRecipesList;
        this.isViewGrid = isViewGrid;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.recipe_row_layout_list, parent, false);

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(isViewGrid ? R.layout.recipe_row_layout_list : R.layout.recipe_row_layout_grid, null);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.txt_recipes_title.setTypeface(mContext.getTypeFace());

        RecipesData mRecipes = mRecipesList.get(position);

        holder.txt_recipes_title.setText(mRecipes.getName());
        holder.txt_recipes_total_like.setText(mRecipes.getTotalLikes());

        Log.e("Tag","tiel  "+mRecipes.getName());

        imageLoader.get(mRecipes.getImage(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.progress_recipe_item.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.img_recipes.setImageBitmap(response.getBitmap());
                    holder.progress_recipe_item.setVisibility(View.GONE);
                }
            }
        });

//        holder.lay_list_recipe.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent it = new Intent(mContext, RecipeDetailsActivity.class);
//                mContext.startActivity(it);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        Log.e("Tag","size"+ mRecipesList.size());
        return mRecipesList.size();
    }
}

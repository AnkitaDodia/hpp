package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.LeaderBoard;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.LeaderBoardHolder>
{
    private List<LeaderBoard> leaderList;
    MainActivity mContext;
    ImageLoader imageLoader;

    public class LeaderBoardHolder extends RecyclerView.ViewHolder
    {
        public TextView text_user_name_leader_board,text_point_leader_board;

        CircleImageView image_leader_board;

        LinearLayout leader_board_item_main;

        ProgressBar leader_board_progress;

        public LeaderBoardHolder(View view) {
            super(view);
            text_user_name_leader_board = (TextView) view.findViewById(R.id.text_user_name_leader_board);
            text_point_leader_board = (TextView) view.findViewById(R.id.text_point_leader_board);

            image_leader_board = (CircleImageView) view.findViewById(R.id.image_leader_board);

            leader_board_item_main = (LinearLayout) view.findViewById(R.id.leader_board_item_main);

            leader_board_progress = (ProgressBar) view.findViewById(R.id.leader_board_progress);
        }
    }

    public LeaderBoardAdapter(MainActivity mContext, List<LeaderBoard> leaderList) {
        this.leaderList = leaderList;
        this.mContext = mContext;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public LeaderBoardAdapter.LeaderBoardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.leader_board_item, parent, false);
        LeaderBoardHolder newsVh = new LeaderBoardHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(final LeaderBoardHolder holder, int position)
    {
        LeaderBoard mLeader = leaderList.get(position);

        mContext.overrideFonts(mContext,holder.leader_board_item_main);

        holder.text_user_name_leader_board.setText(mLeader.getFirstName());
        holder.text_point_leader_board.setText(mLeader.getPoints());

        imageLoader.get(mLeader.getProfilePic(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.leader_board_progress.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.image_leader_board.setImageBitmap(response.getBitmap());
                    holder.leader_board_progress.setVisibility(View.GONE);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return leaderList.size();
    }
}

package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.RecipeDetailsActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.RecipeImages;
import com.codefidea.hpp.model.RecipePrepStep;

import java.util.ArrayList;

/**
 * Created by codfidea on 22-05-2016.
 */


public class RecipeImagesGridAdapter extends RecyclerView.Adapter<RecipeImagesGridAdapter.MyViewHolder> {

    private ArrayList<RecipeImages> mRecipeImagesList;
    RecipeDetailsActivity mContext;
    ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_grid_recipes;

        ProgressBar progress_recipe;

        public MyViewHolder(View view) {
            super(view);
            img_grid_recipes = (ImageView) view.findViewById(R.id.img_grid_recipes);

            progress_recipe = (ProgressBar) view.findViewById(R.id.progress_recipe);
        }
    }

    public RecipeImagesGridAdapter(RecipeDetailsActivity mContext, ArrayList<RecipeImages> mRecipeImagesList) {
        this.mContext = mContext;
        this.mRecipeImagesList = mRecipeImagesList;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recipe_images, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        RecipeImages mRecipeImages = mRecipeImagesList.get(position);

        Log.e("GridImagePath","GridImagePath"+ mRecipeImages.getRecipeImagesUrl());

        imageLoader.get(mRecipeImages.getRecipeImagesUrl(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.progress_recipe.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.img_grid_recipes.setImageBitmap(response.getBitmap());
                    holder.progress_recipe.setVisibility(View.GONE);
                }
            }
        });



    }

    @Override
    public int getItemCount() {
        Log.e("Tag","size"+ mRecipeImagesList.size());
        return mRecipeImagesList.size();
    }
}

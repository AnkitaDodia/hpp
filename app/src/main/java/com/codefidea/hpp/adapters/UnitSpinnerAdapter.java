package com.codefidea.hpp.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.WeightCeilingActivity;
import com.codefidea.hpp.common.BaseActivity;

import java.util.List;

/**
 * Created by Raavan on 21-Mar-18.
 */

public class UnitSpinnerAdapter extends ArrayAdapter<String> {

    // Initialise custom font, for example:
//    Typeface font = Typeface.createFromAsset(getContext().getAssets(),
//            "fonts/Blambot.otf");
    BaseActivity mContext;

    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public UnitSpinnerAdapter(BaseActivity context, int resource, List<String> items) {
        super(context, resource, items);

        mContext = context;
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(mContext.getTypeFace());
        view.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(mContext.getTypeFace());
        view.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
        return view;
    }
}
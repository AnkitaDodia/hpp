package com.codefidea.hpp.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.MorphesType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 16-Mar-18.
 */

public class MorphoTypeAdapter extends RecyclerView.Adapter<MorphoTypeAdapter.MorphoTypeHolder>
{
    SignUpPagerActivity mContext;

    private ArrayList<MorphesType> mMorpho = new ArrayList<>();
    List<Integer> mMorphoSelectionsList = new ArrayList<>();

    ImageLoader imageLoader;

    public class MorphoTypeHolder extends  RecyclerView.ViewHolder
    {
        CardView crdv_morpho_item;

        LinearLayout layout_main_morpho;

        ImageView image_morpho;

        TextView txt_morpho_title,txt_morpho_des;

        ProgressBar morpho_progress;

        public MorphoTypeHolder(View view) {
            super(view);

            crdv_morpho_item = (CardView) view.findViewById(R.id.crdv_morpho_item);

            layout_main_morpho = (LinearLayout) view.findViewById(R.id.layout_main_morpho);

            image_morpho = (ImageView) view.findViewById(R.id.image_morpho);

            txt_morpho_title = (TextView) view.findViewById(R.id.txt_morpho_title);
            txt_morpho_des = (TextView) view.findViewById(R.id.txt_morpho_des);

            morpho_progress = (ProgressBar) view.findViewById(R.id.morpho_progress);
        }
    }

    public MorphoTypeAdapter(SignUpPagerActivity mContext,ArrayList<MorphesType> mMorpho, List<Integer> mMorphoSelectionsList)
    {
        this.mContext = mContext;
        this.mMorpho = mMorpho;
        this.mMorphoSelectionsList = mMorphoSelectionsList;

        imageLoader = AppController.getInstance().getImageLoader();
    }

    @Override
    public MorphoTypeAdapter.MorphoTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_item_morpho_type, parent, false);
        MorphoTypeAdapter.MorphoTypeHolder morphoVh = new MorphoTypeAdapter.MorphoTypeHolder(itemView);
        return morphoVh;
    }

    @Override
    public void onBindViewHolder(final MorphoTypeHolder holder, final int position) {
        mContext.overrideFonts(mContext,holder.layout_main_morpho);

        final MorphesType mData = mMorpho.get(position);

        holder.txt_morpho_title.setText(mData.getName());
        holder.txt_morpho_des.setText(mData.getDescription());

        if(mMorphoSelectionsList.size() > 0 && mMorphoSelectionsList.get(position) == 1){
            holder.crdv_morpho_item.setCardElevation(30.0f);
            holder.crdv_morpho_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.dot_light_green));
        }else{
            holder.crdv_morpho_item.setCardElevation(2.0f);
            holder.crdv_morpho_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        imageLoader.get(mData.getImage(), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", "Image Load Error: " + error.getMessage());
                holder.morpho_progress.setVisibility(View.GONE);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    holder.image_morpho.setImageBitmap(response.getBitmap());
                    holder.morpho_progress.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMorpho.size();
    }
}

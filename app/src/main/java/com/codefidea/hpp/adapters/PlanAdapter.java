package com.codefidea.hpp.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.SignUpPagerActivity;
import com.codefidea.hpp.common.AppController;
import com.codefidea.hpp.model.LifeStyle;
import com.codefidea.hpp.model.Plans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 16-Mar-18.
 */

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.PlanHolder>
{
    SignUpPagerActivity mContext;

    private ArrayList<Plans> mList = new ArrayList<>();
    List<Integer> mPlanSelectionsList = new ArrayList<>();


    public class PlanHolder extends  RecyclerView.ViewHolder
    {
        CardView crdv_plans_item;

        LinearLayout layout_plan_main;

        TextView txt_title_plan_item,txt_description_plan_item,txt_plan_price_plan_item;


        public PlanHolder(View view) {
            super(view);

            crdv_plans_item = (CardView) view.findViewById(R.id.crdv_plans_item);

            layout_plan_main = (LinearLayout) view.findViewById(R.id.layout_plan_main);

            txt_title_plan_item = (TextView) view.findViewById(R.id.txt_title_plan_item);
            txt_description_plan_item = (TextView) view.findViewById(R.id.txt_description_plan_item);
            txt_plan_price_plan_item = (TextView) view.findViewById(R.id.txt_plan_price_plan_item);
        }
    }

    public PlanAdapter(SignUpPagerActivity mContext, ArrayList<Plans> mPlans, List<Integer> mPlanSelectionsList)
    {
        this.mContext = mContext;
        this.mList = mPlans;
        this.mPlanSelectionsList = mPlanSelectionsList;
    }

    @Override
    public PlanAdapter.PlanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_item_plans, parent, false);
        PlanAdapter.PlanHolder morphoVh = new PlanAdapter.PlanHolder(itemView);
        return morphoVh;
    }

    @Override
    public void onBindViewHolder(final PlanHolder holder, final int position) {
        mContext.overrideFonts(mContext,holder.layout_plan_main);

        final Plans mData = mList.get(position);

        holder.txt_title_plan_item.setText(mData.getName());
        holder.txt_description_plan_item.setText(mData.getDescription());

        if(mData.getCost().equalsIgnoreCase("0.00"))
        {
            holder.txt_plan_price_plan_item.setText("Free");
        }
        else
        {
            holder.txt_plan_price_plan_item.setText(mData.getCost());
        }

        if(mPlanSelectionsList.size() > 0 && mPlanSelectionsList.get(position) == 1){
            holder.crdv_plans_item.setCardElevation(30.0f);
            holder.crdv_plans_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.dot_light_green));
        }else{
            holder.crdv_plans_item.setCardElevation(2.0f);
            holder.crdv_plans_item.setCardBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}

package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.CuisinesActivity;
import com.codefidea.hpp.activities.MainActivity;
import com.codefidea.hpp.model.Cuisines;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class CuisinesAdapter extends RecyclerView.Adapter<CuisinesAdapter.CuisinesHolder>
{
    private ArrayList<Cuisines> CuisinesList;

    List<Integer> CuisinesSelectionsList = new ArrayList<>();
    CuisinesActivity mContext;

    public class CuisinesHolder extends RecyclerView.ViewHolder
    {
        TextView txt_cuisine_name;

        LinearLayout ll_parent_row_cuisine;


        public CuisinesHolder(View view) {
            super(view);

            txt_cuisine_name = (TextView) view.findViewById(R.id.txt_cuisine_name);

            ll_parent_row_cuisine = (LinearLayout) view.findViewById(R.id.ll_parent_row_cuisine);
        }
    }

    public CuisinesAdapter(CuisinesActivity mContext, ArrayList<Cuisines> mCuisinesList, List<Integer> mCuisinesSelectionsList) {
        this.CuisinesList = mCuisinesList;
        this.mContext = mContext;
        this.CuisinesSelectionsList = mCuisinesSelectionsList;
    }

    @Override
    public CuisinesAdapter.CuisinesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_cuisines_layout, parent, false);
        CuisinesHolder newsVh = new CuisinesHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(CuisinesHolder holder, int position)
    {
        final Cuisines mCuisines = CuisinesList.get(position);

        holder.txt_cuisine_name.setText(mCuisines.getCuisinesName());

        mContext.overrideFonts(mContext,holder.ll_parent_row_cuisine);

        holder.ll_parent_row_cuisine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(mContext, ""+mCuisines.getCuisinesName(), Toast.LENGTH_SHORT).show();
            }
        });

        if(CuisinesSelectionsList.get(position) == 1){
            holder.txt_cuisine_name.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }else{
            holder.txt_cuisine_name.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
        }
    }

    @Override
    public int getItemCount() {
        return CuisinesList.size();
    }
}

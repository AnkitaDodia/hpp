package com.codefidea.hpp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codefidea.hpp.R;
import com.codefidea.hpp.activities.CuisinesActivity;
import com.codefidea.hpp.activities.RecipeDetailsActivity;
import com.codefidea.hpp.model.Cuisines;
import com.codefidea.hpp.model.RecipeIngredients;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by My 7 on 07-Mar-18.
 */

public class RecipeIngredientsAdapter extends RecyclerView.Adapter<RecipeIngredientsAdapter.RecipeIngredientsHolder>
{
    private ArrayList<RecipeIngredients> mRecipeIngredientsList;

    RecipeDetailsActivity mContext;

    public class RecipeIngredientsHolder extends RecyclerView.ViewHolder
    {
        TextView txt_recipe_ingredients_name;

        LinearLayout ll_parent_row_cuisine;


        public RecipeIngredientsHolder(View view) {
            super(view);

            txt_recipe_ingredients_name = (TextView) view.findViewById(R.id.txt_cuisine_name);

            ll_parent_row_cuisine = (LinearLayout) view.findViewById(R.id.ll_parent_row_cuisine);
        }
    }

    public RecipeIngredientsAdapter(RecipeDetailsActivity mContext, ArrayList<RecipeIngredients> mRecipeIngredientsList) {
        this.mRecipeIngredientsList = mRecipeIngredientsList;
        this.mContext = mContext;
    }

    @Override
    public RecipeIngredientsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View itemView = inflater.inflate(R.layout.row_ingredients, parent, false);
        RecipeIngredientsHolder newsVh = new RecipeIngredientsHolder(itemView);
        return newsVh;
    }

    @Override
    public void onBindViewHolder(RecipeIngredientsHolder holder, int position)
    {
        final RecipeIngredients mRecipeIngredients = mRecipeIngredientsList.get(position);

        holder.txt_recipe_ingredients_name.setText(mRecipeIngredients.getIngredientsName());

        mContext.overrideFonts(mContext,holder.ll_parent_row_cuisine);

        holder.ll_parent_row_cuisine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(mContext, ""+mCuisines.getCuisinesName(), Toast.LENGTH_SHORT).show();
            }
        });


//        if(mRecipeIngredientsList.get(position) == 1){
//            holder.txt_cuisine_name.setTextColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
//        }else{
//            holder.txt_cuisine_name.setTextColor(mContext.getResources().getColor(R.color.SealBrown));
//        }

    }

    @Override
    public int getItemCount() {
        return mRecipeIngredientsList.size();
    }
}
